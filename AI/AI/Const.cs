﻿//using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    static class Const {

        //STATES for the game loop to be in, from the AI's point of view
        //this in combination with who's the current player will allow the ai to know what it's current options are. sometimes none. (e.g. other player has to place initial settlement)

        //waiting for server
        public const int AWAITINGMONOPOLYPRETURN = -9;
        public const int AWAITINGROBBERPLACEMENTDUETOKNIGHTBEINGPLAYEDBEFOREROLLINGDICE = -8;
        public const int AWAITINGSTEALPRETURN = -7;//waiting a steal caused by playing a KNIGHT before rolling the dice, pretty niche
        public const int AWAITINGMONOPOLY = -6;//waiting for the host to decide what resource was stolen
        public const int AWAITINGSTEAL = -5;//waiting for the host to decide what resource was stolen
        public const int AWAITINGENDTURN = -4;//i.e. the AI has ended its turn, and so has to stop doing things
        public const int AWAITDISCARD = -3; //waiting for other players to discard after 7 is rolled
        public const int AWAITINGTRADERESPONSE = -2;//waiting for the 'server' to respond to a trade request
        public const int AWAITINGDEVCARDBUYING = -1;//waiting for the server to choose a dev card
        public const int AWAITINGDICEROLL = -0;//waiting for the server to choose dice roll

        //initial states
        public const int INITIALSETTLEMENT = 1;//needs to build an initial settlement
        public const int INITIALROAD = 2;//needs to build an initial road

        //turn loop states
        public const int TURNSTART = 3;//is at the start of the turn (can play dev card or roll dice)
        public const int WAITPLAY = 5;//in the main 'turn phase' so able to buy stuff, play devcard, trade ...

        //states in the middle of playing dev cards (and also moving robber after rolling a 7)
        public const int FIRSTROADBUILDING = 6;//needs to place the first road of road building
        public const int SECONDROADBUILDING = 7;//needs to place the second road of road building
        public const int FIRSTYEAROFPLENTY = 8;//needs to choose the first resource to take for year of plenty;
        public const int SECONDYEAROFPLENTY = 9;//needs to choose the seconds resource to take for year of plenty;
        public const int CHOOSINGMONOPOLY = 10;//needs to choose the resource to monopolise
        public const int CHOOSINGROBBERPLACEMENT = 11;//needs to choose where to place the robber
        public const int RESOURCETOSTEAL = 12;//needs to choose what resource to take and from what player, based on the robber pos?
        public const int RESOURCETOSTEALPRETURN = 13;

        //Player Trading
        public const int TRADE_OFFERED_TO_AI = 14; //state when another player has offered a trade to the AI
        public const int TRADE_WITHOUT_AI = 15; //state when a trade is happening that doesnt involve the AI

        public const int FIRSTROADBUILDINGBEFOREROLLINGTHEDICE = 16;//needs to place the first road of road building
        public const int SECONDROADBUILDINGBEFOREROLLINGTHEDICE = 17;//needs to place the second road of road building
        public const int FIRSTYEAROFPLENTYBEFOREROLLINGTHEDICE = 18;//needs to choose the first resource to take for year of plenty;
        public const int SECONDYEAROFPLENTYBEFOREROLLINGTHEDICE = 19;//needs to choose the seconds resource to take for year of plenty;
        public const int CHOOSINGMONOPOLYBEFOREROLLINGTHEDICE = 20;//needs to choose the resource to monopolise


        //RESOURCES 
        public const int NUM_OF_RESOURCES = 5;

        public const int DESERT = -1;//for terrain types	
        public const int BRICK = 0;
        public const int LUMBER = 1;
        public const int WOOL = 2;
        public const int GRAIN = 3;
        public const int ORE = 4;
        //General Port
        public const int UNIVERSAL = 5;

        //DEV CARDS
        public const int NUM_OF_DEVCARDS = 5;

        public const int KNIGHT = 0;
        public const int ROAD_BUILDING = 1;
        public const int MONOPOLY = 2;
        public const int YEAR_OF_PLENTY = 3;
        public const int VICTORY_POINT = 4;
        public const int UNKNOWN_DEVCARD = 5;

        //BANK QUANTITY
        public const int Q_BANK_RESOURCES = 19;
        public const int Q_KNIGHT_DEV = 14;
        public const int Q_PROGRESS_DEV = 2;
        public const int Q_VP_DEV = 5;
        public const int Q_DEV_TOTAL = 25;

        //ROLL PROBABILITY
        public static readonly Dictionary<int, double> ROLL_PROBABILITY = new Dictionary<int, double> {
            {2, 2.78},
            {3, 5.56},
            {4, 8.33},
            {5, 11.11},
            {6, 13.89},
            {7,  16.67},
            {8, 13.89},
            {9, 11.11},
            {10, 8.33},
            {11, 5.56},
            {12, 2.78}
        };
    }
}
