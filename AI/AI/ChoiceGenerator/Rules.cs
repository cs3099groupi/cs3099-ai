﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class Rules : IRules {
        const int MAXSETTLEMENTS = 5;
        const int MAXCITIES = 4;
        const int MAXROADS = 15;

        List<int[]> discardCombos = new List<int[]>();

        public bool canAcceptTrade(Intergroup.Event tradeToAI, IPlayer p) {

            int[] want = new int[Const.NUM_OF_RESOURCES];

            want[Const.BRICK] = tradeToAI.PlayerTradeInitiated.Wanting.Brick;
            want[Const.LUMBER] = tradeToAI.PlayerTradeInitiated.Wanting.Lumber;
            want[Const.WOOL] = tradeToAI.PlayerTradeInitiated.Wanting.Wool;
            want[Const.ORE] = tradeToAI.PlayerTradeInitiated.Wanting.Ore;
            want[Const.GRAIN] = tradeToAI.PlayerTradeInitiated.Wanting.Grain;


            if (p.getNumberResource(Const.BRICK) < want[Const.BRICK] ||
                p.getNumberResource(Const.LUMBER) < want[Const.LUMBER] ||
                p.getNumberResource(Const.WOOL) < want[Const.WOOL] ||
                p.getNumberResource(Const.ORE) < want[Const.ORE] ||
                p.getNumberResource(Const.GRAIN) < want[Const.GRAIN]) {
                return false;
            }

            return true;
        }

        public bool canAffordCity(IPlayer p) {
            return (p.getNumberResource(Const.ORE) > 2 && p.getNumberResource(Const.GRAIN) > 1);
        }

        public bool canAffordDevCard(IPlayer p) {
            return (p.getNumberResource(Const.ORE) > 0 && p.getNumberResource(Const.GRAIN) > 0 && p.getNumberResource(Const.WOOL) > 0);
        }

        public bool canAffordRoad(IPlayer p) {
            return (p.getNumberResource(Const.LUMBER) > 0 && p.getNumberResource(Const.BRICK) > 0);
        }

        public bool canAffordSettlement(IPlayer p) {
            return (p.getNumberResource(Const.LUMBER) > 0 && p.getNumberResource(Const.BRICK) > 0 && p.getNumberResource(Const.WOOL) > 0 && p.getNumberResource(Const.GRAIN) > 0);
        }

        public bool canBuildCity(IGameState st, IPlayer p) {
            return (st.isState(Const.WAITPLAY)
                && st.getCurrentPlayer() == p
                && canAffordCity(p)
                && getValidCityBuildSpots(st, p).Count > 0
                && p.getCities().Count < MAXCITIES);
        }

        public bool canBuildRoad(IGameState st, IPlayer p) {
            //Console.WriteLine("" + st.isState(Const.WAITPLAY)
            //    + (st.getCurrentPlayer() == p)
            //    + (canAffordRoad(p))
            //    + (getValidRoadBuildSpots(st, p).Count > 0)
            //    + (p.getRoads().Count < MAXROADS));
            return (st.isState(Const.WAITPLAY)
                && st.getCurrentPlayer() == p
                && canAffordRoad(p)
                && getValidRoadBuildSpots(st, p).Count > 0
                && p.getRoads().Count < MAXROADS);
        }

        public bool canBuildSettlement(IGameState st, IPlayer p) {
            return (st.isState(Const.WAITPLAY)
                && st.getCurrentPlayer() == p
                && canAffordSettlement(p)
                && getValidSettlementBuildSpots(st, p).Count > 0
                && p.getSettlements().Count < MAXSETTLEMENTS);
        }

        public bool canBuyDevCard(IGameState st, IPlayer p) {
            return (st.isState(Const.WAITPLAY)
                && st.getCurrentPlayer() == p
                && canAffordDevCard(p)
                && st.getBank().getDevCardTotal() > 0);

        }
        public List<ISettlement> getValidCityBuildSpots(IGameState state, IPlayer p) {
            return p.getSettlements();
        }

        public List<Tuple<ICoord, ICoord>> getValidRoadBuildSpots(IGameState state, IPlayer p) {
            List<Tuple<ICoord, ICoord>> validSpots = new List<Tuple<ICoord, ICoord>>();
            List<Tuple<ICoord, ICoord>> allSpots = new List<Tuple<ICoord, ICoord>>();
            foreach (Tuple<ICoord, ICoord> t in state.getFreeRoadSpots()) {
                allSpots.Add(t);
            }
            List<Tuple<ICoord, ICoord>> spotsToRemove = new List<Tuple<ICoord, ICoord>>();
            foreach (IRoad r in p.getRoads()) {
                foreach (Tuple<ICoord, ICoord> tpl in allSpots) {
                    if (r.getPos().isEqual(tpl.Item1) || r.getPos().isEqual(tpl.Item2) || r.getPos2().isEqual(tpl.Item1) || r.getPos2().isEqual(tpl.Item2)) {
                        validSpots.Add(tpl);
                        spotsToRemove.Add(tpl);
                    }
                }
                foreach (Tuple<ICoord, ICoord> c in spotsToRemove) {
                    allSpots.Remove(c);
                }
                spotsToRemove.Clear();
            }
            return validSpots;
        }

        public List<Tuple<ICoord, ICoord>> getValidRoadBuildSpotsStart(IGameState state, IPlayer p) {
            if (state.getLastBuiltSettlement() == null) return new List<Tuple<ICoord, ICoord>>();
            ICoord spot = state.getLastBuiltSettlement().getPos();
            return state.getFreeRoadSpots().Where(tpl => spot.isEqual(tpl.Item1) || spot.isEqual(tpl.Item2)).ToList();
        }

        public List<ICoord> getValidSettlementBuildSpots(IGameState state, IPlayer p) {
            List<ICoord> validSpots = new List<ICoord>();
            List<ICoord> allSpots = new List<ICoord>();
            foreach (ICoord pos in state.getFreeSettlementSpots()) {
                allSpots.Add(pos);
            }
            List<ICoord> spotsToRemove = new List<ICoord>();


            foreach (IRoad rd in p.getRoads()) {
                foreach (ICoord pos in allSpots) {
                    if (rd.getPos().isEqual(pos) || rd.getPos2().isEqual(pos)) {
                        validSpots.Add(pos);
                        spotsToRemove.Add(pos);
                    }
                }
                foreach (ICoord c in spotsToRemove) {
                    allSpots.Remove(c);
                }
                spotsToRemove.Clear();
            }
            return validSpots;
        }

        public List<ICoord> getValidSettlementBuildSpotsStart(IGameState state, IPlayer p) {
            return state.getFreeSettlementSpots();
        }

        public List<ICoord> getValidRobberMoveSpots(IGameState state) {
            List<ICoord> spots = new List<ICoord>();
            for (int x = -5; x <= 5; x++) {
                for (int y = -5; y <= 5; y++) {
                    if (isTile(x, y) && !state.getRobberPos().isEqual(new Coord(x, y))) {
                        spots.Add(new Coord(x, y));
                    }
                }
            }
            return spots;
        }

        public bool isCorner(int x, int y) {
            return (withinBounds(x, y) && !isTile(x, y));
        }

        private bool withinBounds(int x, int y) {
            return ((Math.Abs(y + x) < 9) && (Math.Abs(y - 2 * x) < 9) && (Math.Abs(2 * y - x) < 9));
        }

        public bool isTile(int x, int y) {
            return (withinBounds(x, y) && Math.Abs(x + y) % 3 == 0);
        }

        //this is just checking max length in one dir
        public int checkDirection(IPlayer player, int x1, int x2, int y1, int y2, List<IRoad> visitedRoads, IGameState st) {
            int type = checkUpDown(x1, y1);
            int max1 = 0;
            int max2 = 0;
            int max3 = 0;
            if (type == 2) {  //this is an down
                              //these discount the direction in which the road already exists
                if (x1 + 1 != x2 && y1 + 1 != y2) {
                    //checks if the road moving in this plane belongs to the player
                    if (isPlayerRoad(x1, (x1 + 1), y1, (y1 + 1), visitedRoads, player, st)) {
                        //if the road belongs to the player then move along the plane 
                        max1 = checkDirection(player, (x1 + 1), x1, (y1 + 1), y1, visitedRoads, st) + 1;
                    }
                }
                if (x1 - 1 != x2 && y1 != y2) {
                    if (isPlayerRoad(x1, (x1 - 1), y1, y1, visitedRoads, player, st)) {
                        max2 = checkDirection(player, (x1 - 1), x1, y1, y1, visitedRoads, st) + 1;
                    }
                }
                if (x1 != x2 && y1 - 1 != y2) {
                    if (isPlayerRoad(x1, x1, y1, (y1 - 1), visitedRoads, player, st)) {
                        max3 = checkDirection(player, x1, x1, (y1 - 1), y1, visitedRoads, st) + 1;
                    }

                }
                //return the greatest of max1, max2, max3
            } else {  //its an up
                if (x1 + 1 != x2 && y1 != y2) {
                    if (x1 + 1 != x2 && y1 != y2) {
                        if (isPlayerRoad(x1, (x1 + 1), y1, y1, visitedRoads, player, st)) {
                            max1 = checkDirection(player, (x1 + 1), x1, y1, y1, visitedRoads, st) + 1;
                        }
                    }
                }
                if (x1 - 1 != x2 && y1 - 1 != y2) {
                    if (x1 - 1 != x2 && y1 - 1 != y2) {
                        if (isPlayerRoad(x1, (x1 - 1), y1, (y1 - 1), visitedRoads, player, st)) {
                            max2 = checkDirection(player, (x1 - 1), x1, (y1 - 1), y1, visitedRoads, st) + 1;
                        }
                    }
                }
                if (x1 != x2 && y1 + 1 != y2) {
                    if (x1 != x2 && y1 + 1 != y2) {
                        if (isPlayerRoad(x1, x1, y1, (y1 + 1), visitedRoads, player, st)) {
                            max3 = checkDirection(player, x1, x1, (y1 + 1), y1, visitedRoads, st) + 1;
                        }
                    }
                }
            }
            return Math.Max(max1, Math.Max(max2, max3));
        }
        bool isPlayerRoad(int x1, int x2, int y1, int y2, List<IRoad> visitedRoads, IPlayer player, IGameState st) {
            IRoad road;
            road = st.getRoad(new Coord(x1, y1), new Coord(x2, y2));
            if (road == null) {
                return false;
            }
            //if the road has already been visited then start of loop so stop walking down path
            if (visitedRoads.Contains(road)) {
                return false;
            }

            visitedRoads.Add(road);

            //if a settlement breaks the road - stop recursing
            ISettlement structure = st.getSettlement(new Coord(x1, y1));
            if (structure != null && structure.getOwner() != player) {
                return false;
            }

            if (road.getOwner() != player) {
                return false;
            } else {
                return true;
            }
        }
        public int checkUpDown(int x, int y) {
            int val = x + y;
            val = val % 3;
            if (val < 0) {
                val += 3;
            }
            return val;
        }

        public List<ITile> getHexes(IGameState g, IStructure s) {
            ICoord c = s.getPos();
            return getHexes(g, c);
        }

        public List<ITile> getHexes(IGameState g, ICoord c) {
            List<ITile> tiles = new List<ITile>();
            if (g == null) {
                Console.WriteLine("state was null");
            } else if (g.getBoard() == null) {
                Console.WriteLine("board was null");
            } else if (c == null) Console.WriteLine("coord was null");

            if (checkUpDown(c.X(), c.Y()) == 2) {//its a down
                ITile t = g.getBoard().getTile(c.copyAdd(1, 0));
                if (t != null) tiles.Add(t);
                t = g.getBoard().getTile(c.copyAdd(-1, -1));
                if (t != null) tiles.Add(t);
                t = g.getBoard().getTile(c.copyAdd(0, 1));
                if (t != null) tiles.Add(t);
            } else {//its an up
                ITile t = g.getBoard().getTile(c.copyAdd(-1, 0));
                if (t != null) tiles.Add(t);
                t = g.getBoard().getTile(c.copyAdd(1, 1));
                if (t != null) tiles.Add(t);
                t = g.getBoard().getTile(c.copyAdd(0, -1));
                if (t != null) tiles.Add(t);
            }
            return tiles;
        }

        public List<IPlayer> getValidPlayersToStealFrom(IGameState g) {
            List<IPlayer> players = new List<IPlayer>();
            foreach (ISettlement s in g.getNeighbourSettlements(g.getBoard().getTile(g.getRobberPos()))) {
                if (!players.Contains(s.getOwner()) && s.getOwner().totalResources() > 0 && g.getCurrentPlayer() != s.getOwner()) players.Add(s.getOwner());
            }
            return players;
        }
        //has a knight to play and hasn't already played a card this turn, and the game 'state' is either waitplay (mid turn) or turnstart (before rolling dice)
        public bool canPlayKnight(IGameState st, IPlayer p) {
            return (p.getNumberDevCard(Const.KNIGHT) > 0
                && !st.hasPlayedDevCardThisTurn()
                && (st.getState() == Const.WAITPLAY || st.getState() == Const.TURNSTART));
        }

        public bool canPlayYearOfPlenty(IGameState st, IPlayer p) {
            return (p.getNumberDevCard(Const.YEAR_OF_PLENTY) > 0
                && !st.hasPlayedDevCardThisTurn()
                && (st.getState() == Const.WAITPLAY || st.getState() == Const.TURNSTART));
        }

        public bool canPlayMonopoly(IGameState st, IPlayer p) {
            return (p.getNumberDevCard(Const.MONOPOLY) > 0
                && !st.hasPlayedDevCardThisTurn()
                && (st.getState() == Const.WAITPLAY || st.getState() == Const.TURNSTART));
        }

        public bool canPlayRoadBuilding(IGameState st, IPlayer p) {
            return (p.getNumberDevCard(Const.ROAD_BUILDING) > 0
                && !st.hasPlayedDevCardThisTurn()
                && (st.getState() == Const.WAITPLAY || st.getState() == Const.TURNSTART)
                && p.numRoads() < 14);//REMOVE HARD CODE NUMBER LATER!
        }

        public int[] getCostRoad() {
            return new int[Const.NUM_OF_RESOURCES] { 1, 1, 0, 0, 0 };
        }

        public int[] getCostCity() {
            return new int[Const.NUM_OF_RESOURCES] { 0, 0, 0, 2, 3 };
        }

        public int[] getCostSettlement() {
            return new int[Const.NUM_OF_RESOURCES] { 1, 1, 1, 1, 0 };
        }

        public int[] getCostDevCard() {
            return new int[Const.NUM_OF_RESOURCES] { 0, 0, 1, 1, 1 };
        }

        public List<Tuple<int[], int[]>> getValidBankTrades(IGameState g, IPlayer p) {
            //if universal port owned then add all 3 to 1 possibilities
            //add all 4 to 1 possibilities
            //foreach special port add all 2 for 1 possibilities
            //return

            List<Tuple<int[], int[]>> trades = new List<Tuple<int[], int[]>>();

            int[] resources = p.getResources();
            int[] resConsts = new int[] { Const.BRICK, Const.LUMBER, Const.WOOL, Const.GRAIN, Const.ORE };

            List<IPort> ports = p.getPorts();
            List<int> portTypes = new List<int>();
            foreach (IPort port in ports) {
                if (!portTypes.Contains(port.getResourceType())) {
                    portTypes.Add(port.getResourceType());
                }
            }

            for (int resourceKey = 0; resourceKey < resources.Length; resourceKey++) {

                int resource = resources[resourceKey];

                //can you make a general 4:1 trade?
                if (resource >= 4) {
                    //yes, set up trade for all other resources
                    for (int x = 0; x < resConsts.Length; x++) {

                        //check not same resource
                        if (resourceKey != x) {
                            int[] tradeIn = new int[Const.NUM_OF_RESOURCES];
                            tradeIn[resourceKey] = 4;
                            int[] tradeOut = new int[Const.NUM_OF_RESOURCES];
                            tradeOut[x] = 1;

                            trades.Add(new Tuple<int[], int[]>(tradeIn, tradeOut));
                        }
                    }
                }

                //check for a direct resource port 2:1 
                if (portTypes.Contains(resourceKey) && resource >= 2) {
                    //yes, set up trade for all other resources
                    for (int x = 0; x < resConsts.Length; x++) {
                        //this could be made into a sub function to avoid repitition, possible refactoring later
                        //check not same resource
                        if (resourceKey != x) {
                            int[] tradeIn = new int[Const.NUM_OF_RESOURCES];
                            tradeIn[resourceKey] = 2;
                            int[] tradeOut = new int[Const.NUM_OF_RESOURCES];
                            tradeOut[x] = 1;

                            trades.Add(new Tuple<int[], int[]>(tradeIn, tradeOut));
                        }
                    }

                }

                //check for a universal port 3:1
                if (portTypes.Contains(Const.UNIVERSAL) && resource >= 3) {
                    //yes, set up trade for all other resources
                    for (int x = 0; x < resConsts.Length; x++) {

                        //check not same resource
                        if (resourceKey != x) {
                            int[] tradeIn = new int[Const.NUM_OF_RESOURCES];
                            tradeIn[resourceKey] = 3;
                            int[] tradeOut = new int[Const.NUM_OF_RESOURCES];
                            tradeOut[x] = 1;

                            trades.Add(new Tuple<int[], int[]>(tradeIn, tradeOut));
                        }
                    }

                }
            }

            return trades;
        }

        //given resources, returns all possible arrays of n/2 resources out of the n
        public List<int[]> getValidDiscards(IPlayer p) {
            discardCombos.Clear();//clears the list from nay previous attempts
            int[] resources = p.getResources();
            List<int> flattened = new List<int>();
            for (int i = 0; i < resources.Length; i++) {
                int count = resources[i];
                for (int j = 0; j < count; j++) {
                    flattened.Add(i);
                }
            }
            int n = flattened.Count / 2;
            combos(flattened.ToArray(), new int[n], 0, flattened.Count - 1, 0, n);
            List<int[]> unflattened = new List<int[]>();
            return discardCombos;
        }

        static bool isUniqueCombo(List<int[]> arr, int[] data) {
            foreach (int[] discard in arr) {
                if (discard[0] == data[0] && discard[1] == data[1] && discard[2] == data[2] && discard[3] == data[3] && discard[4] == data[4]) {
                    return false;
                }
            }
            return true;
        }

        private void combos(int[] array, int[] data, int start, int end, int index, int n) {
            if (index == n) {
                int[] unflattenedArray = new int[Const.NUM_OF_RESOURCES];
                foreach (int res in data) {
                    unflattenedArray[res]++;
                }
                if (isUniqueCombo(discardCombos, unflattenedArray)) {
                    discardCombos.Add(unflattenedArray);
                }
                return;
            }
            for (int i = start; i <= end && end - i + 1 >= n - index; i++) {
                data[index] = array[i];
                combos(array, data, i + 1, end, index + 1, n);
            }
        }

        private void calculateCombinations(List<int> arr, int len, int startPosition, int[] result, List<int[]> combinations) {
            if (len == 0) {
                if (!isRepeat(combinations, result)) {
                    combinations.Add((int[])result.Clone());
                }
                return;
            }
            for (int i = startPosition; i <= arr.Count - len; i++) {
                result[result.Length - len] = arr[i];
                calculateCombinations(arr, len - 1, i + 1, result, combinations);
            }
        }
        //returns an array of ints that each represent a resource type
        public int[] getResourceChoices() {
            return new int[Const.NUM_OF_RESOURCES] { Const.BRICK, Const.LUMBER, Const.WOOL, Const.GRAIN, Const.ORE };
        }
        //if the tooAdd list already exists in combinations then it returns true
        private bool isRepeat(List<int[]> combinations, int[] tooAdd) {
            foreach (int[] arr in combinations) {
                if (arr.SequenceEqual(tooAdd)) {
                    return true;
                }
            }
            return false;
        }
    }
}
