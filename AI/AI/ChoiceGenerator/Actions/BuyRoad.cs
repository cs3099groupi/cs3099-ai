﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class BuyRoad : BuildRoad {
        
		public override void print(CatanAI ai) {
            Console.WriteLine("BuyRoad: " + pos.toStr() + " " + pos2.toStr());
        }

		//run the buy road event
        public override void run(CatanAI ai, IGameState g) {
            IPlayer p = g.getCurrentPlayer();
            a.buildRoad(g, pos, pos2, p);
            int[] cost = r.getCostRoad();
            p.removeResources(cost);
            g.getBank().giveResources(cost);
        }

		//send buy road request to the server
        public override void send(CatanAI ai, NetworkManager nm) {
            Intergroup.Board.Point pnt1 = new Intergroup.Board.Point();
            Intergroup.Board.Point pnt2 = new Intergroup.Board.Point();
            pnt1.X = pos.X();
            pnt1.Y = pos.Y();
            pnt2.X = pos2.X();
            pnt2.Y = pos2.Y();
            Intergroup.Board.Edge edg = new Intergroup.Board.Edge();
            edg.A = pnt1;
            edg.B = pnt2;
            Intergroup.Request req = new Intergroup.Request { BuildRoad = edg };
            nm.sendRequest(req);
        }

        public BuyRoad(ICoord c, ICoord c2) {
            this.pos = c;
            this.pos2 = c2;
        }

    }
}
