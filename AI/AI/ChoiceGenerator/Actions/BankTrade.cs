﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class BankTrade : Action {

        public int[] resourcesIn;
        public int[] resourcesOut;

		//print the bank trade exchange
        public override void print(CatanAI ai) {
            string[] resourceInString = resourcesIn.Select(x => x.ToString()).ToArray();
            string[] resourceOutString = resourcesOut.Select(x => x.ToString()).ToArray();
            String bankTradeIn = string.Join(",", resourceInString);
            String bankTradeOut = string.Join(",", resourceOutString);

            Console.WriteLine("BankTrade in: " + bankTradeIn + ", out:" + bankTradeOut);
        }

		//run the action on the gamestate
        public override void run(CatanAI ai, IGameState g) {
            a.bankTrade(g, resourcesIn, resourcesOut, g.getCurrentPlayer());
        }

		//send the bank trade details (resources in and out) through the network manager,
        public override void send(CatanAI ai, NetworkManager nm) {
            Intergroup.Request req = new Intergroup.Request {
                InitiateTrade = new Intergroup.Trade.Kind {
                    Bank = new Intergroup.Trade.WithBank {
                        Offering = new Intergroup.Resource.Counts {
                            Brick = resourcesIn[Const.BRICK],
                            Ore = resourcesIn[Const.ORE],
                            Grain = resourcesIn[Const.GRAIN],
                            Lumber = resourcesIn[Const.LUMBER],
                            Wool = resourcesIn[Const.WOOL]

                        },
                        Wanting = new Intergroup.Resource.Counts {
                            Brick = resourcesOut[Const.BRICK],
                            Ore = resourcesOut[Const.ORE],
                            Grain = resourcesOut[Const.GRAIN],
                            Lumber = resourcesOut[Const.LUMBER],
                            Wool = resourcesOut[Const.WOOL]
                        }
                    }
                }
            };

            nm.sendRequest(req);
        }

        public BankTrade(int[] resourcesIn, int[] resourcesOut) {
            this.resourcesIn = resourcesIn;
            this.resourcesOut = resourcesOut;
        }
    }
}
