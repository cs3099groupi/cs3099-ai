﻿using System;

namespace AI {
    class RejectPlayerTrade : Action {
        public override void print(CatanAI ai) {
            Console.WriteLine("RejectPlayerTrade");
        }

		//set the state to wait play - back to pre-trade state
        public override void run(CatanAI ai, IGameState g) {
            g.setState(Const.WAITPLAY);
        }

		//Send the reject trade request to the server
        public override void send(CatanAI ai, NetworkManager nm) {
            Intergroup.Request req = new Intergroup.Request {
                SubmitTradeResponse = Intergroup.Trade.Response.Reject
            };

            nm.sendRequest(req);
        }
    }
}
