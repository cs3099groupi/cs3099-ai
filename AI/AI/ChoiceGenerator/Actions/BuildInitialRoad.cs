﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class BuildInitialRoad : BuildRoad {
        public override void print(CatanAI ai) {
            Console.WriteLine("BuildInitialRoad " + pos.toStr() + " " + pos2.toStr());
        }

		//run the initial road build onto the game state
        public override void run(CatanAI ai, IGameState g) {
            a.buildRoad(g, pos, pos2, g.getCurrentPlayer());
            if (g.getCurrentPlayer().points() == 1) {//aka first road
                if (g.getCurrentPlayer().getID() == g.getPlayers().Count - 1) { //last player to build before going back across again
                    //do nothing because we are going to run initial settlement with this player again
                } else {
                    g.iterateCurrentPlayer();
                }
                g.setState(Const.INITIALSETTLEMENT);
            } else {//built second road
                if (g.getCurrentPlayer().getID() == 0) {
                    //last Player to build initial settlements and roads
                    g.setState(Const.TURNSTART);
                } else {
                    //set to previous player, go backwards round table
                    g.decrementCurrentPlayer();
                    g.setState(Const.INITIALSETTLEMENT);
                }
            }
        }

		//send the initial build road request to the server
        public override void send(CatanAI ai, NetworkManager nm) {
            Intergroup.Board.Point pnt1 = new Intergroup.Board.Point();
            Intergroup.Board.Point pnt2 = new Intergroup.Board.Point();
            pnt1.X = pos.X();
            pnt1.Y = pos.Y();
            pnt2.X = pos2.X();
            pnt2.Y = pos2.Y();
            Intergroup.Board.Edge edg = new Intergroup.Board.Edge();
            edg.A = pnt1;
            edg.B = pnt2;
            Intergroup.Request req = new Intergroup.Request { BuildRoad = edg };
            nm.sendRequest(req);
        }

        public BuildInitialRoad(ICoord c, ICoord c2) {
            this.pos = c;
            this.pos2 = c2;
        }
    }
}
