﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class BuildRoadBuildingRoad : BuildRoad {
        
		public override void print(CatanAI ai) {
            Console.WriteLine("BuildRoadBuildingRoad" + pos.toStr() + " " + pos2.toStr());
        }

		//build road on the set positions
        public override void run(CatanAI ai, IGameState g) {
            IPlayer p = g.getCurrentPlayer();
            a.buildRoad(g, pos, pos2, p);
        }

		//send the road build request to the network manager 
        public override void send(CatanAI ai, NetworkManager nm) {
            Intergroup.Board.Point pnt1 = new Intergroup.Board.Point();
            Intergroup.Board.Point pnt2 = new Intergroup.Board.Point();
            pnt1.X = pos.X();
            pnt1.Y = pos.Y();
            pnt2.X = pos2.X();
            pnt2.Y = pos2.Y();
            Intergroup.Board.Edge edg = new Intergroup.Board.Edge();
            edg.A = pnt1;
            edg.B = pnt2;
            Intergroup.Request req = new Intergroup.Request { BuildRoad = edg };
            nm.sendRequest(req);
        }

        public BuildRoadBuildingRoad(ICoord c, ICoord c2) {
            pos = c;
            pos2 = c2;
        }
    }
}
