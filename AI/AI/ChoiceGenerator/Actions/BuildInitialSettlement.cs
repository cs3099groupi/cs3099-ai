﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class BuildInitialSettlement : BuildStructure {
        
		public override void print(CatanAI ai) {
            Console.WriteLine("BuildInitialSettlement " + pos.toStr());
        }

		//run the initial settlement onto the game state
        public override void run(CatanAI ai, IGameState g) {
            IPlayer p = g.getCurrentPlayer();
            a.buildSettlement(g, pos, p);
            if (p.points() == 2) {//the second settlement
                foreach (Tile t in r.getHexes(g, g.getLastBuiltSettlement())) {
                    if (t.getResourceType() != Const.DESERT) {
                        p.GiveResource(t.getResourceType(), 1);//give resources of adjacent spots
                        g.getBank().removeResource(t.getResourceType(), 1);
                    }
                }
            }
            g.setState(Const.INITIALROAD);
        }

		//send the initial settlement build through the network manager
        public override void send(CatanAI ai, NetworkManager nm) {
            Intergroup.Board.Point pnt = new Intergroup.Board.Point();
            pnt.X = pos.X();
            pnt.Y = pos.Y();
            Intergroup.Request req = new Intergroup.Request { BuildSettlement = pnt };
            nm.sendRequest(req);
        }

        public BuildInitialSettlement(ICoord c) {
            this.pos = c;
        }
    }
}
