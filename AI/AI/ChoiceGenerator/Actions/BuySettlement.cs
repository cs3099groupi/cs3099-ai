﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class BuySettlement : BuildStructure {
        
		public override void print(CatanAI ai) {
            Console.WriteLine("BuySettlement: " + pos.toStr());
        }

		//build the settlement on the game state
        public override void run(CatanAI ai, IGameState g) {
            IPlayer p = g.getCurrentPlayer();
            a.buildSettlement(g, pos, p);
            int[] cost = r.getCostSettlement();
            p.removeResources(cost);
            g.getBank().giveResources(cost);
        }


        public BuySettlement(ICoord c) {
            this.pos = c;
        }

		//send the build settlement request to the server
        public override void send(CatanAI ai, NetworkManager nm) {
            Intergroup.Board.Point pnt = new Intergroup.Board.Point();
            pnt.X = pos.X();
            pnt.Y = pos.Y();
            Intergroup.Request req = new Intergroup.Request { BuildSettlement = pnt };
            nm.sendRequest(req);
        }
    }
}
