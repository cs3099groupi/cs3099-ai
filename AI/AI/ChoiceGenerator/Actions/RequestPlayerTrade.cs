﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class RequestPlayerTrade : Action {

        int[] offer;
        int[] want;
        IPlayer target;

        public RequestPlayerTrade(int[] offer, int[] want, IPlayer target) {
            this.offer = offer;
            this.want = want;
            this.target = target;
        }

		//Set the state to wait for trade response
        public override void run(CatanAI ai, IGameState g) {
            g.setState(Const.AWAITINGTRADERESPONSE);
        }

        public override void print(CatanAI ai) {
            Console.WriteLine("Request Player trade: ");
            //Console.WriteLine("Instigator" + instigatorID);
            //Console.WriteLine("Target" + targetID);
        }

		//Encapsulate player trade resources (offtering and wanting) into 
		//Intergroup format, and send it to the server
        public override void send(CatanAI ai, NetworkManager nm) {

            Intergroup.Request req = new Intergroup.Request {
                InitiateTrade = new Intergroup.Trade.Kind {

                    Player = new Intergroup.Trade.WithPlayer {

                        Offering = new Intergroup.Resource.Counts {
                            Brick = offer[Const.BRICK],
                            Ore = offer[Const.ORE],
                            Wool = offer[Const.WOOL],
                            Lumber = offer[Const.LUMBER],
                            Grain = offer[Const.GRAIN]
                        },
                        Wanting = new Intergroup.Resource.Counts {
                            Brick = want[Const.BRICK],
                            Ore = want[Const.ORE],
                            Wool = want[Const.WOOL],
                            Lumber = want[Const.LUMBER],
                            Grain = want[Const.GRAIN]
                        },
                        Other = new Intergroup.Board.Player {
                            Id = nm.getProtobufPlayerID(target.getID())
                        }
                    }
                }
            };

            nm.sendRequest(req);
        }
    }
}
