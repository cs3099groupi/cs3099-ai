﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class RequestDiceRoll : Action {
        
		//run the dice roll sent by the server
		public override void run(CatanAI ai, IGameState g) {
            a.requestDiceRoll(g);
        }

        public override void print(CatanAI ai) {
            Console.WriteLine("RequestDiceRoll");
        }

		//request the dice roll from the server
        public override void send(CatanAI ai, NetworkManager nm) {
            Intergroup.Request req = new Intergroup.Request {
                RollDice = new Intergroup.Empty()
            };
            nm.sendRequest(req);
        }
    }
}
