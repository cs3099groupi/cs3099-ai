﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    abstract class Action : IAction {

		//this is a class to encapsulate any single action made in catan
        //a representation of any action made by a player in catan.
        //used by the ai to store the path it took to create a new game state. 
        public static IRules r = new Rules();
        public static IActions a = new Actions();

		//applies it to the game state
        public abstract void run(CatanAI ai, IGameState g);

		//print action
        public abstract void print(CatanAI ai);

		//send the action through the network manager
        public abstract void send(CatanAI ai, NetworkManager nm);

    }
}
