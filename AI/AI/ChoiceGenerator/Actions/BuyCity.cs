﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class BuyCity : BuildStructure {
        public override void print(CatanAI ai) {
            Console.WriteLine("BuyCity" + pos.toStr());
        }

		//build the city in the game state
        public override void run(CatanAI ai, IGameState g) {
            IPlayer p = g.getCurrentPlayer();
            a.buildCity(g, pos, p);
            int[] cost = r.getCostCity();
            p.removeResources(cost);
            g.getBank().giveResources(cost);
        }

		//send build city request to the server
        public override void send(CatanAI ai, NetworkManager nm) {
            Intergroup.Board.Point pnt = new Intergroup.Board.Point();
            pnt.X = pos.X();
            pnt.Y = pos.Y();
            Intergroup.Request req = new Intergroup.Request { BuildCity = pnt };
            nm.sendRequest(req);
        }

        public BuyCity(ICoord c) {
            this.pos = c;
        }

    }
}
