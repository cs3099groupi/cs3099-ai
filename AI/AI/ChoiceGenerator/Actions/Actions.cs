﻿using AI.ChoiceGenerator.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class Actions : IActions {
        static IRules rules = new Rules();

		//Performs the monopolo resolution on the game state
        public void monopolyResolution(IGameState st, int resource, Dictionary<IPlayer, int> steals, IPlayer monopP) {
            //for each player in the steal, remove their resources and give to monopoly player
            foreach (KeyValuePair<IPlayer, int> steal in steals) {
                steal.Key.RemoveResource(resource, steal.Value);
                monopP.GiveResource(resource, steal.Value);
            }

            st.setState(st.getState() == Const.CHOOSINGMONOPOLY ? Const.WAITPLAY : Const.TURNSTART);
        }

		//Perform the bank trade on the game state 
        public void bankTrade(IGameState st, int[] resourcesIn, int[] resourcesOut, IPlayer p) {
            //ResourcesIn = resources given to the bank, resources removed from the player
            //ResourcesOut = resources removed from the bank, resources given to the player
            IBank bank = st.getBank();

            //Bank exchange
            bank.giveResources(resourcesIn);
            bank.removeResources(resourcesOut);

            p.removeResources(resourcesIn);

            p.giveResources(resourcesOut);
        }

		//Perform the build city on the game state
        public void buildCity(IGameState st, ICoord pos, IPlayer p) {
            ISettlement stl = st.getSettlement(pos);
            stl.upgradeToCity();
            p.addPoints(1);
        }

		//Perform the build road on the game state, updates the longest road
        public void buildRoad(IGameState st, ICoord pos1, ICoord pos2, IPlayer p) {
            IRoad newRd = new Road(pos1, pos2);
            st.removeFreeRoad(new Tuple<ICoord, ICoord>(pos1, pos2));
            st.addStructure(newRd);
            p.addStructure(newRd);
            newRd.setOwner(p);
            checkLongestRoad(st, p, newRd);
            switch (st.getState()) {
                case Const.FIRSTROADBUILDING:
                    st.setState(Const.SECONDROADBUILDING);
                    break;
                case Const.SECONDROADBUILDING:
                    st.setState(Const.WAITPLAY);
                    break;
                case Const.FIRSTROADBUILDINGBEFOREROLLINGTHEDICE:
                    st.setState(Const.SECONDROADBUILDINGBEFOREROLLINGTHEDICE);
                    break;
                case Const.SECONDROADBUILDINGBEFOREROLLINGTHEDICE:
                    st.setState(Const.TURNSTART);
                    break;
                case Const.WAITPLAY:
                    st.setState(Const.WAITPLAY);//not necessary but here for readability because im super tired.
                    break;
            }
        }

		//Peform the build settlement on the gamestate, checking its impact on the game i.e longest road
        public void buildSettlement(IGameState st, ICoord pos, IPlayer p) {
            ISettlement newStl = new Settlement(pos);
            st.removeFreeSettlementSpots(pos);
            st.addStructure(newStl);
            p.addStructure(newStl);
            newStl.setOwner(p);
            p.addPoints(1);
            checkBrokenSettlement(pos, p, st);

            //check for port
            IBoard b = st.getBoard();
            IPort port = b.getPort(pos);
            if (port != null) {
                p.addPort(port);
                port.setOwner(p);
            }
            st.setLastBuildSettlement(newStl);
        }


        public void endTurn(IGameState st) {
            //iterate current player and set the state to pre-turn
            st.iterateCurrentPlayer();
            Console.WriteLine("Current player is now: " + st.getCurrentPlayer().getID());
            st.setState(Const.TURNSTART);//now the options are to roll dice or play dev card etc.
            foreach (IPlayer p in st.getPlayers()) {
                p.setHasDiscarded(true);
            }
            st.setNotPlayedDevCardThisTurn();
        }

        //internal functions
        //*****************************************************************************************************


		//checks for longest road and applies it if appropriate
        static void checkLongestRoad(IGameState state, IPlayer player, IRoad road) {
            int x1 = road.getPos().X();
            int y1 = road.getPos().Y();
            int x2 = road.getPos2().X();
            int y2 = road.getPos2().Y();
            //this makes sure links at both ends are taken
            List<IRoad> visitedRoads = new List<IRoad>();   //stops following loops
            int forwardMaxDepth = rules.checkDirection(player, x1, x2, y1, y2, visitedRoads, state);
            int backwardMaxDepth = rules.checkDirection(player, x2, x1, y2, y1, visitedRoads, state);
            int longest = forwardMaxDepth + backwardMaxDepth + 1;   //need to include the road itself
            if (longest > player.getLongestRoad()) {
                player.SetLongestRoad(longest);
            }
            if (longest > 4 && longest > state.getLongestRoad()) {
                if (state.getLongestRoadPlayer() != null) {
                    state.getLongestRoadPlayer().removePoints(2);
                }
                state.setLongestRoad(longest);
                state.setLongestRoadPlayer(player);
                player.addPoints(2);
            }
        }

        //on creation of a settlement checks whether it is breaking the longest road
        static public void checkBrokenSettlement(ICoord pos, IPlayer player, IGameState st) {
            int type = rules.checkUpDown(pos.X(), pos.Y());
            IPlayer nullP = new Player(0, 0, 0, 0);
            IPlayer player1 = nullP;
            IPlayer player2 = nullP;
            IPlayer player3 = nullP;
            //checks ownership of all adjacent roads based on the directions possible
            if (type == 2) {
                //this is a down
                if (st.getRoad(pos, pos.copyAdd(1, 1)) != null)
                    player1 = st.getRoad(pos, pos.copyAdd(1, 1)).getOwner();
                if (st.getRoad(pos, pos.copyAdd(-1, 0)) != null)
                    player2 = st.getRoad(pos, pos.copyAdd(-1, 0)).getOwner();
                if (st.getRoad(pos, pos.copyAdd(0, -1)) != null)
                    player3 = st.getRoad(pos, pos.copyAdd(0, -1)).getOwner();
            } else {

                if (st.getRoad(pos, pos.copyAdd(1, 0)) != null)
                    player1 = st.getRoad(pos, pos.copyAdd(1, 0)).getOwner();
                if (st.getRoad(pos, pos.copyAdd(-1, -1)) != null)
                    player2 = st.getRoad(pos, pos.copyAdd(-1, -1)).getOwner();
                if (st.getRoad(pos, pos.copyAdd(0, 1)) != null)
                    player3 = st.getRoad(pos, pos.copyAdd(0, 1)).getOwner();
            }

            //if two road are owned by the same person then the settlement is between them
            if ((player1 != null && (player1 == player2 || player1 == player3) && player1 != player) || (player2 != null && player2 == player3 && player2 != player)) {
                if (player2 == player3) {
                    player1 = player2; // this stores the player they're interrupting
                }
                //if the roads that are the same don't belong to the player
                if (player1 != player) {// && player1 != null) {
                                        //reset longest road fields as longest road has been broken


                    st.setLongestRoad(0);
                    st.setLongestRoadPlayer(null);

                    //set player 1 to 0
                    resetPlayersLongest(player1, st);
                    IPlayer oldLongestPlayer = player1;
                    int oldLongest = oldLongestPlayer.getLongestRoad();
                    if (st.getLongestRoadPlayer() != null) {
                        st.getLongestRoadPlayer().removePoints(2);
                    }
                    //gives the longest road to any elligible player before checking if previous owner of the card had the longest road
                    foreach (IPlayer p in st.getPlayers()) {
                        if (p.getLongestRoad() > st.getLongestRoad() && p.getLongestRoad() > oldLongest && p.getLongestRoad() > 4) {
                            st.setLongestRoad(p.getLongestRoad());
                            if (st.getLongestRoadPlayer() != null)
                                st.getLongestRoadPlayer().removePoints(2);
                            p.addPoints(2);
                            st.setLongestRoadPlayer(p);
                            oldLongest = p.getLongestRoad();
                        } else if (p.getLongestRoad() == st.getLongestRoad() && p != oldLongestPlayer && oldLongestPlayer != st.getLongestRoadPlayer()) {
                            if (st.getLongestRoadPlayer() != null) {
                                st.getLongestRoadPlayer().removePoints(2);
                                st.setLongestRoadPlayer(null);
                                st.setLongestRoad(0);
                            }
                        }
                    }
                }
            }
        }

        static public void resetPlayersLongest(IPlayer player, IGameState st) {
            player.SetLongestRoad(0);
            player.removePoints(2);
            List<IRoad> roads = player.getRoads();
            //for each road that the player has built checks if it is the longest road
            foreach (IRoad i in roads) {
                checkLongestRoad(st, player, i);
            }
        }

        public static void checkLargestArmy(IGameState g, IPlayer p) {
            if (p.numKnightsPlayed() > g.getLargestArmy() && p.numKnightsPlayed() >= 3) {
                g.setLargestArmy(p.numKnightsPlayed());
                if (g.getLargestArmyPlayer() != null) {
                    g.getLargestArmyPlayer().removePoints(2);
                }
                g.setLargestArmyPlayer(p);
                p.addPoints(2);
            }
        }

		//set the state to requestdiceroll to make the state become in the process of waiting for external change (the host)
        public void requestDiceRoll(IGameState st) {
            st.setState(Const.AWAITINGDICEROLL);
        }

		//set the state to awaiting dev card building 
        public void requestBuyDevCard(IGameState st, IPlayer p) {
            st.setState(Const.AWAITINGDEVCARDBUYING);
        }

		//set the state to awaiting trade response 
        public void initiateTradeRequest(IGameState st) {
            st.setState(Const.AWAITINGTRADERESPONSE);
        }

		//set the state to awaiting end turn
        public void requestEndTurn(IGameState st) {
            st.setState(Const.AWAITINGENDTURN);
        }

		//give the player a given dev card
        public void giveDevCard(IGameState st, int devCard, IPlayer p) {
            if (devCard != Const.UNKNOWN_DEVCARD) st.getBank().removeDevCard(devCard, 1);
            p.giveDevCard(devCard);
        }

		//remove dev card bought this turn from the player
        public void removeDevCardBoughtThisTurn(IGameState st, int devCard, IPlayer p) {
            p.removeDevCardBoughtThisTurn(devCard);
        }

		//give a particular player a particular dev card
        public void giveDevCardBoughtThisTurn(IGameState st, int devCard, IPlayer p) {
            p.giveDevCardBoughtThisTurn(devCard);
        }

		//play a a given dev card on the game state
        public void playDevCard(IGameState st, int devCard, IPlayer p) {
            bool wasKnown = false;
            if (p.getNumberDevCard(devCard) > 0) {
                wasKnown = true;
                p.removeDevCard(devCard);
            } else {
                if (p.getNumberDevCard(Const.UNKNOWN_DEVCARD) > 0) {
                    p.removeDevCard(Const.UNKNOWN_DEVCARD);
                } else {
                    throw new InvalidOperationException();
                }
            }
            if (!wasKnown) st.getBank().removeDevCard(devCard, 1);
            switch (devCard) {
                case Const.KNIGHT:
                    p.addKnightToArmy();
                    checkLargestArmy(st, p);
                    st.setState((st.getState() == Const.WAITPLAY) ? Const.CHOOSINGROBBERPLACEMENT : Const.AWAITINGROBBERPLACEMENTDUETOKNIGHTBEINGPLAYEDBEFOREROLLINGDICE);
                    break;
                case Const.MONOPOLY:
                    st.setState((st.getState() == Const.WAITPLAY) ? Const.CHOOSINGMONOPOLY : Const.CHOOSINGMONOPOLYBEFOREROLLINGTHEDICE);
                    break;
                case Const.YEAR_OF_PLENTY:
                    st.setState((st.getState() == Const.WAITPLAY) ? Const.FIRSTYEAROFPLENTY : Const.FIRSTYEAROFPLENTYBEFOREROLLINGTHEDICE);
                    break;
                case Const.ROAD_BUILDING:
                    st.setState((st.getState() == Const.WAITPLAY) ? Const.FIRSTROADBUILDING : Const.FIRSTROADBUILDINGBEFOREROLLINGTHEDICE);
                    break;
                case Const.VICTORY_POINT:
                    //check victory??!?!
                    //this should not be run because victory points are never 'played' they are assumed until the game is over.
                    throw new InvalidOperationException();
            }
            st.setPlayedDevCardThisTurn();
        }

		//move the robber in the game state
        public void moveRobber(IGameState st, ICoord pos) {
            st.setRobberPos(pos);
        }

		//spawn all the free roads for a particular state
        public void spawnFreeRoads(IGameState st) {
            List<Tuple<ICoord, ICoord>> spots = new List<Tuple<ICoord, ICoord>>();
            for (int x = -5; x < 6; x++) {
                for (int y = -5; y < 6; y++) {
                    if (rules.isCorner(x, y)) {
                        if (rules.isCorner(x, y + 1)) spots.Add(new Tuple<ICoord, ICoord>(new Coord(x, y), new Coord(x, y + 1)));
                        if (rules.isCorner(x, y - 1)) spots.Add(new Tuple<ICoord, ICoord>(new Coord(x, y), new Coord(x, y - 1)));
                        if (rules.isCorner(x - 1, y)) spots.Add(new Tuple<ICoord, ICoord>(new Coord(x, y), new Coord(x - 1, y)));
                        if (rules.isCorner(x + 1, y)) spots.Add(new Tuple<ICoord, ICoord>(new Coord(x, y), new Coord(x + 1, y)));
                        if (rules.isCorner(x + 1, y + 1)) spots.Add(new Tuple<ICoord, ICoord>(new Coord(x, y), new Coord(x + 1, y + 1)));
                    }
                }
            }
            List<Tuple<ICoord, ICoord>> spotsToRemove = new List<Tuple<ICoord, ICoord>>();
            List<Tuple<ICoord, ICoord>> spotsChecked = new List<Tuple<ICoord, ICoord>>();

            foreach (Tuple<ICoord, ICoord> spotToRemove in spots) {
                foreach (Tuple<ICoord, ICoord> spotChecked in spotsChecked) {
                    if ((spotToRemove.Item1.isEqual(spotChecked.Item1) && spotToRemove.Item2.isEqual(spotChecked.Item2)) || (spotToRemove.Item2.isEqual(spotChecked.Item1) && spotToRemove.Item1.isEqual(spotChecked.Item2))) {
                        spotsToRemove.Add(spotToRemove);
                        break;
                    }
                }
                spotsChecked.Add(spotToRemove);
            }
            foreach (Tuple<ICoord, ICoord> spot in spotsToRemove) {
                Console.WriteLine("removing duplicate road!");
                spots.Remove(spot);
            }
            foreach (Tuple<ICoord, ICoord> spot in spots) {
                Console.WriteLine("freeRoad generated at: " + spot.Item1.toStr() + " " + spot.Item2.toStr());
            }
            st.setFreeRoadSpots(spots);
        }

        public void spawnBlankTiles(IBoard b) {
            List<ITile> all = new List<ITile>();
            for (int x = -6; x <= 6; x++) {
                for (int y = -6; y <= 6; y++) {
                    if (rules.isTile(x, y)) {
                        all.Add(new Tile(-1, -1, new Coord(x, y)));
                    }
                }
            }
            b.setTiles(all);
        }

		//spawn all the free settlement for a particular state
        public void spawnFreeSettlements(IGameState st) {
            List<ICoord> freeSpots = new List<ICoord>();
            for (int x = -5; x <= 5; x++) {
                for (int y = -5; y <= 5; y++) {
                    if (rules.isCorner(x, y)) {
                        freeSpots.Add(new Coord(x, y));
                    }
                }
            }
            st.setFreeSettlementSpots(freeSpots);
        }

        public IGameState spawnEmptyGameState() {
            IGameState g = new GameState();//creates a game state
            g.SetBoard(new Board());//creates and empty board
            g.setBank(new Bank(19, 19, 19, 19, 19, 14, 2, 2, 2, 5, 25));
            spawnBlankTiles(g.getBoard());//adds empty tiles to the empty board
            return g;
        }
    }
}
