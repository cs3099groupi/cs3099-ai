﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class Discard : Action {

        public int[] resourcesToDiscard;
        int playerID;
        public override void print(CatanAI ai) {
            Console.WriteLine("Discard by player " + playerID);
        }

		//Run the discard on the gamestate
        public override void run(CatanAI ai, IGameState g) {
            IPlayer p = g.getPlayer(playerID);
            IBank b = g.getBank();
            p.removeResources(resourcesToDiscard);
            p.setHasDiscarded(true);
            ((Player)p).DiscardRequested = false;
            b.giveResources(resourcesToDiscard);
            g.setState(Const.CHOOSINGROBBERPLACEMENT);
            foreach (IPlayer pl in g.getPlayers()) {
                if (!pl.hasDiscarded() && pl.totalResources() > 7) {
                    g.setState(Const.AWAITDISCARD);
                }
            }
        }

		//Send the discard request to the server, with resources to discard
        public override void send(CatanAI ai, NetworkManager nm) {
            Player p = (Player)ai.theGameState.getPlayer(playerID);
            p.DiscardRequested = true;

            Intergroup.Request req = new Intergroup.Request {
                DiscardResources = new Intergroup.Resource.Counts {
                    Brick = resourcesToDiscard[Const.BRICK],
                    Ore = resourcesToDiscard[Const.ORE],
                    Grain = resourcesToDiscard[Const.GRAIN],
                    Lumber = resourcesToDiscard[Const.LUMBER],
                    Wool = resourcesToDiscard[Const.WOOL]
                }
            };
            nm.sendRequest(req);
        }

        public Discard(int[] ress, int playerID) {
            resourcesToDiscard = ress;
            this.playerID = playerID;
        }
    }
}
