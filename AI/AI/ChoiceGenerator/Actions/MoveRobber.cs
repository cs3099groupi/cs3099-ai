﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class MoveRobber : Action {
        
		public ICoord pos;

		/* move the robber in the game state,
		 * change the state appropriately based on
		 * the reason for robber movement e.g. knight played
		 */
        public override void run(CatanAI ai, IGameState g) {
            List<ISettlement> stls = g.getNeighbourSettlements(g.getBoard().getTile(pos));
            int stateToStealIn = Const.RESOURCETOSTEAL;
            if (g.getState() == Const.AWAITINGROBBERPLACEMENTDUETOKNIGHTBEINGPLAYEDBEFOREROLLINGDICE) {
                stateToStealIn = Const.RESOURCETOSTEALPRETURN;
            }
            if (stls.Count > 0) {//unneccessary?
                foreach (ISettlement s in stls) {
                    if (s.getOwner() != g.getCurrentPlayer() && s.getOwner().totalResources() > 0)
                        g.setState(stateToStealIn);
                }
            }
            if (g.getState() != stateToStealIn) {
                if (g.getState() == Const.AWAITINGROBBERPLACEMENTDUETOKNIGHTBEINGPLAYEDBEFOREROLLINGDICE) {
                    g.setState(Const.TURNSTART);
                } else {
                    g.setState(Const.WAITPLAY);
                }
            }
            a.moveRobber(g, pos);
        }

        public override void print(CatanAI ai) {
            Console.WriteLine("MoveRobber to " + pos.toStr());
        }

		//send the move robber event to the server
        public override void send(CatanAI ai, NetworkManager nm) {

            Intergroup.Request req = new Intergroup.Request {
                MoveRobber = new Intergroup.Board.Point {
                    X = pos.X(),
                    Y = pos.Y()
                }
            };
            nm.sendRequest(req);
        }

        public MoveRobber(ICoord c) {
            pos = c;
        }

    }
}
