﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class EndTurn : Action {

        public override void run(CatanAI ai, IGameState g) {
            IPlayer p = g.getCurrentPlayer();
            a.requestEndTurn(g);
        }

        public override void print(CatanAI ai) {
            Console.WriteLine("EndTurn");
        }

		//send end turn request to the server
        public override void send(CatanAI ai, NetworkManager nm) {
            Intergroup.Request req = new Intergroup.Request {
                EndTurn = new Intergroup.Empty()
            };
            nm.sendRequest(req);
        }
    }
}
