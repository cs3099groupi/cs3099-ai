﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class ChooseResource : Action {

        public override void print(CatanAI ai) {
            Console.WriteLine("Choose Resource: " + res);
        }
        public int res;

        public override void run(CatanAI ai, IGameState g) {
            IPlayer p = g.getCurrentPlayer();
            IBank b = g.getBank();
            switch (g.getState()) {
                case Const.CHOOSINGMONOPOLY:
                    g.setState(Const.AWAITINGMONOPOLY);
                    break;
                case Const.FIRSTYEAROFPLENTY:
                    p.GiveResource(res, 1);
                    b.removeResource(res, 1);
                    g.setState(Const.SECONDYEAROFPLENTY);
                    break;
                case Const.SECONDYEAROFPLENTY:
                    p.GiveResource(res, 1);
                    b.removeResource(res, 1);
                    g.setState(Const.WAITPLAY);
                    break;
                case Const.CHOOSINGMONOPOLYBEFOREROLLINGTHEDICE:
                    g.setState(Const.AWAITINGMONOPOLYPRETURN);
                    break;
                case Const.FIRSTYEAROFPLENTYBEFOREROLLINGTHEDICE:
                    p.GiveResource(res, 1);
                    b.removeResource(res, 1);
                    g.setState(Const.SECONDYEAROFPLENTYBEFOREROLLINGTHEDICE);
                    break;
                case Const.SECONDYEAROFPLENTYBEFOREROLLINGTHEDICE:
                    p.GiveResource(res, 1);
                    b.removeResource(res, 1);
                    g.setState(Const.TURNSTART);
                    break;
            }
        }

		//send the choosen resource to the server
        public override void send(CatanAI ai, NetworkManager nm) {
            Intergroup.Resource.Kind theres = Intergroup.Resource.Kind.Generic;
            switch (res) {
                case Const.BRICK:
                    theres = Intergroup.Resource.Kind.Brick;
                    break;
                case Const.ORE:
                    theres = Intergroup.Resource.Kind.Ore;
                    break;
                case Const.LUMBER:
                    theres = Intergroup.Resource.Kind.Lumber;
                    break;
                case Const.WOOL:
                    theres = Intergroup.Resource.Kind.Wool;
                    break;
                case Const.GRAIN:
                    theres = Intergroup.Resource.Kind.Grain;
                    break;
            }
            Intergroup.Request req = new Intergroup.Request {
                ChooseResource = theres
            };
            nm.sendRequest(req);
        }

        public ChooseResource(int res) {
            this.res = res;
        }
    }
}
