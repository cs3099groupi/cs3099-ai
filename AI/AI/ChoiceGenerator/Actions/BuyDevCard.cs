﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class BuyDevCard : Action {
        
		public override void print(CatanAI ai) {
            Console.WriteLine("BuyDevCard");
        }


        public override void run(CatanAI ai, IGameState g) {
            a.requestBuyDevCard(g, g.getCurrentPlayer());
        }

		//send buy dev card request to the server
        public override void send(CatanAI ai, NetworkManager nm) {

            Intergroup.Request req = new Intergroup.Request {
                BuyDevCard = new Intergroup.Empty()
            };
            nm.sendRequest(req);
        }
    }
}
