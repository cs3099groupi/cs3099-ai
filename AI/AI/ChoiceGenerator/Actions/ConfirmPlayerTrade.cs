﻿using System;
using System.Collections.Generic;

namespace AI {
    class ConfirmPlayerTrade : Action {

        private int instigatorID;
        private int targetID;


        private IPlayer instigator;//the player offering the trade
        private IPlayer target;//player who the trade is offered to

        private int[] offer = new int[Const.NUM_OF_RESOURCES];
        private int[] want = new int[Const.NUM_OF_RESOURCES];


        public override void print(CatanAI ai) {
            Console.WriteLine("Accepting Player Trade");
            Console.WriteLine("Instigator" + instigatorID);
            Console.WriteLine("Target" + targetID);

        }

		//Send the player trade confirmation 
        public override void send(CatanAI ai, NetworkManager nm) {
            Intergroup.Request req = new Intergroup.Request {
                SubmitTradeResponse = Intergroup.Trade.Response.Accept
            };

            nm.sendRequest(req);
        }

		//Run the trade confirmation on the game state
        public override void run(CatanAI ai, IGameState g) {

            instigator = g.getPlayer(instigatorID);
            target = g.getPlayer(targetID);
            instigator.removeResources(offer);
            instigator.giveResources(want);
            target.removeResources(want);
            target.giveResources(offer);
            g.setState(Const.WAITPLAY);
        }

		//Assigns the resources involved in the trade (offer and want)
        public ConfirmPlayerTrade(Intergroup.Event ev, NetworkManager nm) {

            offer[Const.BRICK] = ev.PlayerTradeInitiated.Offering.Brick;
            offer[Const.LUMBER] = ev.PlayerTradeInitiated.Offering.Lumber;
            offer[Const.WOOL] = ev.PlayerTradeInitiated.Offering.Wool;
            offer[Const.ORE] = ev.PlayerTradeInitiated.Offering.Ore;
            offer[Const.GRAIN] = ev.PlayerTradeInitiated.Offering.Grain;

            want[Const.BRICK] = ev.PlayerTradeInitiated.Wanting.Brick;
            want[Const.LUMBER] = ev.PlayerTradeInitiated.Wanting.Lumber;
            want[Const.WOOL] = ev.PlayerTradeInitiated.Wanting.Wool;
            want[Const.ORE] = ev.PlayerTradeInitiated.Wanting.Ore;
            want[Const.GRAIN] = ev.PlayerTradeInitiated.Wanting.Grain;

            instigatorID = nm.parsePlayerID(ev.Instigator.Id);
            targetID = nm.parsePlayerID(ev.PlayerTradeInitiated.Other.Id);
        }

        public IPlayer getInstigator() {
            return instigator;
        }

        public IPlayer getTarget() {
            return target;
        }

        public int[] getOffer() {
            return offer;
        }

        public int[] getWant() {
            return want;
        }
    }
}
