﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class RequestSteal : Action {
        public int targetPlayer;

		//Run the steal on gamestate
        public override void run(CatanAI ai, IGameState g) {
            if (g.getState() == Const.RESOURCETOSTEAL) {
                g.setState(Const.AWAITINGSTEAL);
            } else {
                g.setState(Const.AWAITINGSTEALPRETURN);
            }
        }

        public override void print(CatanAI ai) {
            Console.WriteLine("RequestSteal");
        }

		//Send the request to steal to the server
        public override void send(CatanAI ai, NetworkManager nm) {
            Intergroup.Request req = new Intergroup.Request {
                SubmitTargetPlayer = new Intergroup.Board.Player {
                    Id = nm.getProtobufPlayerID(targetPlayer)
                }
            };

            nm.sendRequest(req);
        }

        public RequestSteal(int PlayerToStealFrom) {
            this.targetPlayer = PlayerToStealFrom;
        }
    }
}
