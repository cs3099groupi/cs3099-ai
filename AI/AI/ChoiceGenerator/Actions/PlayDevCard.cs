﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class PlayDevCard : Action {
        public int devCardType;
        public override void run(CatanAI ai, IGameState g) {
            a.playDevCard(g, devCardType, g.getCurrentPlayer());
        }
        public override void print(CatanAI ai) {
            Console.WriteLine("PlayDevCard: " + devCardToStr());
        }

		//convert internal representation of a dev to string
        public String devCardToStr() {
            switch (devCardType) {
                case Const.KNIGHT:
                    return "knight";
                case Const.MONOPOLY:
                    return "monopoly";
                case Const.YEAR_OF_PLENTY:
                    return "year of plenty";
                case Const.ROAD_BUILDING:
                    return "road building";
                case Const.VICTORY_POINT:
                    return "victory point";
                case Const.UNKNOWN_DEVCARD:
                    return "unknown dev card";
            }
            return "invalid devCardType";
        }

		//send the play dev card request to the server
        public override void send(CatanAI ai, NetworkManager nm) {
            Intergroup.Request req = new Intergroup.Request {
                PlayDevCard = parseintoIntergroup(devCardType)
            };

            nm.sendRequest(req);
        }

        public PlayDevCard(int type) {
            this.devCardType = type;
        }

		//parse internal representation of the dev card to intergroup protocol
        public Intergroup.Board.PlayableDevCard parseintoIntergroup(int card) {
            switch (card) {
                case Const.KNIGHT:
                    return Intergroup.Board.PlayableDevCard.Knight;
                case Const.YEAR_OF_PLENTY:
                    return Intergroup.Board.PlayableDevCard.YearOfPlenty;
                case Const.MONOPOLY:
                    return Intergroup.Board.PlayableDevCard.Monopoly;
                case Const.ROAD_BUILDING:
                    return Intergroup.Board.PlayableDevCard.RoadBuilding;
                default:
                    throw new Exception("Card " + card + " not found");
            }
        }
    }
}
