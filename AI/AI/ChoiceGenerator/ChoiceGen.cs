﻿using System;
using System.Collections.Generic;

namespace AI {
    class ChoiceGen {
        IRules rules = new Rules();
        //STATES for the game loop to be in, from the AI's point of view
        //this in combination with who's the current player will allow the ai to know what it's current options are. sometimes none. (e.g. other player has to place initial settlement)

        //given a game state and a player (itself) it generates a list of choices by utilizing rules.cs to check the options available,
        //creating actions for each option,
        //and runs that action to create a new game state. this is done till a base case (where the game state is no longer under its control (no actions left)
        //a tree of actions and the game states they create are produced. with each path through the tree represented as a choice
        //a choice being a final end state and the list of actions to create the state

        //gets all choices given a gamestate
        //can represent a list of actions to do or just 1 action
        //current version just returns a list of single action choices
        public List<Choice> generateChoices(CatanAI ai, IGameState g, IPlayer p, NetworkManager nm) {
            List<Choice> choices = new List<Choice>();
            Choice blankChoice = new Choice(g);
            generateAllChoices(ai, choices, blankChoice, blankChoice.state(), p.getID(), nm);
            return choices;
        }

        //this is a version of the above that can be called recursively
        //however the recursion is disabled in this version.
        void generateAllChoices(CatanAI ai, List<Choice> allChoices, Choice currentChoice, IGameState g, int p, NetworkManager nm) {
            List<IAction> actions = getAvailableActions(g, p, nm);
            if (actions.Count == 0) {//leaf of the tree
                allChoices.Add(currentChoice);
            } else {
                foreach (IAction a in actions) {
                    Choice c = currentChoice.copyAdd(ai, a);
                    allChoices.Add(c);
                    //can be recursive
                    //generateAllChoices(allChoices, c, c.state(), p); 
                }
            }
        }
        //gets a list of actions given a game state.
        List<IAction> getAvailableActions(IGameState g, int p, NetworkManager nm) {
            List<IAction> actions = new List<IAction>();
            if (g.getCurrentPlayer() != g.getPlayer(p)) {//if it isn't the player's turn then the only actions it needs to worry about are trades and discards
                if (g.getState() == Const.TRADE_OFFERED_TO_AI) {
                    //someone has issued a trade request, so add a reject and accept action if the trade is valid
                    Console.WriteLine("AI must decide on the trade!");
                    actions.Add(new RejectPlayerTrade());
                    lock (nm.currentPlayerTradeOfferToAi) {
                        if (rules.canAcceptTrade(nm.currentPlayerTradeOfferToAi, g.getPlayer(p))) {
                            actions.Add(new ConfirmPlayerTrade(nm.currentPlayerTradeOfferToAi, nm));
                        }
                    }
                } else {
                    if (g.getState() == Const.AWAITDISCARD) {//add any discard actions possible
                        if (g.getPlayer(p).totalResources() > 7 && (!g.getPlayer(p).hasDiscarded() && !((Player)g.getPlayer(p)).DiscardRequested)) {
                            foreach (int[] resourcesToDiscard in rules.getValidDiscards(g.getPlayer(p))) {
                                actions.Add(new Discard(resourcesToDiscard, p));
                            }
                        }
                    }
                }
                return actions;
            }

            //based on the state of the game, checks for different options available to the player.
            switch (g.getState()) {
                //waiting for the server
                case Const.AWAITDISCARD:
                    if (g.getPlayer(p).totalResources() > 7 && (!g.getPlayer(p).hasDiscarded() || !((Player)g.getPlayer(p)).DiscardRequested))
                        foreach (int[] resourcesToDiscard in rules.getValidDiscards(g.getPlayer(p))) {
                            actions.Add(new Discard(resourcesToDiscard, p));
                        }
                    break;
                    //these states (AWAITING) are states where the server is performing computation of some sort. So no actions are possible 
                case Const.AWAITINGDEVCARDBUYING:
                case Const.AWAITINGSTEAL:
                case Const.AWAITINGSTEALPRETURN:
                case Const.AWAITINGDICEROLL:
                case Const.AWAITINGENDTURN:
                case Const.AWAITINGMONOPOLY:
                case Const.AWAITINGMONOPOLYPRETURN:
                case Const.AWAITINGTRADERESPONSE:
                    break;
                case Const.INITIALSETTLEMENT:
                    foreach (ICoord c in rules.getValidSettlementBuildSpotsStart(g, g.getPlayer(p))) {
                        actions.Add(new BuildInitialSettlement(c));
                    }
                    break;
                case Const.INITIALROAD:
                    foreach (Tuple<ICoord, ICoord> c in rules.getValidRoadBuildSpotsStart(g, g.getPlayer(p))) {
                        actions.Add(new BuildInitialRoad(c.Item1, c.Item2));
                    }
                    break;
                case Const.TURNSTART:
                    if (rules.canPlayKnight(g, g.getPlayer(p))) actions.Add(new PlayDevCard(Const.KNIGHT));
                    if (rules.canPlayMonopoly(g, g.getPlayer(p))) actions.Add(new PlayDevCard(Const.MONOPOLY));
                    if (rules.canPlayRoadBuilding(g, g.getPlayer(p))) actions.Add(new PlayDevCard(Const.ROAD_BUILDING));
                    if (rules.canPlayYearOfPlenty(g, g.getPlayer(p))) actions.Add(new PlayDevCard(Const.YEAR_OF_PLENTY));
                    actions.Add(new RequestDiceRoll());//action to denote rolling the dice
                    break;
                case Const.WAITPLAY:
                    //WAITPLAY is the normal state for when the player is making its move.
                    //various things can be done in this state
                    if (rules.canBuildCity(g, g.getPlayer(p))) {
                        foreach (ISettlement st in rules.getValidCityBuildSpots(g, g.getPlayer(p))) {
                            actions.Add(new BuyCity(st.getPos()));//create an action that defines building at that spot
                        }
                    }
                    if (rules.canBuildSettlement(g, g.getPlayer(p))) {
                        foreach (ICoord c in rules.getValidSettlementBuildSpots(g, g.getPlayer(p))) {
                            actions.Add(new BuySettlement(c));//create an action that defines building at that spot
                        }
                    }
                    if (rules.canBuildRoad(g, g.getPlayer(p))) {
                        foreach (Tuple<ICoord, ICoord> tpl in rules.getValidRoadBuildSpots(g, g.getPlayer(p))) {
                            actions.Add(new BuyRoad(tpl.Item1, tpl.Item2));//create an action that defines building at that spot
                        }
                    }
                    if (rules.canBuyDevCard(g, g.getPlayer(p))) {
                        actions.Add(new BuyDevCard());
                    }
                    foreach (Tuple<int[], int[]> tpl in rules.getValidBankTrades(g, g.getPlayer(p))) {
                        actions.Add(new BankTrade(tpl.Item1, tpl.Item2));
                    }
                    //dev cards possible to play
                    if (rules.canPlayKnight(g, g.getPlayer(p))) actions.Add(new PlayDevCard(Const.KNIGHT));
                    if (rules.canPlayMonopoly(g, g.getPlayer(p))) actions.Add(new PlayDevCard(Const.MONOPOLY));
                    if (rules.canPlayRoadBuilding(g, g.getPlayer(p))) actions.Add(new PlayDevCard(Const.ROAD_BUILDING));
                    if (rules.canPlayYearOfPlenty(g, g.getPlayer(p)) && g.getBank().getResourceTotal() >= 2) actions.Add(new PlayDevCard(Const.YEAR_OF_PLENTY));
                    actions.Add(new EndTurn());
                    break;
                    //dev card states
                case Const.FIRSTROADBUILDINGBEFOREROLLINGTHEDICE:
                case Const.FIRSTROADBUILDING:
                case Const.SECONDROADBUILDINGBEFOREROLLINGTHEDICE:
                case Const.SECONDROADBUILDING:
                    foreach (Tuple<ICoord, ICoord> tpl in rules.getValidRoadBuildSpots(g, g.getPlayer(p))) {
                        actions.Add(new BuildRoadBuildingRoad(tpl.Item1, tpl.Item2));//create an action that defines building at that spot
                    }
                    break;
                case Const.FIRSTYEAROFPLENTY:
                case Const.FIRSTYEAROFPLENTYBEFOREROLLINGTHEDICE:
                    addResourceChoicesYOP(actions, g);
                    break;

                case Const.SECONDYEAROFPLENTY:
                case Const.SECONDYEAROFPLENTYBEFOREROLLINGTHEDICE:
                    addResourceChoicesYOP(actions, g);
                    break;
                case Const.CHOOSINGMONOPOLY:
                case Const.CHOOSINGMONOPOLYBEFOREROLLINGTHEDICE:
                    addResourceChoicesMonopoly(actions);
                    break;
                    //robber / stealing states
                case Const.CHOOSINGROBBERPLACEMENT:
                case Const.AWAITINGROBBERPLACEMENTDUETOKNIGHTBEINGPLAYEDBEFOREROLLINGDICE:
                    Console.WriteLine("must choose a robber placement spot.");
                    foreach (ICoord c in rules.getValidRobberMoveSpots(g)) {
                        actions.Add(new MoveRobber(c));
                    }
                    break;
                case Const.RESOURCETOSTEAL:
                case Const.RESOURCETOSTEALPRETURN:
                    foreach (IPlayer player in rules.getValidPlayersToStealFrom(g)) {
                        actions.Add(new RequestSteal(player.getID()));
                    }
                    break;
            }
            return actions;
        }
        //adds the possible choices of resources to monopolise
        public void addResourceChoicesMonopoly(List<IAction> actns) {
            foreach (int res in rules.getResourceChoices()) {
                actns.Add(new ChooseResource(res));
            }
        }
        //adds the possible choices of resources to recieve from the bank
        public void addResourceChoicesYOP(List<IAction> actns, IGameState g) {
            foreach (int res in rules.getResourceChoices()) {
                if (g.getBank().getResourceCount(res) > 0) {
                    actns.Add(new ChooseResource(res));
                }
            }
        }
    }
}
