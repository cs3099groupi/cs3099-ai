﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI.ChoiceGenerator.Events {
    class rejectRequest : EventAction {//special case of iaction for when a server rejects a request made by the ai. throws a spanner in the works for everyone really.
        public override void print(CatanAI ai) {
            Console.WriteLine("server rejects request, unexpectedly");
        }

        public override void run(CatanAI ai, IGameState g) {
            //do nothing
        }
    }
}
