﻿using System;
using System.Collections.Generic;
using Intergroup.Board;
using System.Linq;
using System.Text;


namespace AI.ChoiceGenerator.Events {
    class MonopolyResolution : EventAction {

        int monopPID;
        int resource;
        Dictionary<IPlayer, int> steals = new Dictionary<IPlayer, int>();

        public override void print(CatanAI ai) {
            Console.WriteLine("dev card bought.");
        }

		//run the monopoly resolution on the game state
        public override void run(CatanAI ai, IGameState g) {
            IPlayer monopP = g.getPlayer(monopPID);
            a.monopolyResolution(g, resource, steals, monopP);
        }

		//create the monopoly resolution 
        public MonopolyResolution(CatanAI ai, Intergroup.Event ev, NetworkManager nm) {
            MultiSteal monopoly = ev.MonopolyResolution;
            monopPID = nm.parsePlayerID(ev.Instigator.Id);
            if (monopoly.Thefts.Count > 0) {
                resource = nm.parseResource(monopoly.Thefts[0].Resource);
                foreach (Steal steal in monopoly.Thefts) {
                    int pid = nm.parsePlayerID(steal.Victim.Id);
                    IPlayer p = ai.theGameState.getPlayer(pid);
                    if (p != null) {
                        steals.Add(p, steal.Quantity);
                    }
                }
            }
        }
    }
}
