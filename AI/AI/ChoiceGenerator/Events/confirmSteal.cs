﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class confirmSteal : EventAction {
        private static Random random = new Random();
        private bool resourceKnown;
        private int resource;
        private int victimID;

        public confirmSteal(int victim) {
            resourceKnown = false;
            victimID = victim;
        }

        public confirmSteal(int i, int victim) {
            resourceKnown = true;
            resource = i;
            victimID = victim;
        }

		//Run the confirm steal on the game state
        public override void run(CatanAI ai, IGameState g) {
            //either set the state to await play if the was a knight played during await play or turnstart if the dice has not yet been rolled
            if (g.getState() == Const.AWAITINGSTEAL || g.getState() == Const.RESOURCETOSTEAL) {
                g.setState(Const.WAITPLAY);
            } else {
                if (g.getState() == Const.AWAITINGSTEALPRETURN || g.getState() == Const.RESOURCETOSTEALPRETURN) {
                    g.setState(Const.TURNSTART);
                } else {
                    //CASE SHOULD NOT HAPPEN: this is bad it should always be one of the two!
                    Console.WriteLine(g.getState());
                    g.setState(Const.WAITPLAY);
                }
            }
            IPlayer stealingP = g.getCurrentPlayer();
            IPlayer victim = g.getPlayer(victimID);
            if (resourceKnown) {
                stealingP.GiveResource(resource, 1);
                victim.RemoveResource(resource, 1);
            } else {
                stealingP.addToResourceCount();
                victim.removeFromResourceCount();
            }
        }

        public override void print(CatanAI ai) {
            Console.WriteLine("Steal confirmed: stealing from player " + victimID + " resource: " + (resourceKnown ? (" " + resource) : "unknown"));
        }
    }
}
