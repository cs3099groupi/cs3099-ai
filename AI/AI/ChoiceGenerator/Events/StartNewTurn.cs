﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI.ChoiceGenerator.Events {
    class StartNewTurn : EventAction {
        public override void print(CatanAI ai) {
            Console.WriteLine("Starting new turn");
        }

        public override void run(CatanAI ai, IGameState g) {
            IActions a = new Actions();
            IPlayer p = g.getCurrentPlayer();
            for (int i = 0; i < p.getDevCardBoughtThisTurn().Length; i++) {
                for (int j = 0; j < p.getDevCardBoughtThisTurn()[i]; j++) {
                    a.removeDevCardBoughtThisTurn(g, i, p);
                    a.giveDevCard(g, i, p);
                }
            }
            a.endTurn(g);
        }
    }
}
