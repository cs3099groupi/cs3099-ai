﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI.ChoiceGenerator.Action_Classes {
    class AllocateResources : EventAction {

        public override void print(CatanAI ai) {
            Console.WriteLine("AllocatResources on roll: " + diceRoll);
        }
        private int diceRoll;
        private const int SETTLEMENT_RESOURCE = 1, CITY_RESOURCE = 2;

        public override void run(CatanAI ai, IGameState g) {
            allocateResources(g, diceRoll);
            g.setState(Const.WAITPLAY);
        }

        public AllocateResources(int roll) {
            this.diceRoll = roll;
        }

		//Supplementary class
        private class ResourceProduction {

            private int totalCount;
            private List<int> players = new List<int>();

			//get total count of resources of that production
            public int getTotalCount() {
                return totalCount;
            }

			//get the number of players involved in the production
            public List<int> getPlayers() {
                return players;
            }

			//increase the production by count
            public void increaseTotal(int count) {
                this.totalCount += count;
            }

			//does the production invole player
            public bool hasPlayer(int player) {
                return players.Contains(player);
            }

			//add the player to the production
            public void addPlayer(int player) {
                this.players.Add(player);
            }

        }

        public static void allocateResources(IGameState st, int diceRoll) {
            st.getBank().print();
            Console.WriteLine("-----ResourceAllocation-----: dice roll = " + diceRoll);

            //Represents the datastructure which holds information about each RESOURCE'S production 
            //e.g. Each resource, will have a count (number of that resource produced) and the players that want that resource
            Dictionary<int, ResourceProduction> productionTotals = new Dictionary<int, ResourceProduction>();


            //Represents the datastructure which holds information about each PLAYER's production 
            //e.g. Each player, will have a resource and its amount for the given turn
            Dictionary<int, Dictionary<int, int>> playerResources = new Dictionary<int, Dictionary<int, int>>();

            //Fetch the tiles which contain the given diceRoll
            ITile[] tiles = st.getBoard().getTiles(diceRoll);

            Boolean hasProduction = false;
            foreach (var tile in tiles) {
                if (!st.getRobberPos().isEqual(tile.getPos())) {
                    if (!hexProduction(st, playerResources, productionTotals, tile)) {
                        Console.WriteLine("No Production on this tile");
                        continue; //no production on that hex: no structures
                    } else {
                        hasProduction = true;
                    }
                } else {
                    Console.WriteLine("ROBBER IN THE WAY");
                }

            }

			//checks if there is a production, if so try and allocate
            if (hasProduction) {
                allocateProduction(st, playerResources, productionTotals);
            }
        }

        private static bool hexProduction(IGameState st, Dictionary<int, Dictionary<int, int>> playerResources, Dictionary<int, ResourceProduction> productionTotals, ITile tile) {

            int resourceType = tile.getResourceType();

            //Structures surrounding the hex
            List<ISettlement> structures = st.getNeighbourSettlements(tile);

            //Are there structures surrounding the hex?
            if (structures.Count == 0) {
                Console.WriteLine("0 structures surrounding tile");
                return false;
            }
            //If this resource hasn't been seen so far, add it
            if (!productionTotals.ContainsKey(resourceType)) {
                ResourceProduction a = new ResourceProduction();
                productionTotals.Add(resourceType, a);
            }

            //structures around the hex
            foreach (var structure in structures) {
                int quantity = 1; //production quantity (Settlement: 1, City: 2)
                if (structure.isCity()) {
                    quantity = 2;
                }
                updateProductionTotals(productionTotals, resourceType, quantity, structure.getOwner().getID());
                updatePlayerResources(playerResources, resourceType, quantity, structure.getOwner().getID());
            }

            return true;
        }

        private static bool hasPlayer(int player, Dictionary<int, Dictionary<int, int>> playerResources) {
            return (playerResources.ContainsKey(player));
        }

        private static void updatePlayerResources(Dictionary<int, Dictionary<int, int>> playerResources, int resourceType, int quantity, int player) {
            //if this player hasn't already been producing resources
            if (!hasPlayer(player, playerResources)) { 
                Dictionary<int, int> resourceValue = new Dictionary<int, int>();
                resourceValue.Add(resourceType, quantity);
                playerResources.Add(player, resourceValue);
            }

            //if this resource hasn't been produced for this player
            else if (!playerResources[player].ContainsKey(resourceType)) {
                playerResources[player].Add(resourceType, quantity);
            } else {
                Dictionary<int, int> playerResource = playerResources[player];
                playerResource[resourceType] += quantity;
            }
        }

        private static void updateProductionTotals(Dictionary<int, ResourceProduction> productionTotals, int resourceType, int quantity, int player) {
            updateProductionTotalsCount(productionTotals, resourceType, quantity);
            updateProductionTotalsPlayers(productionTotals, resourceType, player);
        }

        private static void updateProductionTotalsCount(Dictionary<int, ResourceProduction> productionTotals, int resourceType, int quantity) {
            productionTotals[resourceType].increaseTotal(quantity);
        }

        private static void updateProductionTotalsPlayers(Dictionary<int, ResourceProduction> productionTotals, int resourceType, int player) {

            bool productionHasPlayer = productionTotals[resourceType].hasPlayer(player);
            if (!productionHasPlayer) { 
                productionTotals[resourceType].addPlayer(player);
            }
        }

		//Performs allocation if possible
        private static bool allocateProduction(IGameState st, Dictionary<int, Dictionary<int, int>> playerResources, Dictionary<int, ResourceProduction> productionTotals) {

            bool doneAllocation = false;

            //For each resource, check with the bank if it can allocate the given resource for the players
            foreach (KeyValuePair<int, ResourceProduction> production in productionTotals) {

                int resourceType = production.Key;

                int count = production.Value.getTotalCount();
                //Bank side allocation

                IBank bank = st.getBank();
                bool bankCanAllocate = bank.hasResource(resourceType, count);

                //If the bank can allocate: allocate to players
                if (bankCanAllocate) {
                    bank.removeResource(resourceType, count);
                    allocateProductionToPlayers(st, playerResources, production.Value.getPlayers(), resourceType);
                    doneAllocation = true;
                    continue;
                }
                //Single player's production might be able to be allocated
                else {
                    Console.WriteLine("All player's production cannot be satisfied by the bank");
                    bool hasAllocated = singlePlayerLeftOverAllocation(st, production.Value.getPlayers().Count, resourceType, production.Value.getPlayers()[0]);
                    if (hasAllocated) {
                        doneAllocation = true;
                    }
                    continue;
                }

            }

            return doneAllocation;
        }

		//Allocation for the single player if the bank can't allocate the total production
        private static bool singlePlayerLeftOverAllocation(IGameState st, int playerCount, int resourceType, int player) {
            IBank bank = st.getBank();
            if (playerCount == 1) {
                int bankAmount = bank.getResourceCount(resourceType);
                if (bankAmount > 0) {
                    bank.removeResource(resourceType, bankAmount);

                    st.getPlayer(player).GiveResource(resourceType, bankAmount);
                    return true;
                }
            }

            return false;
        }

		//Allocate production to all the players 
        private static void allocateProductionToPlayers(IGameState st, Dictionary<int, Dictionary<int, int>> playerResources, List<int> players, int resourceType) {
            foreach (var player in players) {

                int quantity = playerResources[player][resourceType];
                st.getPlayer(player).GiveResource(resourceType, quantity);
            }
        }
    }
}
