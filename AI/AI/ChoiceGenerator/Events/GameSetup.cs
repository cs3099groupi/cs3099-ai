﻿using System;
using System.Linq;
namespace AI {
    class GameSetup : EventAction {
        private String userName;
        private Intergroup.Lobby.GameSetup thesetup;
        IActions actions;

        public override void print(CatanAI ai) {
            Console.WriteLine("game set up");
            Console.WriteLine("AI PLayer is: " + ai.aiPlayerID);
        }

		//Run game setup on the game state
        public override void run(CatanAI ai, IGameState g) {
            ai.theGameState = actions.spawnEmptyGameState();
            actions.spawnFreeSettlements(ai.theGameState);
            actions.spawnFreeRoads(ai.theGameState);
            addPlayers(ai, thesetup);

            addHexes(ai, thesetup);
            addPorts(ai, thesetup);
            ai.theGameState.getBoard().generateRollValDic();
            ai.theGameState.setState(Const.INITIALSETTLEMENT);
            ai.theGameState.setCurrentPlayer(ai.theGameState.getPlayer(0));
        }

        public GameSetup(Intergroup.Lobby.GameSetup setup, String username) {
            this.userName = username;
            thesetup = setup;
            actions = new Actions();
        }

		//Add players to the lobby
        private void addPlayers(CatanAI ai, Intergroup.Lobby.GameSetup setup) {
            IPlayer clientPlayer = new Player(0, 0, 0, 0);
            if (setup.PlayerSettings == null) {
                //no players returned
            }
            for (int i = 0; i < (setup.PlayerSettings.Count); i++) {
                if (setup.PlayerSettings[i].Player.Id.Equals(setup.OwnPlayer.Id)) {
                    ai.theGameState.addPlayer(clientPlayer);
                    clientPlayer.setID(int.Parse(setup.PlayerSettings[i].Player.Id.ToString().Substring(setup.PlayerSettings[i].Player.Id.ToString().Length - 1, 1)) - 1);
                    ai.aiPlayerID = clientPlayer.getID();
                } else {
                    IPlayer p = new Player(0, 0, 0, int.Parse(setup.PlayerSettings[i].Player.Id.ToString().Substring(setup.PlayerSettings[i].Player.Id.ToString().Length - 1, 1)) - 1);
                    ai.theGameState.addPlayer(p);
                }
            }
            ai.theGameState.setCurrentPlayer(ai.theGameState.getPlayers().First());//necessary?!?!
        }


        //assigns board tiles a resource type
        private void addHexes(CatanAI ai, Intergroup.Lobby.GameSetup setup) {
            for (int i = 0; i < setup.Hexes.Count; i++) {
                Intergroup.Board.Point point = setup.Hexes[i].Location;
                Intergroup.Terrain.Kind terrain = setup.Hexes[i].Terrain;
                int rollValue = setup.Hexes[i].NumberToken;
                ITile cur = ai.theGameState.getBoard().getTile(new Coord(point.X, point.Y));
                Console.WriteLine("ROll val: " + rollValue + " terrain: " + parseTerrain(terrain));
                cur.SetResourceType(parseTerrain(terrain));
                if (parseTerrain(terrain) == Const.DESERT) ai.theGameState.setRobberPos(new Coord(point.X, point.Y));
                //adding roll value
                if (rollValue > 0) {
                    cur.SetRollValue(rollValue);
                }

            }
        }

        //converts Intergroup.buf terrains into game resource types
        private int parseTerrain(Intergroup.Terrain.Kind terrain) {
            switch (terrain) {
                case Intergroup.Terrain.Kind.Mountains:
                    return Const.ORE;
                case Intergroup.Terrain.Kind.Fields:
                    return Const.GRAIN;
                case Intergroup.Terrain.Kind.Hills:
                    return Const.BRICK;
                case Intergroup.Terrain.Kind.Forest:
                    return Const.LUMBER;
                case Intergroup.Terrain.Kind.Pasture:
                    return Const.WOOL;
                case Intergroup.Terrain.Kind.Desert:
                    return Const.DESERT;
                default:
                    throw new TerrainNotFoundException(terrain);
            }
        }

		//adds ports on the ai's game state
        private void addPorts(CatanAI ai, Intergroup.Lobby.GameSetup setup) {
            for (int i = 0; i < setup.Harbours.Count; i++) {
                Intergroup.Board.Edge IntergroupEdge = setup.Harbours[i].Location;
                int resourceInt = parseResource(setup.Harbours[i].Resource);
                ai.theGameState.getBoard().addPort(new Port(resourceInt, new Coord(IntergroupEdge.A.X, IntergroupEdge.A.Y)));
                ai.theGameState.getBoard().addPort(new Port(resourceInt, new Coord(IntergroupEdge.B.X, IntergroupEdge.B.Y)));

            }
        }

		//converts Intergroup.buf resources into internal game resource types
        private int parseResource(Intergroup.Resource.Kind resource) {
            switch (resource) {
                case Intergroup.Resource.Kind.Ore:
                    return Const.ORE;
                case Intergroup.Resource.Kind.Grain:
                    return Const.GRAIN;
                case Intergroup.Resource.Kind.Brick:
                    return Const.BRICK;
                case Intergroup.Resource.Kind.Lumber:
                    return Const.LUMBER;
                case Intergroup.Resource.Kind.Wool:
                    return Const.WOOL;
                case Intergroup.Resource.Kind.Generic:
                    return Const.UNIVERSAL;
                default:
                    throw new ResourceNotFoundException("");
            }
        }
    }
}
