﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class WinGame : EventAction {
        public override void print(CatanAI ai) {
            Console.WriteLine("Game won");
        }

        public override void run(CatanAI ai, IGameState g) {
            throw new GameOverException();
        }
    }
}
