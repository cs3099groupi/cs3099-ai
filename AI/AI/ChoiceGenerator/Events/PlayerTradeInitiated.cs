﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class PlayerTradeInitiated : EventAction {

        private int[] offer = new int[Const.NUM_OF_RESOURCES];
        private int[] want = new int[Const.NUM_OF_RESOURCES];
        private IPlayer instigator;
        private int instigatorID;
        private int targetID;
        private IPlayer target;

        public override void print(CatanAI ai) {
            Console.WriteLine("A trade has been offered");
        }

		//run the player trade initated on the game state
        public override void run(CatanAI ai, IGameState g) {
            instigator = g.getPlayer(instigatorID);
            target = g.getPlayer(targetID);
            if (target == g.getPlayer(ai.aiPlayerID)) {
                g.setState(Const.TRADE_OFFERED_TO_AI);
            } else {
                if (instigator == g.getPlayer(ai.aiPlayerID)) {
                    g.setState(Const.AWAITINGTRADERESPONSE);
                } else {
                    g.setState(Const.TRADE_WITHOUT_AI);
                }
            }
        }

		//extract the resources involved in the trade initiation - offer and want
        public PlayerTradeInitiated(Intergroup.Event ev, NetworkManager nm) {
            offer[Const.BRICK] = ev.PlayerTradeInitiated.Offering.Brick;
            offer[Const.LUMBER] = ev.PlayerTradeInitiated.Offering.Lumber;
            offer[Const.WOOL] = ev.PlayerTradeInitiated.Offering.Wool;
            offer[Const.ORE] = ev.PlayerTradeInitiated.Offering.Ore;
            offer[Const.GRAIN] = ev.PlayerTradeInitiated.Offering.Grain;

            want[Const.BRICK] = ev.PlayerTradeInitiated.Wanting.Brick;
            want[Const.LUMBER] = ev.PlayerTradeInitiated.Wanting.Lumber;
            want[Const.WOOL] = ev.PlayerTradeInitiated.Wanting.Wool;
            want[Const.ORE] = ev.PlayerTradeInitiated.Wanting.Ore;
            want[Const.GRAIN] = ev.PlayerTradeInitiated.Wanting.Grain;
            instigatorID = nm.parsePlayerID(ev.Instigator.Id);
            targetID = nm.parsePlayerID(ev.PlayerTradeInitiated.Other.Id);
        }
    }
}
