﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class AllocateDevCard : EventAction {
        int thecard;//could be -1 (not known)
        public override void print(CatanAI ai) {
            Console.WriteLine("dev card bought.");
        }

		//run allocation of the dev card on the game state 
        public override void run(CatanAI ai, IGameState g) {
            IActions a = new Actions();
            if (thecard == Const.VICTORY_POINT) {
                g.getCurrentPlayer().addPoints(1);
            } else a.giveDevCardBoughtThisTurn(g, thecard, g.getCurrentPlayer());
            IPlayer p = g.getCurrentPlayer();
            int[] cost = r.getCostDevCard();
            p.removeResources(cost);
            g.getBank().giveResources(cost);
            g.getBank().removeDevCard();
        }

		//fetch the dev card from intergroup event
        public AllocateDevCard(Intergroup.Event ev) {
            switch (ev.DevCardBought.CardCase) {
                case Intergroup.Board.DevCard.CardOneofCase.Unknown:
                    thecard = Const.UNKNOWN_DEVCARD;
                    break;
                case Intergroup.Board.DevCard.CardOneofCase.VictoryPoint:
                    thecard = Const.VICTORY_POINT;
                    break;
                case Intergroup.Board.DevCard.CardOneofCase.PlayableDevCard:
                    thecard = parseDevCard(ev.DevCardBought.PlayableDevCard);
                    break;

            }
        }

		//parse the playable intergroup object into internal representation
        int parseDevCard(Intergroup.Board.PlayableDevCard cd) {
            switch (cd) {
                case Intergroup.Board.PlayableDevCard.Knight:
                    return Const.KNIGHT;
                case Intergroup.Board.PlayableDevCard.Monopoly:
                    return Const.MONOPOLY;
                case Intergroup.Board.PlayableDevCard.RoadBuilding:
                    return Const.ROAD_BUILDING;
                case Intergroup.Board.PlayableDevCard.YearOfPlenty:
                    return Const.YEAR_OF_PLENTY;
            }
            return -1;
        }
    }
}
