﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class SevenRolled : EventAction {
        public override void print(CatanAI ai) {
            Console.WriteLine("7 rolled. discards may be necessary before moving the robber.");
        }

		//run the seven rolled on the game state
		//making a state change: discard necessary or robber placement
        public override void run(CatanAI ai, IGameState g) {
            bool discardsnecessary = false;
            foreach (IPlayer p in g.getPlayers()) {

                Console.WriteLine("Player " + p.getID() + " resources owned: " + p.totalResources());

                if (p.totalResources() > 7) {
                    discardsnecessary = true;
                    p.setHasDiscarded(false);
                } else {
                    p.setHasDiscarded(true);
                }
            }
            if (discardsnecessary) {
                g.setState(Const.AWAITDISCARD);
            } else {
                g.setState(Const.CHOOSINGROBBERPLACEMENT);
            }
        }
    }
}
