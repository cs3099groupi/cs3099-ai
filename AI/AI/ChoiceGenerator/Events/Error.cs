﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI.ChoiceGenerator.Events {
    class Error : IAction {
        Intergroup.Event message;
        public void print(CatanAI ai) {
            Console.WriteLine("Error, invalid action: " + message);
        }

        public void run(CatanAI ai, IGameState g) {
            throw new InvalidOperationException();
        }

        public void send(CatanAI ai, NetworkManager nm) {
            throw new InvalidOperationException();
        }

        public Error(Intergroup.Event msg) {
            message = msg;
        }
    }
}
