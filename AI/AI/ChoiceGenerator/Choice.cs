﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class Choice {
        /* represents a choice the AI can make when given a game state. 
         * is comprised of a list of things to do, and the end state that is created. 
         * the AI can look at the state and decide if this is a good decision.
         * then based on the information stored in the actions, it can recreate these actions.
         * 
         */
        IGameState endState;//the state of the game after the actions have been made
        List<IAction> actions;//the list of actions to take to create the end state
        public Choice(IGameState g) {
            endState = g;
            actions = new List<IAction>();
        }
        public Choice copyAdd(CatanAI ai, IAction a) {
            //returns a copy of itself but altered, to take into account the given action
            Choice newC = new Choice(endState.copy());
            foreach (IAction act in actions) {
                newC.actions.Add(act);
            }
            newC.Add(ai, a);
            return newC;
        }
        public Action copy() {
            //returns a copy of itself
            return null;
        }
        public void Add(CatanAI ai, IAction a) {
            a.run(ai, endState);
            actions.Add(a);
        }
        public IGameState state() {
            return endState;
        }
        public void print(CatanAI ai) {
            Console.WriteLine("Choice consists of: ");
            foreach (IAction a in actions) {
                a.print(ai);
            }
            Console.WriteLine("end state: " + ((GameState)endState).stateToString() + "\n");
        }
        public List<IAction> getActions() {
            return actions;
        }
    }
}
