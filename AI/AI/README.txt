﻿Settlers of Catan AI Implementation created by:

Junior Honours group I
Alastair Millican
Bhargava Jariwala
David Heiberg
Gavin Williams
Thomas Strang

Supervisor: Dr. David Harris-Birtill

----OVERVIEW----

This is a standalone project that can be run from the command line in a Windows environment or UNIX with Mono.
There is also an integrated version of this project in the Unity Project, also created by Junior Honours Group I.
Once run it attempts to connect to a running Settlers of Catan Server using the junior honours intergroup protocol

----RUN INSTRUCTIONS----

The executable file is stored in Output/ and is called AI.exe
It is run with a number of arguments, that must be in the correct order.
> AI.exe IPADDRESS PORT NAME [options]
IPADDRESS is the address of the server to connect to
PORT is the port to connect to
NAME is the name you want the AI to provide the server
[options] can be none, one or two of the following:
	'random' this makes the ai perform random moves for the duration of the game
	'disableSleep' this disables waiting 5 seconds before the AI makes it's move.

not including any arguments causes the program to use a default setup, which undoubtedly fail.

Examples:

in windows:
> AI.exe
> AI.exe 138.251.207.194 7000 exampleUsername disableSleep
> AI.exe 138.251.206.24 7784 anotherUsername random disableSleep
> AI.exe 138.251.207.194 7000 thirdUsername

in Unix (mono prerequisite):
> mono AI.exe
> mono AI.exe 138.251.207.194 7000 exampleUsername disableSleep
> mono AI.exe 138.251.206.24 7784 anotherUsername random disableSleep
> mono AI.exe 138.251.207.194 7000 thirdUsername