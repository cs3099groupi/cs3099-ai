﻿using AI.ChoiceGenerator.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


namespace AI {
    class CatanAI {
        bool stateChecked;
        public int aiPlayerID;
        public IGameState theGameState;

        public Decider ai;
        public Thread aiThread;
        public Thread loopThread;

        public List<Intergroup.Event> incomingEvents = new List<Intergroup.Event>();//buffer of incoming events from server to reset
        public List<IAction> outgoingActions = new List<IAction>();//buffer of incoming events from server to reset

        public bool sleepActivated;

        public NetworkManager nm;

        public bool eventParsed = false;

        public void handleIncomingEvent(Intergroup.Event ev) {//called by the networking thread to add events to the incoming events list
            lock (incomingEvents) {
                incomingEvents.Add(ev);
            }
        }

        public void handleOutgoingEvent(IAction act) {//called by the ai thread to add events to the outgoing events list
            lock (outgoingActions) {
                outgoingActions.Add(act);
            }
        }

        public void main(object obj) {
            string[] args = (string[])obj;
            Console.WriteLine("Welcome to junior honours group i catan AI.");
            string address = (args.Length > 0) ? args[0] : "138.251.207.83";
            Console.WriteLine("Address: " + address);
            int port = (args.Length > 1) ? int.Parse(args[1]) : 7000;
            Console.WriteLine("Port: " + port);
            if (args.Length > 3 && args[3] == "random") {
                ai = new RandomAI();
            } else {
                ai = new AIClass();
            }
            sleepActivated = true;
            foreach (string arg in args) {
                if (arg == "disableSleep") sleepActivated = false;
            }

            nm = new NetworkManager();
            nm.StartClient(this, address, port, (args.Length > 2) ? args[2] : "a wild AI");//start the networking thread
            loopThread = new Thread(new ThreadStart(runLoop));
            loopThread.Start();//run the loop thread which connects the networking and ai

            Console.WriteLine("network manager and the main loop has been created. program is now running.");
        }

        public void runLoop() {
            try {
                while (true) {
                    if (incomingEvents.Count != 0) {//if something unexpected happens
                        stateChecked = false;
                        rollBack();
                        lock (incomingEvents) {
                            //kill the ai thread because it is going to need to be restarted
                            Intergroup.Event unparsedAct = incomingEvents.First();
                            IAction act = nm.parse(this, unparsedAct, nm);
                            if (act is rejectRequest || act is Error) {//something very unexpected happens that the ai did not take into account. everything must be restarted.
                                Console.WriteLine("reject request or error given.");
                                act.print(this);
                            } else {
                                // if (!(act is GameSetup || act is UpdateLobby)) Console.WriteLine("resources owned: " + aiPlayerRef.totalResources() + " Lumber " + aiPlayerRef.getNumberResource(Const.LUMBER) + " Wheat " + aiPlayerRef.getNumberResource(Const.GRAIN) + " Brick " + aiPlayerRef.getNumberResource(Const.BRICK) + " Wool " + aiPlayerRef.getNumberResource(Const.WOOL) + " Ore " + aiPlayerRef.getNumberResource(Const.ORE));

                                act.run(this, theGameState);//update the game state to reflect the action
                                Console.WriteLine("running event sent by server:");
                                act.print(this);
                                // if (!(act is GameSetup || act is UpdateLobby)) Console.WriteLine("resources owned: " + aiPlayerRef.totalResources() + " Lumber " + aiPlayerRef.getNumberResource(Const.LUMBER) + " Wheat " + aiPlayerRef.getNumberResource(Const.GRAIN) + " Brick " + aiPlayerRef.getNumberResource(Const.BRICK) + " Wool " + aiPlayerRef.getNumberResource(Const.WOOL) + " Ore " + aiPlayerRef.getNumberResource(Const.ORE));
                                Console.WriteLine("");
                                eventParsed = true;
                            }
                            incomingEvents.Remove(unparsedAct);
                            if (incomingEvents.Count == 0 && !(act is UpdateLobby)) nm.syncedWithServer = true;//we have synced with the server at THIS point. the loop will be run again and check
                        }
                    } else {//nothing unexpected has happened. so either we are waiting for the server to respond or it has responded and we can continue
                        if (nm.syncedWithServer) {//server is synced, we can continue
                            if (outgoingActions.Count != 0) {//there are still things to do, decided by the ai
                                lock (outgoingActions) {
                                    //send that action to the server to check and confirm / reject
                                    //set the networkmanager to NOT synced.
                                    if (justRolledDice) {
                                        if (sleepActivated) System.Threading.Thread.Sleep(5000);
                                        justRolledDice = false;
                                    }
                                    IAction act = outgoingActions.First();
                                    if (act is RequestDiceRoll) justRolledDice = true;
                                    act.send(this, nm);//parses the action, sends it to the server, sets the networkmanager to not synced, i.e. waiting
                                    outgoingActions.Remove(act);//removes action from list, it is 'handled'
                                }
                            } else {//we have run out of things to do and the server is synced, we must start the ai to find more things to do
                                if ((aiThread == null || !aiThread.IsAlive) && !stateChecked) {//nothing in queue to send to server, server has been synced, ai is not generating things to do. lets run the ai!
                                    stateChecked = true;
                                    aiThread = new Thread(() => ai.run(this, nm));//this is just in case the ai was aborted and didn't finish, as restarting an aborted thread causes an exception
                                    aiThread.Start();
                                } else {
                                }
                            }
                        } else {
                            //we need to wait for the server, so essentially do nothing here.
                        }
                    }
                }
            } catch (GameOverException) {
                Console.WriteLine("Game ended. Goodbye!");
                nm.EndClient();
                aiThread.Join(500);
                loopThread.Join(500);
            }
        }
        public Boolean justRolledDice;

        public void rollBack() {
            if (aiThread != null) if (aiThread.IsAlive) aiThread.Abort();//kill the ai if necessary, as the server should not be publishing when the ai is working
            if (outgoingActions.Count > 0) lock (outgoingActions) outgoingActions.Clear();//remove all the ai's work, sad times
        }

        public NetworkManager getNm() {
            return nm;
        }
    }
}
