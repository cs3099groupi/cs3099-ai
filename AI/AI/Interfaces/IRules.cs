using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    interface IRules {
        //this class is probably going to be huge. 
        //example "rule"

        List<Tuple<ICoord, ICoord>> getValidRoadBuildSpots(IGameState state, IPlayer p);
        List<ICoord> getValidSettlementBuildSpots(IGameState state, IPlayer p);
        List<Tuple<ICoord, ICoord>> getValidRoadBuildSpotsStart(IGameState state, IPlayer p);
        List<ICoord> getValidSettlementBuildSpotsStart(IGameState state, IPlayer p);
        List<ISettlement> getValidCityBuildSpots(IGameState state, IPlayer p);
        List<Tuple<int[], int[]>> getValidBankTrades(IGameState g, IPlayer p);
        List<ICoord> getValidRobberMoveSpots(IGameState state);
        List<int[]> getValidDiscards(IPlayer p);
        List<ITile> getHexes(IGameState g, IStructure s);
        List<ITile> getHexes(IGameState g, ICoord c);
        List<IPlayer> getValidPlayersToStealFrom(IGameState g);
        //returns a list of players touching the robber, who are not the current player
        //returns boolean as to whether the player has the ability to build in that current gamestate
        //turn state, funds, number of buildings owned, possible locations, and current player making the turn
        //takes into account all rules:
        bool canAcceptTrade(Intergroup.Event tradeToAI, IPlayer p);
        bool canBuildSettlement(IGameState st, IPlayer p);
        bool canBuildCity(IGameState st, IPlayer p);
        bool canBuildRoad(IGameState st, IPlayer p);
        bool canBuyDevCard(IGameState st, IPlayer p);
        bool canPlayKnight(IGameState st, IPlayer p);//has a knight to play and hasn't already played a card this turn, and the game 'state' is either waitplay (mid turn) or turnstart (before rolling dice)
        bool canPlayYearOfPlenty(IGameState st, IPlayer p);//same as above but for year of plenty
        bool canPlayMonopoly(IGameState st, IPlayer p);//same but for monopoly
        bool canPlayRoadBuilding(IGameState st, IPlayer p);//same but for road building
        //returns boolean referring to whether the player has the funds. Does not take into account other game rules
        bool canAffordSettlement(IPlayer p);
        bool canAffordRoad(IPlayer p);
        bool canAffordCity(IPlayer p);
        bool canAffordDevCard(IPlayer p);

        int[] getCostRoad();//used in Actions for spending money on things, giving correct amount back to bank
        int[] getCostCity();
        int[] getCostSettlement();
        int[] getCostDevCard();
        int[] getResourceChoices();
        int checkDirection(IPlayer player, int x1, int x2, int y1, int y2, List<IRoad> visitedRoads, IGameState st);
        int checkUpDown(int x, int y);
        bool isCorner(int x, int y);
        bool isTile(int x, int y);
    }
}
