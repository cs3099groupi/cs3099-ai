﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    interface ITile {

        //returns a deep copy of the tile
        ITile copy();

        //gets the resource type of the tile
        int getResourceType();

        //gets the positon of the coordinate
        ICoord getPos();

        //gets the roll value of the tile
        int getRollValue();

        //set the resource type of the tile
        void SetResourceType(int i);

        //set the roll value of the tile
        void SetRollValue(int i);
    }
}
