using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    interface IActions {

        //REQUESTS ********************************************************************
        //requests that rely on server decisions e.g. get dice roll
        //the gamestate level changes are mostly just changing the 'state' variable in the IGamestate.
        //basically, these requests pause the game for the ai while the server has to make computation
        //these are the base cases for when the AI creates it's tree of possible things it can do. (as it is at these points it has to stop doing things)
        void requestDiceRoll(IGameState st);
        void requestBuyDevCard(IGameState st, IPlayer p);
        void initiateTradeRequest(IGameState st);//simply sets the game 'state' to awaiting trade response 
        void requestEndTurn(IGameState st);//although this is not actually non-deterministic (i.e. it has a set outcome) for the sake of creating stop points for the AI this is considered a stop point, where it ends its tree creation

        //EVENTS ********************************************************************
        //deterministic changes on the game state, changes that have a definitive outcome.
        //because these have only 1 result the AI can assume it's result and continue to model the changes on the game created by these decisions.
        void bankTrade(IGameState st, int[] resourcesIn, int[] resourcesOut, IPlayer p);//gives resources to bank from player, takes resources from bank to player (depends on ports possibly)
        void buildCity(IGameState st, ICoord pos, IPlayer p);//upgrades settlement at pos and gives point to player
        void buildSettlement(IGameState st, ICoord pos, IPlayer p);//adds settlement at pos, gives point to player, checks for breaking of longest road
        void buildRoad(IGameState st, ICoord pos1, ICoord pos2, IPlayer p);//adds road at pos, checks for longest road update
        void giveDevCard(IGameState st, int devCard, IPlayer p);
        void removeDevCardBoughtThisTurn(IGameState st, int devCard, IPlayer p);
        void giveDevCardBoughtThisTurn(IGameState st, int devCard, IPlayer p);
        void monopolyResolution(IGameState st, int resource, Dictionary<IPlayer, int> steals, IPlayer monopP);
        void playDevCard(IGameState st, int devCard, IPlayer p);//if it is a knight, then the robber is not moved because this event is always followed by moveRobber.

        void moveRobber(IGameState st, ICoord pos);
        void endTurn(IGameState st);//iterates the current player, sets the state to await turn start (or equivalent), sets the hasPlayedDevCardThoisTurnToFalse
        void spawnFreeRoads(IGameState st);
        void spawnFreeSettlements(IGameState st);
        void spawnBlankTiles(IBoard b);
        IGameState spawnEmptyGameState();//spawns a game state with a blank board
    }
}
