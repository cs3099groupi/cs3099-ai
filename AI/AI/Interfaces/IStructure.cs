﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    interface IStructure {
        IStructure copy(IPlayer p);
        ICoord getPos();
        IPlayer getOwner();

        void setOwner(IPlayer p);
        void print();
    }
}
