﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    interface IPlayer {
        //returns a deep copy of the player
        IPlayer copy();

        //get player id
        //used for checking player equality between copies of the game, 
        //and for parsing Intergroup. into player object 
        int getID();

        //set player id
        void setID(int i);

        //get the number of victory points for the player
        int points();

        //add victory points to the player
        void addPoints(int i);

        //remove victory points to the player
        void removePoints(int i);

        //increment total resource count
        void addToResourceCount();

        //decrement total resource count
        void removeFromResourceCount();

        //get all the resources from player
        int[] getResources();

        //get total number of resources 
        int totalResources();

        //get number of resources of a type 
        int getNumberResource(int type);

        //give type of resource by an amount to the player
        void GiveResource(int type, int amount);


        //remove the resources from player
        //index of ress represents the resource
        //the value at the index represents the count
        void RemoveResource(int type, int amount);

        //give the player resources
        //index of ress represents the resource
        //the value at the index represents the count
        void giveResources(int[] ress);

        //remove the resources from player
        //index of ress represents the resource
        //the value at the index represents the count
        void removeResources(int[] ress);

        //add a structure (does not set owner reference)
        void addStructure(IStructure str);

        //get the player's structures
        List<IStructure> getStructures();

        //get the player's cities and settlements
        List<ISettlement> getSettlementsAndCities();

        //get the player's settlements
        List<ISettlement> getSettlements();

        //get the player's ports
        List<IPort> getPorts();

        //add a port
        void addPort(IPort port);

        //get number of settlements
        int numSettlements();

        //get number of cities
        List<ISettlement> getCities();

        //get number of cities
        int numCities();

        //get the roads the player has
        List<IRoad> getRoads();

        //get the number of roads the player has
        int numRoads();

        //give player a dev card
        void giveDevCard(int str);

        //get count of particular type of dev card
        int getNumberDevCard(int type);

        //remove dev card bought this turn
        void removeDevCard(int str);

        //increment number of dev cards bought this turn
        void giveDevCardBoughtThisTurn(int str);


        //get number of dev card bought this turn
        int getNumberDevCardBoughtThisTurn(int type);

        //remove dev card bought this turn
        void removeDevCardBoughtThisTurn(int str);

        //get the dev cards bought this turn
        int[] getDevCardBoughtThisTurn();

        //add a knight to players army
        void addKnightToArmy();

        //get numbers of knights played
        int numKnightsPlayed();

        //set the longest road
        void SetLongestRoad(int i);

        //get the longest road
        int getLongestRoad();

        //print player details
        void print();

        //print player's resource counts
        void printResources();

        //checks if the player has discarded
        bool hasDiscarded();

        //sets if the player has discarded
        void setHasDiscarded(bool dsc);
    }
}