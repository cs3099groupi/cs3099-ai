﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    interface IStrategy {
        bool isLongestRoad();
        float buyRoadPriority(IGameState g, IPlayer p);
        float buySettlementPriority(IGameState g, IPlayer p);
        float buyCityPriority(IGameState g, IPlayer p);
        float buyDevCardPriority(IGameState g, IPlayer p);
        float getOreWeighting();
        float getWoolWeighting();
        float getLumberWeighting();
        float getBrickWeighting();
        float getGrainWeighting();
        float getResourceWeighting(int resource);
    }
}
