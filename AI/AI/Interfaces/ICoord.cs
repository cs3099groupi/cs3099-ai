﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    interface ICoord {

        //returns a deep copy of the coordinate
        ICoord copy();

        //create a coordinate with a change in x and y
        ICoord copyAdd(int x, int y);

        //get the x component of the coordinate
        int X();

        //get the y component of the coordinate
        int Y();

        //compare the coordinate by value
        bool isEqual(ICoord coord);

        //check if the coordinate is adjacent to this coordinate
        bool isAdjacent(ICoord coord);

        //convert the coordinate to string
        string toStr();
    }
}
