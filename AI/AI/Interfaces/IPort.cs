﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    interface IPort {

        //returns a deep copy of the port
        IPort copy();

        //get the resource type of the port
        int getResourceType();

        //get the coordinate of the port
        ICoord getPos();

        //gets the owner of the port
        IPlayer getOwner();

        //sets the owner of the port
        void setOwner(IPlayer p);
    }
}
