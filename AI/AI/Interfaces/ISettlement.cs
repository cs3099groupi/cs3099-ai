﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    interface ISettlement : IStructure {
        //upgrade the settlement to a city
        void upgradeToCity();

        //checks if the structure is a city
        Boolean isCity();
    }
}
