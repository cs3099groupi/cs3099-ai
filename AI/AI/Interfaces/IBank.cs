﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    interface IBank {

        //returns a deep copy of the bank
        IBank copy();

        //add a resource to the bank 
        //takes a resouce type and amount
        void addResource(int resource, int amount);

        //remove resource from the bank
        //takes a resource type and amount
        void removeResource(int resource, int amount);

        //give the bank resources
        //index represents the resource
        //value at the index represents the count 
        void giveResources(int[] ress);

        //remove resources from the bank
        //index represents the resource
        //value at the index represents the count  
        void removeResources(int[] ress);

        //get count of a particular resource
        int getResourceCount(int resource);

        //checks if the bank has the resource
        bool hasResource(int resource, int amount);

        //remove the dev card from the bank
        //takes the type of dev car and amount to remove
        void removeDevCard(int devCard, int amount);

        //decrements devcard total count
        void removeDevCard();

        //give dev card to the bank by the amount
        void giveDevCard(int devCard, int amount);

        //gets the number of dev cards in the bank
        int getDevCardCount(int devCard);

        //checks if the bank has a particular devcard
        bool hasDevCard(int devCard);

        //get the devcards in the bank
        int[] devCardDeck();

        //get the number of devcards in the bank 
        int getDevCardTotal();

        //get the total sum of all the resources in the bank
        int getResourceTotal();

        //print the resource contents of the bank
        void print();
    }
}
