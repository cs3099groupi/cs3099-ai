﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    interface IAction {
        void run(CatanAI ai, IGameState g);
        void print(CatanAI ai);
        void send(CatanAI ai, NetworkManager nm);
    }
}
