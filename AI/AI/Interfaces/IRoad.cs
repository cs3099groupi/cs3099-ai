﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    interface IRoad : IStructure {
        ICoord getPos2();

        //get the id of the road
        int getID();

        //set the id of the road
        void setID(int i);
    }
}
