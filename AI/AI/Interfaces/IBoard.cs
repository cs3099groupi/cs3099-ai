﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    interface IBoard {

        //returns a deep copy of the game
        IBoard copy();

        //get the tile based on the coordinate
        ITile getTile(ICoord pos);

        //get the port based on the coordinate
        IPort getPort(ICoord pos);

        //add a port to the list of ports
        void addPort(IPort p);

        //get the tiles based on the dice roll
        ITile[] getTiles(int diceRoll);

        //get the tiles of the board
        List<ITile> getTiles();

        //get the tiles of the board
        void setTiles(List<ITile> all);

        //generate a dictionary which maps roll values to tiles
        void generateRollValDic();
    }
}
