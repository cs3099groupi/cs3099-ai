using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    interface IGameState {

        //returns a deep copy of the game state
        IGameState copy();

        //get the board
        IBoard getBoard();

        //set the board
        void SetBoard(IBoard board);

        //get the bank
        IBank getBank();

        //set the bank
        void setBank(IBank b);

        //get the players
        List<IPlayer> getPlayers();

        //set the players
        void setPlayers(List<IPlayer> players);

        //adds the player to the list of players
        //adds the structures of that place to the list of structures
        void addPlayer(IPlayer p);

        //get the structures
        List<IStructure> getStructures();

        //get the settlements
        List<ISettlement> getSettlements();

        //get the roads
        List<IRoad> getRoads();

        //get the settlements and cities
        List<ISettlement> getSettlementsAndCities();

        //get the cities
        List<ISettlement> getCities();

        //add structure to the list of structures
        void addStructure(IStructure str);

        //set the structures
        void setStructures(List<IStructure> structures);

        //get the robber position
        ICoord getRobberPos();

        //set the robber positoin
        void setRobberPos(ICoord pos);

        //get the player based on id
        IPlayer getPlayer(int id);

        //get the current player
        IPlayer getCurrentPlayer();

        //set the current player
        void setCurrentPlayer(IPlayer p);

        //iterates the current player based on the player number
        void iterateCurrentPlayer();

        //decrements the current player
        void decrementCurrentPlayer();

        //gets current dice roll
        int getCurrentDiceRoll();

        //sets the current dice roll
        void setCurrentDiceRoll(int i);

        //get the longest road
        int getLongestRoad();

        //get the neighbouring settlements around a tile
        List<ISettlement> getNeighbourSettlements(ITile tile);

        //set the longest road
        void setLongestRoad(int i);

        //get the player which has the longest road
        IPlayer getLongestRoadPlayer();

        //set the player which has the longest road
        void setLongestRoadPlayer(IPlayer p);

        //get the player which has the largest army
        IPlayer getLargestArmyPlayer();

        //get the larget army
        int getLargestArmy();

        //set the player which has largest army 
        void setLargestArmyPlayer(IPlayer p);

        //set the count for the largest army
        void setLargestArmy(int i);

        //checks if a state is the same as the current game state
        bool isState(int s);

        //get the current state
        int getState();

        //set the current state
        void setState(int i);

        //checks if the dev card has been played this turn
        bool hasPlayedDevCardThisTurn();

        //sets dev card has been played this turn
        void setPlayedDevCardThisTurn();

        //sets dev card has not been played this turn
        void setNotPlayedDevCardThisTurn();

        //set free settlement spots 
        void setFreeSettlementSpots(List<ICoord> l);

        //get free settlement spots for the whole board
        List<ICoord> getFreeSettlementSpots();

        //get the last built settlement
        ISettlement getLastBuiltSettlement();

        //set the last built settlement
        void setLastBuildSettlement(ISettlement stl);

        //remove free settlement spots at a coordinate
        void removeFreeSettlementSpots(ICoord pos);

        //set free road spots
        void setFreeRoadSpots(List<Tuple<ICoord, ICoord>> l);

        //get free road spots on the board which can be  built on
        List<Tuple<ICoord, ICoord>> getFreeRoadSpots();

        //remove free road based on the road coordinates
        void removeFreeRoad(Tuple<ICoord, ICoord> tpl);

        //get road based on the coordinates
        IRoad getRoad(ICoord pos1, ICoord pos2);

        //get settlement at the coordinate
        ISettlement getSettlement(ICoord pos);

        //print game state information
        void print();
    }
}
