﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class Player : IPlayer {

        //returns a deep copy of the player
        public IPlayer copy() {
            Player newPlayer = new Player(victoryPoints, largestArmy, longestRoad, ID);//creates a new player with copies of primitive variables

            for (int i = 0; i < finalresources.Length; i++) {
                newPlayer.finalresources[i] = finalresources[i];
            }


            newPlayer.setHasDiscarded(discarded);
            newPlayer.devCards[Const.KNIGHT] = getNumberDevCard(Const.KNIGHT);
            newPlayer.devCards[Const.MONOPOLY] = getNumberDevCard(Const.MONOPOLY);
            newPlayer.devCards[Const.YEAR_OF_PLENTY] = getNumberDevCard(Const.YEAR_OF_PLENTY);
            newPlayer.devCards[Const.ROAD_BUILDING] = getNumberDevCard(Const.ROAD_BUILDING);
            newPlayer.devCards[Const.UNKNOWN_DEVCARD] = getNumberDevCard(Const.UNKNOWN_DEVCARD);

            newPlayer.devCardsBoughtThisTurn[Const.KNIGHT] = getNumberDevCardBoughtThisTurn(Const.KNIGHT);
            newPlayer.devCardsBoughtThisTurn[Const.MONOPOLY] = getNumberDevCardBoughtThisTurn(Const.MONOPOLY);
            newPlayer.devCardsBoughtThisTurn[Const.YEAR_OF_PLENTY] = getNumberDevCardBoughtThisTurn(Const.YEAR_OF_PLENTY);
            newPlayer.devCardsBoughtThisTurn[Const.ROAD_BUILDING] = getNumberDevCardBoughtThisTurn(Const.ROAD_BUILDING);
            newPlayer.devCardsBoughtThisTurn[Const.UNKNOWN_DEVCARD] = getNumberDevCardBoughtThisTurn(Const.UNKNOWN_DEVCARD);
            lock (structures) {
                foreach (IStructure str in structures) {
                    newPlayer.structures.Add(str.copy(newPlayer));//copies structures into the new player
                }
            }
            newPlayer.numberofresources = numberofresources;
            return newPlayer;
        }

        int[] finalresources;


        int numberofresources;

        int[] devCards;
        int[] devCardsBoughtThisTurn;
        private int ID;
        private int victoryPoints;
        private int longestRoad;
        private int largestArmy;
        private bool discarded;
        private bool discardRequested = false;

        //the structures that the player owns
        private List<IStructure> structures;
        private List<IPort> ports;

        public bool DiscardRequested {
            get {
                return discardRequested;
            }

            set {
                discardRequested = value;
            }
        }

        public Player(int vp, int la, int lr, int id) {
            //resources and devCards initialised to 0
            finalresources = new int[Const.NUM_OF_RESOURCES];
            devCards = new int[Const.NUM_OF_DEVCARDS + 1];
            devCardsBoughtThisTurn = new int[Const.NUM_OF_DEVCARDS + 1];

            this.ID = id;
            this.victoryPoints = vp;
            this.largestArmy = la;
            this.longestRoad = lr;
            this.structures = new List<IStructure>();
            this.ports = new List<IPort>();
        }

        //get player id
        //used for checking player equality between copies of the game, 
        //and for parsing Intergroup. into player object 
        public int getID() {
            return ID;
        }

        //give player a dev card
        public void giveDevCard(int card) {
            devCards[card]++;
        }

        //remove a dev card from player
        public void removeDevCard(int card) {
            devCards[card]--;
        }

        //get count of particular type of dev card
        public int getNumberDevCard(int type) {
            return devCards[type];
        }

        //increment number of dev cards bought this turn
        public void giveDevCardBoughtThisTurn(int card) {
            devCardsBoughtThisTurn[card]++;
        }

        //get the dev cards bought this turn
        public int[] getDevCardBoughtThisTurn() {
            return devCardsBoughtThisTurn;
        }

        //remove dev card bought this turn
        public void removeDevCardBoughtThisTurn(int card) {
            devCardsBoughtThisTurn[card]--;
        }

        //get number of dev card bought this turn
        public int getNumberDevCardBoughtThisTurn(int type) {
            return devCardsBoughtThisTurn[type];
        }

        //get the number of victory points for the player
        public int points() {
            return victoryPoints;
        }

        //add victory points to the player
        public void addPoints(int i) {
            this.victoryPoints += i;
        }

        //remove victory points from the player
        public void removePoints(int i) {
            this.victoryPoints -= i;
        }

        //remove type of resource by an amount to the player
        public void RemoveResource(int type, int amount) {
            //check each possibility.
            //each possibility that is compatible is kept the otherwise thrown away.
            //however after a list of compatible 
            finalresources[type] -= amount;
            numberofresources -= amount;
        }

        //give type of resource by an amount to the player
        public void GiveResource(int type, int amount) {
            finalresources[type] += amount;
            numberofresources += amount;
        }

        //get number of resources of a type 
        public int getNumberResource(int type) {
            return finalresources[type];
        }

        //get total number of resources 
        public int totalResources() {
            return numberofresources;
        }

        //add a structure (does not set owner reference)
        public void addStructure(IStructure str) {
            lock (structures)
                structures.Add(str);
        }

        //get the player's structures
        public List<IStructure> getStructures() {
            return structures;
        }

        //get the player's cities and settlements
        public List<ISettlement> getSettlementsAndCities() {
            return structures.Where(str => str is ISettlement).Select(x => (ISettlement)x).ToList();
        }

        //get the player's settlements
        public List<ISettlement> getSettlements() {
            return structures.Where(str => str is ISettlement && !((ISettlement)str).isCity()).Select(x => (ISettlement)x).ToList();
        }

        //get the player's ports
        public List<IPort> getPorts() {
            return ports;
        }

        //add a port
        public void addPort(IPort port) {
            ports.Add(port);
        }

        //get number of settlements
        public int numSettlements() {
            return getSettlements().Count();
        }

        //get cities the player owns
        public List<ISettlement> getCities() {
            return structures.Where(str => str is ISettlement && ((ISettlement)str).isCity()).Select(x => (ISettlement)x).ToList();
        }

        //get number of cities
        public int numCities() {
            return getCities().Count();
        }

        //get the roads the player has
        public List<IRoad> getRoads() {
            return structures.Where(str => str is IRoad).Select(x => (IRoad)x).ToList();
        }

        //get the number of roads the player has
        public int numRoads() {
            return getRoads().Count();
        }

        //add a knight to players army
        public void addKnightToArmy() {
            largestArmy++;
        }

        //get numbers of knights played
        public int numKnightsPlayed() {
            return largestArmy;
        }

        //set the longest road
        public void SetLongestRoad(int i) {
            longestRoad = i;
        }

        //get the longest road
        public int getLongestRoad() {
            return longestRoad;
        }

        //give the player resources
        //index of ress represents the resource
        //the value at the index represents the count
        public void giveResources(int[] ress) {
            for (int i = 0; i < ress.Length; i++) {
                GiveResource(i, ress[i]);
            }
        }

        //remove the resources from player
        //index of ress represents the resource
        //the value at the index represents the count
        public void removeResources(int[] ress) {
            for (int i = 0; i < ress.Length; i++) {
                RemoveResource(i, ress[i]);
            }
        }

        //get all the resources from player
        public int[] getResources() {
            return finalresources;
        }

        //print player details
        public void print() {

            Console.WriteLine("\nPlayer: " + ID);
            Console.WriteLine("points: " + points());
            Console.WriteLine("largest army: " + largestArmy);
            Console.WriteLine("longest road: " + longestRoad);
            Console.WriteLine("Structures: ");
            lock (structures)
                foreach (IStructure s in structures) {
                    s.print();
                }

            printResources();
        }

        //print player's resource counts
        public void printResources() {
            Console.WriteLine("Ore:" + getNumberResource(Const.ORE));
            Console.WriteLine("Wool:" + getNumberResource(Const.WOOL));
            Console.WriteLine("Brick:" + getNumberResource(Const.BRICK));
            Console.WriteLine("Grain:" + getNumberResource(Const.GRAIN));
            Console.WriteLine("Lumber:" + getNumberResource(Const.LUMBER));
        }

        //set player id
        public void setID(int i) {
            this.ID = i;
        }

        //checks if the player has discarded
        public bool hasDiscarded() {
            return discarded;
        }

        //sets if the player has discarded
        public void setHasDiscarded(bool dsc) {
            discarded = dsc;
        }

        //increment total resource count
        public void addToResourceCount() {
            numberofresources++;
        }

        //decrement total resource count
        public void removeFromResourceCount() {
            numberofresources--;
        }
    }
}
