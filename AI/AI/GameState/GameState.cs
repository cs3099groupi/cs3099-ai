﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace AI {
    class GameState : IGameState {

        //VARIABLES
        //****************************************
        private IBoard board;
        private IBank bank;
        private List<IPlayer> players;
        private List<IStructure> structures;
        public List<ICoord> freeSettlementSpots;
        private List<Tuple<ICoord, ICoord>> freeRoadSpots;
        private ICoord robberPos;
        private int longestRoad;
        private IPlayer longestRoadPlayer;
        private int largestArmy;
        private IPlayer largestArmyPlayer;
        private IPlayer currentPlayer;
        private ISettlement lastBuiltSettlement;
        private int currentDiceRoll;
        private bool hasPlayedDevCardThisTurn;
        private int state;//the state of the game e.g. initial move in progress, awaitplay, await trade acceptance
        //****************************************

        public GameState() {
            structures = new List<IStructure>();
            players = new List<IPlayer>();
            freeSettlementSpots = new List<ICoord>();
            freeRoadSpots = new List<Tuple<ICoord, ICoord>>();
        }


        //returns a deep copy of the game state
        public IGameState copy() {
            GameState newState = new GameState(); //create new copy (blank at the moment)
            newState.state = state;
            if (board != null) newState.board = board; //DOESNT COPY THE BOARD, BECAUSE THE BOARD SHOULD NEVER NEED TO BE CHANGED
            if (bank != null) newState.bank = bank.copy();
            if (robberPos != null) newState.robberPos = robberPos.copy();
            if (lastBuiltSettlement != null) newState.lastBuiltSettlement = (ISettlement)lastBuiltSettlement.copy(lastBuiltSettlement.getOwner());
            newState.currentDiceRoll = currentDiceRoll; //primitive type copies automatically by value
            newState.setLongestRoad(longestRoad);
            newState.setLargestArmy(largestArmy);
            newState.hasPlayedDevCardThisTurn = hasPlayedDevCardThisTurn;
            lock (players)
                foreach (Player p in players) {
                    IPlayer newP = p.copy();
                    newState.players.Add(newP); //add each new player to the new list of players
                    foreach (IStructure str in newP.getStructures()) {
                        lock (newState.structures)
                            newState.structures.Add(str); //add each structure that the new player owned to the new list of structures
                    }
                    if (longestRoadPlayer != null) {
                        if (p == longestRoadPlayer) {
                            newState.setLongestRoadPlayer(newP);
                        }
                    }
                    if (largestArmyPlayer != null) {
                        if (p == largestArmyPlayer) {
                            newState.setLargestArmyPlayer(newP);
                        }
                    }
                    if (p == currentPlayer) {
                        newState.currentPlayer = newP; //if the new player is the equivalent to the current player then set it tothe new current player
                    }
                }
            foreach (ICoord pos in freeSettlementSpots) {
                newState.freeSettlementSpots.Add(pos.copy()); //add each pos in the free settlement list
            }

            foreach (Tuple<ICoord, ICoord> poss in freeRoadSpots) {
                newState.freeRoadSpots.Add(new Tuple<ICoord, ICoord>(poss.Item1.copy(), poss.Item2.copy()));//add each pos tuple  in the free road list
            }

            return newState;
        }

        //get the board
        public IBoard getBoard() {
            return board;
        }

        //set the board
        public void SetBoard(IBoard board) {
            this.board = board;
        }

        //get the bank
        public IBank getBank() {
            return bank;
        }

        //set the bank
        public void setBank(IBank b) {
            this.bank = b;
        }

        //get the players
        public List<IPlayer> getPlayers() {
            return players;
        }

        //set the players
        public void setPlayers(List<IPlayer> players) {
            this.players = players;
        }

        //adds the player to the list of players
        //adds the structures of that place to the list of structures
        public void addPlayer(IPlayer p) {
            players.Add(p);
            foreach (IStructure str in p.getStructures()) {
                lock (structures)
                    structures.Add(str);
            }
        }

        //get the structures
        public List<IStructure> getStructures() {
            return structures;
        }

        //set the structures
        public void setStructures(List<IStructure> structures) {
            this.structures = structures;
        }

        //get the robber position
        public ICoord getRobberPos() {
            return robberPos;
        }

        //set the robber position
        public void setRobberPos(ICoord pos) {
            this.robberPos = pos;
        }

        //get current player
        public IPlayer getCurrentPlayer() {
            return currentPlayer;
        }

        //set current player
        public void setCurrentPlayer(IPlayer p) {
            this.currentPlayer = p;
        }

        //get current dice roll
        public int getCurrentDiceRoll() {
            return currentDiceRoll;
        }

        //set current dice roll
        public void setCurrentDiceRoll(int i) {
            this.currentDiceRoll = i;
        }

        //checks if a state is the same as the current game state
        public bool isState(int s) {
            return s == state;
        }

        //get the state
        public int getState() {
            return state;
        }

        //add structure to the list of structures
        public void addStructure(IStructure str) {
            lock (structures)
                structures.Add(str);
        }

        //get settlements and cities 
        public List<ISettlement> getSettlementsAndCities() {
            return structures.Where(str => str is ISettlement).Select(x => (ISettlement)x).ToList();
        }

        //get the settlements
        public List<ISettlement> getSettlements() {//returns settlements not including cities
            return structures.Where(str => str is ISettlement && !((ISettlement)str).isCity()).Select(x => (ISettlement)x).ToList();
        }

        //get the cities
        public List<ISettlement> getCities() {
            return structures.Where(str => str is ISettlement && ((ISettlement)str).isCity()).Select(x => (ISettlement)x).ToList();
        }

        //get the roads
        public List<IRoad> getRoads() {
            return structures.Where(str => str is IRoad).Select(x => (IRoad)x).ToList();
        }

        //get free settlement spots for the whole board
        public List<ICoord> getFreeSettlementSpots() {
            return freeSettlementSpots;
        }

        //get free road spots on the board which can be  built on
        public List<Tuple<ICoord, ICoord>> getFreeRoadSpots() {
            return freeRoadSpots;
        }

        //remove free settlement spots at a coordinate
        public void removeFreeSettlementSpots(ICoord pos) {
            List<ICoord> poss = new List<ICoord>();
            lock (freeSettlementSpots)
                foreach (Coord freePos in freeSettlementSpots) {
                    if (freePos.isEqual(pos) || freePos.isAdjacent(pos)) {
                        poss.Add(freePos);
                    }
                }
            foreach (Coord posi in poss) {
                freeSettlementSpots.Remove(posi);
            }
        }

        //remove free road based on the road coordinates
        public void removeFreeRoad(Tuple<ICoord, ICoord> tpl) {
            lock (freeRoadSpots)
                foreach (Tuple<ICoord, ICoord> freeTpl in freeRoadSpots) {
                    if ((freeTpl.Item1.isEqual(tpl.Item1) && freeTpl.Item2.isEqual(tpl.Item2)) || (freeTpl.Item2.isEqual(tpl.Item1) && freeTpl.Item1.isEqual(tpl.Item2))) {
                        freeRoadSpots.Remove(freeTpl);
                        //Console.WriteLine("free road has been removed at " + freeTpl.Item1.toStr() + " " + freeTpl.Item2.toStr());
                        return;
                    }
                }
        }

        //get the last build settlement
        public ISettlement getLastBuiltSettlement() {
            if (lastBuiltSettlement != null) return lastBuiltSettlement;
            return null;//wups
        }

        //set the last built settlement
        public void setLastBuildSettlement(ISettlement stl) {
            lastBuiltSettlement = stl;
        }

        //set free settlement spots
        public void setFreeSettlementSpots(List<ICoord> l) {
            this.freeSettlementSpots = l;
        }

        //set free road spots
        public void setFreeRoadSpots(List<Tuple<ICoord, ICoord>> l) {
            this.freeRoadSpots = l;
        }

        //get longest road
        public int getLongestRoad() {
            return longestRoad;
        }

        //set longest road
        public void setLongestRoad(int i) {
            longestRoad = i;
        }

        //get the player which has the longest road
        public IPlayer getLongestRoadPlayer() {
            if (longestRoadPlayer != null) return longestRoadPlayer;
            return null;
        }

        //set the player which has the longest road
        public void setLongestRoadPlayer(IPlayer p) {
            longestRoadPlayer = p;
        }

        //get road based on the coordinates
        public IRoad getRoad(ICoord pos1, ICoord pos2) {
            lock (structures)
                foreach (IRoad r in getRoads()) {
                    if (r.getPos().isEqual(pos1) && r.getPos2().isEqual(pos2)) return r;
                    if (r.getPos2().isEqual(pos1) && r.getPos().isEqual(pos2)) return r;
                }
            return null;
        }

        //get settlement at the coordinate
        public ISettlement getSettlement(ICoord pos) {
            lock (structures)
                foreach (ISettlement stl in getSettlements()) {
                    if (stl.getPos().isEqual(pos)) return stl;
                }
            return null;
        }

        //iterates the current player based on the player number
        public void iterateCurrentPlayer() {
            int current = currentPlayer.getID();
            current++;
            if (current == players.Count) current -= players.Count;
            lock (players)
                foreach (IPlayer p in players) {
                    if (p.getID() == current) {
                        setCurrentPlayer(p);
                        return;
                    }
                }
        }

        //decrements the current player, for use in initial moves only
        public void decrementCurrentPlayer() {
            int current = currentPlayer.getID();
            current--;
            if (current == -1) current += players.Count;
            lock (players)
                foreach (IPlayer p in players) {
                    if (p.getID() == current) {
                        setCurrentPlayer(p);
                        return;
                    }
                }
        }

        //set the current game state to i
        public void setState(int i) {
            state = i;
        }

        //checks if the dev card has been played this turn
        bool IGameState.hasPlayedDevCardThisTurn() {
            return hasPlayedDevCardThisTurn;
        }

        //sets dev card has been played this turn
        public void setPlayedDevCardThisTurn() {
            hasPlayedDevCardThisTurn = true;
        }

        //sets dev card has not been played this turn
        public void setNotPlayedDevCardThisTurn() {
            hasPlayedDevCardThisTurn = false;
        }

        //get the player which has the largest army
        public IPlayer getLargestArmyPlayer() {
            return largestArmyPlayer;
        }

        //get the count of the largest army
        public int getLargestArmy() {
            return largestArmy;
        }

        //set the player which has largest army 
        public void setLargestArmyPlayer(IPlayer p) {
            largestArmyPlayer = p;
        }

        //set the count for the largest army
        public void setLargestArmy(int i) {
            largestArmy = i;
        }

        //get player based on id
        public IPlayer getPlayer(int id) {
            lock (players)
                foreach (IPlayer p in players) {
                    if (p.getID() == id) return p;
                }
            return null;
        }

        //get neighbouring settlements around a tile
        //this checks the edges of the tile for settlement
        //returns all the settlements on the tiles edges
        public List<ISettlement> getNeighbourSettlements(ITile tile) {
            int x = tile.getPos().X();
            int y = tile.getPos().Y();
            List<ISettlement> buildings = getSettlementsAndCities();
            List<ISettlement> neighbouringSettlements = new List<ISettlement>();
            lock (buildings)
                foreach (var building in buildings) {
                    int bX = building.getPos().X();
                    int bY = building.getPos().Y();

                    if (bX == x && bY == (y + 1)) {
                        neighbouringSettlements.Add(building);
                    } else if (bX == (x + 1) && bY == (y + 1)) {
                        neighbouringSettlements.Add(building);
                    } else if (bX == (x + 1) && bY == (y)) {
                        neighbouringSettlements.Add(building);
                    } else if (bX == (x) && bY == (y - 1)) {
                        neighbouringSettlements.Add(building);
                    } else if (bX == (x - 1) && bY == (y - 1)) {
                        neighbouringSettlements.Add(building);
                    } else if (bX == (x - 1) && bY == (y)) {
                        neighbouringSettlements.Add(building);
                    } else {
                        continue;
                    }
                }

            return neighbouringSettlements;
        }

        //print game state information
        public void print() {
            Console.WriteLine("\nPRINTING GAME STATE\n");
            Console.WriteLine("current state: " + stateToString());
            if (robberPos != null) Console.WriteLine("robber pos: " + robberPos.toStr());
            if (currentPlayer != null) Console.WriteLine("current Player: " + currentPlayer.getID());
            if (longestRoadPlayer != null) Console.WriteLine("longest road player: " + longestRoadPlayer.getID());
            if (largestArmyPlayer != null) Console.WriteLine("largest army player: " + largestArmyPlayer.getID());
            Console.WriteLine("");
            lock (players)
                foreach (IPlayer p in players) {
                    p.print();
                }
        }

        //get the string translation of the current state
        public String stateToString() {
            switch (state) {
                case Const.AWAITINGSTEAL:
                    return "awaiting resource stolen";
                case Const.AWAITINGENDTURN:
                    return "awaiting end of turn";
                case Const.AWAITDISCARD:
                    return "awaiting discards";
                case Const.AWAITINGTRADERESPONSE:
                    return "awaiting response to trade";
                case Const.AWAITINGDEVCARDBUYING:
                    return "awaiting dev card buying";
                case Const.AWAITINGDICEROLL:
                    return "awaiting dice generation";
                case Const.INITIALSETTLEMENT:
                    return "awaiting initial settlement production";
                case Const.INITIALROAD:
                    return "awaiting initial road production";
                case Const.TURNSTART:
                    return "start of turn";
                case Const.WAITPLAY:
                    return "making move...";
                case Const.FIRSTROADBUILDING:
                case Const.FIRSTROADBUILDINGBEFOREROLLINGTHEDICE:
                    return "choosing first road building location";
                case Const.SECONDROADBUILDING:
                case Const.SECONDROADBUILDINGBEFOREROLLINGTHEDICE:
                    return "choosing second road building location";
                case Const.FIRSTYEAROFPLENTY:
                case Const.FIRSTYEAROFPLENTYBEFOREROLLINGTHEDICE:
                    return "choosing first year of plenty resource";
                case Const.SECONDYEAROFPLENTY:
                case Const.SECONDYEAROFPLENTYBEFOREROLLINGTHEDICE:
                    return "choosing second year of plenty resource";
                case Const.CHOOSINGMONOPOLY:
                case Const.CHOOSINGMONOPOLYBEFOREROLLINGTHEDICE:
                    return "choosing resource to monopolise";
                case Const.CHOOSINGROBBERPLACEMENT:
                case Const.AWAITINGROBBERPLACEMENTDUETOKNIGHTBEINGPLAYEDBEFOREROLLINGDICE:
                    return "choosing spot to move robber to";
                case Const.RESOURCETOSTEAL:
                    return "choosing lpayer to steal from";
            }
            return "error invalid state: " + state;
        }
    }
}
