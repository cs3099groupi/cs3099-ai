﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class Road : IRoad {

        private IPlayer owner;
        private ICoord pos; //first half of the road end point coordinate
        private ICoord pos2;//second half of the road end point coordinate
        private int ID;

        //requires coordinates of both end points of the road
        public Road(ICoord pos, ICoord pos2) {
            this.pos = pos;
            this.pos2 = pos2;
        }

        //returns a copy of the road, and gives the copy a reference to the player argument
        public IStructure copy(IPlayer p) {
            Road newRd = new Road(pos.copy(), pos2.copy());
            newRd.owner = p;
            return newRd;
        }

        //get the owner of the road
        public IPlayer getOwner() {
            return owner;
        }

        //set the owner of the road
        public void setOwner(IPlayer p) {
            this.owner = p;
        }

        //get the coordinate of the end point of the first half of the road
        public ICoord getPos() {
            return pos;
        }

        //get the coordinate of the end point of the second half of the road
        public ICoord getPos2() {
            return pos2;
        }

        //get the id of the road
        public int getID() {
            return ID;
        }

        //set the id of the road
        public void setID(int id) {
            ID = id;
        }

        //prints the position of the road end points
        public void print() {
            Console.WriteLine("Road: " + pos.toStr() + " " + pos2.toStr());
        }
    }
}