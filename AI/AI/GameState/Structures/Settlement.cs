﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class Settlement : ISettlement {

        private Boolean city;
        private IPlayer owner;
        private ICoord pos;

        //returns a copy of the structure and gives the copy a reference to the player argument
        public IStructure copy(IPlayer p) {
            Settlement newStl = new Settlement(pos.copy());
            newStl.owner = p;
            if (city) newStl.upgradeToCity();
            return newStl;
        }

        //upgrade the settlement to a city
        public void upgradeToCity() {
            city = true;
        }

        //checks if the structure is a city
        public Boolean isCity() {
            return city;
        }

        //get the position of the settlement
        public ICoord getPos() {
            return pos;
        }

        //get the owner of the settlement
        public IPlayer getOwner() {
            return owner;
        }

        //set the owner of the settlement
        public void setOwner(IPlayer p) {
            this.owner = p;
        }

        //print the information about the Structure
        public void print() {
            String t = isCity() ? "City" : "Settlement";
            Console.WriteLine(t + ": " + pos.toStr());
        }

        public Settlement(ICoord pos) {
            this.pos = pos;
            city = false;
        }
    }
}
