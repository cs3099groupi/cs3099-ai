﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {

    class Bank : IBank {

        private int[] resourceVault;
        private int[] devCardVault;
        private int devCardTotal;

        //returns a deep copy of the bank
        public IBank copy() {
            return new Bank(resourceVault[Const.BRICK],
                            resourceVault[Const.LUMBER],
                            resourceVault[Const.BRICK],
                            resourceVault[Const.WOOL],
                            resourceVault[Const.GRAIN],

                            devCardVault[Const.KNIGHT],
                            devCardVault[Const.ROAD_BUILDING],
                            devCardVault[Const.MONOPOLY],
                            devCardVault[Const.YEAR_OF_PLENTY],
                            devCardVault[Const.VICTORY_POINT],
                            devCardTotal
                           );
        }

        //add a resource to the bank 
        //takes a resouce type and amount
        public void addResource(int resource, int amount) {
            resourceVault[resource] += amount;
        }

        //remove resource from the bank
        //takes a resource type and amount
        public void removeResource(int resource, int amount) {
            Console.WriteLine("REMOVING RESOURCE: " + resource);
            resourceVault[resource] -= amount;
        }

        //get count of a particular resource
        public int getResourceCount(int resource) {
            return resourceVault[resource];
        }

        //checks if the bank has the resource
        public bool hasResource(int resource, int amount) {
            Console.WriteLine("Resource: " + resource);
            return getResourceCount(resource) >= amount;
        }

        //remove the dev card from the bank
        //takes the type of dev car and amount to remove
        public void removeDevCard(int devCard, int amount) {
            devCardVault[devCard] -= amount;
        }

        //decrements devcard total count
        public void removeDevCard() {
            devCardTotal--;
        }

        //gets the number of dev cards in the bank
        public int getDevCardCount(int devCard) {
            return devCardVault[devCard];
        }

        //checks if the bank has a particular devcard
        public bool hasDevCard(int devCard) {
            return getDevCardCount(devCard) > 1;
        }

        public Bank(int bricks, int lumber, int wool, int grain, int ore,
                   int devKnight, int devRoadBuilding, int DevMonopoly, int devYearOfPlenty, int devVictoryPoint, int devTotal) {

            resourceVault = new int[Const.NUM_OF_RESOURCES];
            resourceVault[Const.BRICK] = bricks;
            resourceVault[Const.LUMBER] = lumber;
            resourceVault[Const.WOOL] = wool;
            resourceVault[Const.GRAIN] = grain;
            resourceVault[Const.ORE] = ore;

            devCardVault = new int[Const.NUM_OF_DEVCARDS];
            devCardVault[Const.KNIGHT] = devKnight;
            devCardVault[Const.ROAD_BUILDING] = devRoadBuilding;
            devCardVault[Const.MONOPOLY] = DevMonopoly;
            devCardVault[Const.YEAR_OF_PLENTY] = devYearOfPlenty;
            devCardVault[Const.VICTORY_POINT] = devVictoryPoint;
            devCardTotal = devTotal;
        }

        //Initialises the Bank Vault to starting values
        public Bank() {
            resourceVault = new int[Const.NUM_OF_RESOURCES];
            for (int i = 0; i < resourceVault.Length; i++) {
                resourceVault[i] = Const.Q_BANK_RESOURCES;
            }


            devCardVault = new int[Const.NUM_OF_DEVCARDS];
            devCardVault[Const.KNIGHT] = Const.Q_KNIGHT_DEV;
            devCardVault[Const.ROAD_BUILDING] = Const.Q_PROGRESS_DEV;
            devCardVault[Const.MONOPOLY] = Const.Q_PROGRESS_DEV;
            devCardVault[Const.YEAR_OF_PLENTY] = Const.Q_PROGRESS_DEV;
            devCardVault[Const.VICTORY_POINT] = Const.Q_VP_DEV;
            devCardTotal = Const.Q_DEV_TOTAL;
        }

        //give the bank resources
        //index represents the resource
        //value at the index represents the count 
        public void giveResources(int[] ress) {
            for (int i = 0; i < ress.Length; i++) {
                resourceVault[i] += ress[i];
            }
        }

        //remove resources from the bank
        //index represents the resource
        //value at the index represents the count 
        public void removeResources(int[] ress) {
            for (int i = 0; i < ress.Length; i++) {
                resourceVault[i] -= ress[i];
            }
        }

        //give dev card to the bank by the amount
        public void giveDevCard(int devCard, int amount) {
            devCardVault[devCard] += amount;
        }

        //get the devcards in the bank
        public int[] devCardDeck() {
            return devCardVault;
        }

        //get the number of devcards in the bank 
        public int getDevCardTotal() {
            return devCardTotal;
        }

        //print the resource contents of the bank
        public void print() {
            Console.WriteLine("ORE: " + resourceVault[Const.ORE]);
            Console.WriteLine("LUMBER: " + resourceVault[Const.LUMBER]);
            Console.WriteLine("BRICKS: " + resourceVault[Const.BRICK]);
            Console.WriteLine("WOOL: " + resourceVault[Const.WOOL]);
            Console.WriteLine("GRAIN: " + resourceVault[Const.GRAIN]);
        }

        //get the total sum of all the resources in the bank
        public int getResourceTotal() {
            return resourceVault[Const.BRICK] +
                resourceVault[Const.LUMBER] +
                resourceVault[Const.BRICK] +
                resourceVault[Const.WOOL] +
                resourceVault[Const.GRAIN];
        }
    }
}
