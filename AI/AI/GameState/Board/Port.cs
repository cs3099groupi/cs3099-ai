﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class Port : IPort {

        //returns a deep copy of the port
        public IPort copy() {
            IPort p = new Port(type, pos.copy());
            return p;
        }

        //port type, same as resource type + universal
        private int type;
        private ICoord pos;
        private IPlayer owner = null;

        //gets the owner of the port
        public IPlayer getOwner() {
            return owner;
        }

        //sets the owner of the port
        public void setOwner(IPlayer p) {
            owner = p;
        }

        //get the resource type of the port
        public int getResourceType() {
            return type;
        }

        //get the coordinate of the port
        public ICoord getPos() {
            return pos;
        }

        public Port(int type, ICoord pos) {
            this.type = type;
            if (pos != null) this.pos = pos;
        }
    }
}
