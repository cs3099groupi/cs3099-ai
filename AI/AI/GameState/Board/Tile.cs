﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class Tile : ITile {

        //returns a deep copy of the tile
        public ITile copy() {
            return new Tile(resourceType, rollValue, pos.copy());
        }

        private int resourceType;
        private int rollValue;
        private ICoord pos;

        //gets the resource type of the tile
        public int getResourceType() {
            return resourceType;
        }

        //gets the roll value of the tile
        public int getRollValue() {
            return rollValue;
        }

        //gets the positon of the coordinate
        public ICoord getPos() {
            return pos;
        }

        //set the resource type of the tile
        public void SetResourceType(int i) {
            this.resourceType = i;
        }

        //set the roll value of the tile
        public void SetRollValue(int i) {
            this.rollValue = i;
        }

        public Tile(int type, int val, ICoord pos) {
            this.resourceType = type;
            this.rollValue = val;
            if (pos != null) this.pos = pos;
        }
    }
}
