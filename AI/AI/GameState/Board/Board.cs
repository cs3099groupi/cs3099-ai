﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class Board : IBoard {

        //returns a deep copy of the game
        public IBoard copy() {
            //copy the contents of the tiles
            List<ITile> newTiles = new List<ITile>();
            foreach (ITile t in tiles) {
                newTiles.Add(t.copy());
            }
            //copy the contents of the ports
            List<IPort> newPorts = new List<IPort>();
            foreach (IPort p in ports) {
                newPorts.Add(p.copy());
            }
            //create the new board
            IBoard newB = new Board(newTiles, newPorts);
            return newB;
        }

        public Board() {
            tiles = new List<ITile>();
            ports = new List<IPort>();
        }

        private List<ITile> tiles;
        private List<IPort> ports;
        private Dictionary<int, ITile[]> rollValueDict = new Dictionary<int, ITile[]>();

        //get the tile based on the coordinate
        public ITile getTile(ICoord pos) {
            foreach (ITile t in tiles) {
                if (t.getPos().isEqual(pos)) {
                    return t;
                }
            }
            return null;
        }

        //get the port based on the coordinate
        public IPort getPort(ICoord pos) {
            foreach (IPort p in ports) {
                if (p.getPos().isEqual(pos)) {
                    return p;
                }
            }
            return null;
        }

        //get the tiles based on the dice roll
        public ITile[] getTiles(int diceRoll) {
            return rollValueDict[diceRoll];
        }


        public Board(List<ITile> tiles, List<IPort> ports) {
            this.ports = ports;
            this.tiles = tiles;
            generateRollValDic();
        }

        //generate a dictionary which maps roll values to tiles
        public void generateRollValDic() {
            List<ITile> roltiles;
            for (int i = 2; i < 13; i++) {
                if (i != 7) {
                    roltiles = new List<ITile>();
                    foreach (ITile t in tiles) {
                        if (t.getRollValue() == i) {
                            roltiles.Add(t);
                        }
                    }
                    rollValueDict.Add(i, roltiles.ToArray());
                }
            }
        }

        //get the tiles of the board
        public List<ITile> getTiles() {
            return tiles;
        }

        //get the tiles of the board
        public void setTiles(List<ITile> all) {
            tiles = all;
        }

        //add a port to the list of ports
        public void addPort(IPort p) {
            ports.Add(p);
        }
    }
}
