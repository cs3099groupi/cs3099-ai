﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class Coord : ICoord {

        //returns a deep copy of the coordinate
        public ICoord copy() {
            return new Coord(x, y);
        }

        //create a coordinate with a change in x and y
        public ICoord copyAdd(int x, int y) {
            return new Coord(this.x + x, this.y + y);
        }

        private int x;//the x component of the coordinate
        private int y;//the y component

        //get the x component of the coordinate
        public int X() {
            return x;
        }

        //get the y component of the coordinate
        public int Y() {
            return y;
        }

        //compare the coordinate by value
        public bool isEqual(ICoord coord) {
            return ((x == coord.X()) && (y == coord.Y()));
        }

        //check if the coordinate is adjacent to this coordinate
        public bool isAdjacent(ICoord coord) {
            if (x == coord.X()) {
                return (Math.Abs(y - coord.Y()) == 1);
            }
            if (y == coord.Y()) {
                return (Math.Abs(x - coord.X()) == 1);
            }
            return (x - coord.X() == y - coord.Y() && Math.Abs(x - coord.X()) == 1);
        }

        //convert the coordinate to string
        public string toStr() {
            return "(" + x + "," + y + ")";
        }

        public Coord(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
