﻿using System;
namespace AI {
    using System.Collections.Generic;
    using NUnit.Framework;

    [TestFixture]
    public class ActionsTests {

        static IActions a = new Actions();
        IGameState g = new GameState();

        public void initialiseGame() {
            IPlayer p1 = new Player(0, 0, 0, 1);
            IPlayer p2 = new Player(0, 0, 0, 2);
            IPlayer p3 = new Player(0, 0, 0, 3);
            IPlayer p4 = new Player(0, 0, 0, 4);
            a.spawnFreeRoads(g);
            a.spawnFreeSettlements(g);
            ICoord robberPos = new Coord(0, 0);
            g.setRobberPos(robberPos);
            g.SetBoard(initialiseBoard());
            g.addPlayer(p1);
            g.addPlayer(p2);
            g.addPlayer(p3);
            g.addPlayer(p4);
            g.setBank(new Bank());
        }

        private IBoard initialiseBoard() {

            //brick = 3, ore = 3
            //2 of everything, 12 & 2 = 1, no 7
            List<ITile> tiles = new List<ITile> {

                new Tile(Const.WOOL, 3, new Coord(-2,2)),
                new Tile(Const.WOOL, 3, new Coord(0,3)),
                new Tile(Const.WOOL, 3, new Coord(2,4)),
                new Tile(Const.WOOL, 3, new Coord(-3,0)),
                new Tile(Const.WOOL, 3, new Coord(-1,1)),
                new Tile(Const.WOOL, 3, new Coord(1,2)),
                new Tile(Const.WOOL, 3, new Coord(3,3)),
                new Tile(Const.WOOL, 3, new Coord(-4,-2)),
                new Tile(Const.WOOL, 3, new Coord(-2,-1)),
                new Tile(Const.WOOL, 3, new Coord(0,0)),
                new Tile(Const.WOOL, 3, new Coord(2,1)),
                new Tile(Const.WOOL, 3, new Coord(4,2)),
                new Tile(Const.WOOL, 3, new Coord(-3,-3)),
                new Tile(Const.WOOL, 3, new Coord(-1,-2)),
                new Tile(Const.WOOL, 3, new Coord(1,-1)),
                new Tile(Const.WOOL, 3, new Coord(3,0)),
                new Tile(Const.WOOL, 3, new Coord(-2,-4)),
                new Tile(Const.WOOL, 3, new Coord(0,-3)),
                new Tile(Const.WOOL, 3, new Coord(2,-2)),
            };

            Board board = new Board(tiles, new List<IPort>());
            board.setTiles(tiles);
            return board;
        }
    }
}
