﻿
namespace AI {
    using System.Collections.Generic;
    using NUnit.Framework;

    [TestFixture]
    public class GameStateTests {
        static readonly IRules r = new Rules();

        [Test]
        public void CoordCreation() {
            int x = 1;
            int y = 2;

            ICoord coord = new Coord(x, y);

            Assert.AreEqual(x, coord.X());
            Assert.AreEqual(y, coord.Y());
        }

        [Test]
        public void CoordCopying() {
            int x = 1;
            int y = 2;
            ICoord coord = new Coord(x, y);
            ICoord coordCopy = coord.copy();

            //Testing for reference
            Assert.AreNotSame(coord, coordCopy);

            //Testing for value
            Assert.IsTrue(coord.isEqual(coordCopy));
        }

        [Test]
        public void BoardCreationAddTiles() {
            int x = 1;
            int y = 2;

            List<ITile> tiles = new List<ITile>();
            List<IPort> ports = new List<IPort>();

            IBoard board = new Board(tiles, ports);
            ICoord pos = new Coord(x, y);

            ITile t = new Tile(x, y, pos);
            tiles.Add(t);

            Assert.AreSame(t, board.getTile(pos));
        }

        [Test]
        public void BoardCreationCopy() {
            int x = 1;
            int y = 2;
            int rollValue = 7;

            List<ITile> tiles = new List<ITile>();
            List<IPort> ports = new List<IPort>();

            IBoard board = new Board(tiles, ports);
            ICoord pos = new Coord(x, y);

            ITile t = new Tile(Const.GRAIN, rollValue, pos);
            tiles.Add(t);

            IBoard board2 = board.copy();

            //Check copy of tile:
            ITile tCopy = board2.getTile(new Coord(x, y));
            Assert.AreEqual(tCopy.getResourceType(), Const.GRAIN);
            Assert.AreEqual(tCopy.getRollValue(), rollValue);
            Assert.AreEqual(tCopy.getPos().X(), x);
            Assert.AreEqual(tCopy.getPos().Y(), y);
        }

        [Test]
        public void PlayerStructureFunctions() {
            IPlayer p = new Player(0, 0, 0, 0);
            ICoord pos = new Coord(1, 2);
            ISettlement stl1 = new Settlement(pos);
            ISettlement stl2 = new Settlement(pos);
            ISettlement city = new Settlement(pos);
            IRoad rd1 = new Road(pos, pos);
            IRoad rd2 = new Road(pos, pos);
            IRoad rd3 = new Road(pos, pos);

            city.upgradeToCity();
            p.addStructure(stl1);
            p.addStructure(stl2);
            p.addStructure(city);
            p.addStructure(rd1);
            p.addStructure(rd2);
            p.addStructure(rd3);

            Assert.AreEqual(p.numCities(), 1);
            Assert.AreEqual(p.numRoads(), 3);
            Assert.AreEqual(p.numSettlements(), 2);
            Assert.AreEqual(p.getSettlementsAndCities().Count, 3);
            Assert.IsTrue(p.getCities()[0].isCity());
            Assert.AreEqual(p.getCities().Count, 1);
        }

        [Test]
        public void PlayerStructureCopyFunctions() {
            IPlayer p = new Player(0, 0, 0, 0);
            ICoord pos = new Coord(1, 2);
            ISettlement stl1 = new Settlement(pos);
            ISettlement stl2 = new Settlement(pos);
            ISettlement city = new Settlement(pos);
            IRoad rd1 = new Road(pos, pos);
            IRoad rd2 = new Road(pos, pos);
            IRoad rd3 = new Road(pos, pos);

            city.upgradeToCity();
            p.addStructure(stl1);
            p.addStructure(stl2);
            p.addStructure(city);
            p.addStructure(rd1);
            p.addStructure(rd2);
            p.addStructure(rd3);

            p = p.copy();

            Assert.AreEqual(p.numCities(), 1);
            Assert.AreEqual(p.numRoads(), 3);
            Assert.AreEqual(p.numSettlements(), 2);
            Assert.AreEqual(p.getSettlementsAndCities().Count, 3);
            Assert.IsTrue(p.getCities()[0].isCity());
            Assert.AreEqual(p.getCities().Count, 1);

            Assert.IsTrue(p.getCities()[0] != city);
            Assert.AreNotSame(stl1, p.getSettlements()[0]);
        }

        [Test]
        public void GameStateStructureFunctions() {
            IGameState g = new GameState();
            IPlayer p1 = new Player(0, 0, 0, 0);
            IPlayer p2 = new Player(0, 0, 0, 0);
            IPlayer p3 = new Player(0, 0, 0, 0);

            p1.addStructure(new Settlement(new Coord(1, 2)));
            g.addPlayer(p1);
            g.addPlayer(p2);
            g.addPlayer(p3);

            Assert.AreEqual(g.getSettlements().Count, 1);
            Assert.AreEqual(g.getPlayers().Count, 3);
            Assert.AreEqual(g.getPlayers()[0].getSettlements()[0], g.getSettlements()[0]);
        }

        [Test]
        public void GameStateStructureCopyFunctions() {
            IGameState g = new GameState();
            IPlayer p1 = new Player(0, 0, 0, 0);
            IPlayer p2 = new Player(0, 0, 0, 0);
            IPlayer p3 = new Player(0, 0, 0, 0);

            p1.addStructure(new Settlement(new Coord(1, 2)));
            g.addPlayer(p1);
            g.addPlayer(p2);
            g.addPlayer(p3);

            IGameState g2 = g.copy();

            Assert.AreEqual(g2.getSettlements().Count, 1);
            Assert.AreEqual(g2.getPlayers().Count, 3);
            Assert.AreNotEqual(g2.getPlayers()[0].getSettlements()[0], g.getSettlements()[0]);
            Assert.AreEqual(g2.getPlayers().Count, 3);
        }

        [Test]
        public void ValidSettlementBuildSpots() {
            GameState g = new GameState();
            g.freeSettlementSpots.Add(new Coord(0, 0));
            g.freeSettlementSpots.Add(new Coord(1, 0));
            g.freeSettlementSpots.Add(new Coord(0, 1));
            g.freeSettlementSpots.Add(new Coord(1, 1));
            g.freeSettlementSpots.Add(new Coord(2, 1));
            g.freeSettlementSpots.Add(new Coord(1, 2));
            g.freeSettlementSpots.Add(new Coord(2, 2));
            g.freeSettlementSpots.Add(new Coord(2, 3));
            g.freeSettlementSpots.Add(new Coord(3, 2));
            g.freeSettlementSpots.Add(new Coord(3, 3));
            g.freeSettlementSpots.Add(new Coord(4, 4));
            g.freeSettlementSpots.Add(new Coord(5, 5));
            g.freeSettlementSpots.Add(new Coord(4, 5));

            IPlayer p = new Player(0, 0, 0, 0);
            p.addStructure(new Road(new Coord(1, 1), new Coord(1, 2)));
            g.addPlayer(p);

            Assert.AreEqual(g.getFreeSettlementSpots().Count, 13);
            Assert.AreEqual(r.getValidSettlementBuildSpots(g, p).Count, 2);
        }

        [Test]
        public void longestRoadCheck() {
            IGameState g = new GameState();
            IPlayer p = new Player(0, 0, 0, 0);
            IActions a = new Actions();
            a.buildRoad(g, new Coord(0, 1), new Coord(1, 1), p);
            a.buildRoad(g, new Coord(1, 1), new Coord(1, 0), p);
            a.buildRoad(g, new Coord(1, 0), new Coord(2, 0), p);
            a.buildRoad(g, new Coord(1, 0), new Coord(0, -1), p);
            a.buildRoad(g, new Coord(2, 0), new Coord(3, 1), p);
            a.buildRoad(g, new Coord(3, 1), new Coord(3, 2), p);
            a.buildRoad(g, new Coord(3, 2), new Coord(4, 3), p);

            Assert.AreEqual(p.getLongestRoad(), 6);
            Assert.AreEqual(p.numRoads(), 7);
            Assert.AreEqual(g.getLongestRoadPlayer(), p);
            Assert.AreEqual(g.getLongestRoad(), p.getLongestRoad());
        }

        [Test]
        public void brokenLongestRoadChec() {
            IGameState g = new GameState();
            IPlayer p = new Player(0, 0, 0, 0);
            g.addPlayer(p);
            IActions a = new Actions();
            a.spawnFreeSettlements(g);
            a.spawnFreeRoads(g);
            g.SetBoard(new Board());
            a.spawnBlankTiles(g.getBoard());
            a.buildRoad(g, new Coord(0, 1), new Coord(1, 1), p);
            a.buildRoad(g, new Coord(1, 1), new Coord(1, 0), p);
            a.buildRoad(g, new Coord(1, 0), new Coord(2, 0), p);
            a.buildRoad(g, new Coord(1, 0), new Coord(0, -1), p);
            a.buildRoad(g, new Coord(2, 0), new Coord(3, 1), p);
            a.buildRoad(g, new Coord(3, 1), new Coord(3, 2), p);
            a.buildRoad(g, new Coord(3, 2), new Coord(4, 3), p);
            IPlayer p2 = new Player(0, 0, 0, 0);
            g.addPlayer(p2);
            bool passed = true;
            passed = p.getLongestRoad() == 6 && g.getLongestRoad() == 6 && (g.getLongestRoadPlayer() != null) && p.points() == 2;
            a.buildSettlement(g, new Coord(2, 0), p2);

            Assert.AreEqual(p.getLongestRoad(), 3);
            Assert.AreEqual(p.numRoads(), 7);
            Assert.IsNull(g.getLongestRoadPlayer());
            Assert.AreEqual(g.getLongestRoad(), 0);
            Assert.AreEqual(p.points(), 0);
            Assert.IsTrue(passed);
        }
    }
}
