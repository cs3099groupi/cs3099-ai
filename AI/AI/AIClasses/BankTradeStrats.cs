﻿using System;
using System.Collections.Generic;

namespace AI {
    class BankTradeStrats {
        //uses recursion to find and evaluate options available once the bank trade has been doe.
        internal static float valueOfAction(CatanAI ai, IGameState endState, IGameState startState, BankTrade bankTrade, IStrategy theChosenStrat, NetworkManager nm) {
            bool hasResource = false;
            for (int i = 0; i < bankTrade.resourcesOut.Length; i++) {
                if (bankTrade.resourcesOut[i] >= 1) {
                    switch (i) {
                        case Const.WOOL: if (Heuristics.getMe(ai, startState).getNumberResource(i) >= 1) { hasResource = true; }; break;
                        case Const.BRICK: if (Heuristics.getMe(ai, startState).getNumberResource(i) >= 1) { hasResource = true; }; break;
                        case Const.LUMBER: if (Heuristics.getMe(ai, startState).getNumberResource(i) >= 1) { hasResource = true; }; break;
                        case Const.GRAIN: if (Heuristics.getMe(ai, startState).getNumberResource(i) >= 2) { hasResource = true; }; break;
                        case Const.ORE: if (Heuristics.getMe(ai, startState).getNumberResource(i) >= 3) { hasResource = true; }; break;
                        default: Console.WriteLine("Error this should never happen"); hasResource = true; break;
                    }
                }
            }
            float choiceValue = 0;
            if (!hasResource) {
                AIClass AI = new AIClass();
                ChoiceGen generator = new ChoiceGen();
                //runs the choice generator on the state created by the action
                List<Choice> choices = generator.generateChoices(ai, endState, Heuristics.getMe(ai, endState), nm);
                //runs the ai on each choice available after trading to get the best choice
                Choice bestChoice = AI.chooseChoice(ai, choices, endState, Heuristics.getMe(ai, endState), nm, true);
                if (bestChoice.getActions().Count > 0 && !(bestChoice.getActions()[0] is EndTurn)) {
                    //gets the value of the choice
                    choiceValue = AI.evaluateChoice(ai, endState, bestChoice, AI.findActionPriorities(endState, Heuristics.getMe(ai, endState), AI.chooseStrat(endState, Heuristics.getMe(ai, endState))), AI.chooseStrat(endState, Heuristics.getMe(ai, endState)), bestChoice, 0, nm, true);
                    choiceValue += getChoiceValue(bankTrade, theChosenStrat);
                } else if (Heuristics.getMe(ai, endState).totalResources() >= 10) {
                    choiceValue = getChoiceValue(bankTrade, theChosenStrat);
                }
            }
            return choiceValue * 0.95F;//returns a little less than doing it without trading
        }

        //if the action after trading is end turn, i.e. nothing was found that was useful
        //then heuristics are used to define the value
        internal static float getChoiceValue(BankTrade bankTrade, IStrategy theChosenStrat) {
            float choiceValue = 0;
            for (int i = 0; i < bankTrade.resourcesIn.Length; i++) {
                for (int j = 0; j < bankTrade.resourcesIn[i]; j++) {
                    choiceValue -= theChosenStrat.getResourceWeighting(i);//removes the value of the lost resources
                }
            }
            for (int i = 0; i < bankTrade.resourcesOut.Length; i++) {
                for (int j = 0; j < bankTrade.resourcesOut[i]; j++) {
                    choiceValue += theChosenStrat.getResourceWeighting(i) * 1.5F;//adds the value of the incoming resources
                }
            }
            return choiceValue;
        }
    }
}
