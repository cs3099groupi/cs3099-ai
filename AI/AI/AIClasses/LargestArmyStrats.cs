﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class LargestArmyStrats : IStrategy {

        public float ore = 0;
        public float wool = 0;
        public float lumber = 0;
        public float brick = 0;
        public float grain = 0;

        public LargestArmyStrats(float ore, float wool, float lumber, float brick, float grain) {
            this.ore = ore;
            this.wool = wool;
            this.lumber = lumber;
            this.brick = brick;
            this.grain = grain;
        }

        //returns the strategies priority for cities.
        //In theory this could depend on the game state but in reality it is hard coded to a high value
        public float buyCityPriority(IGameState g, IPlayer p) {
            return 0.9F;//largest army considers cities to be really strong
        }

        public float buyDevCardPriority(IGameState g, IPlayer p) {
            // as you get more income, and as you get closer to 10 points, devcards become more valuable.
            //also because they do not share resources with roads it doesn't really hurt to buy them late game as longest road player
            return p.points() + 0.1F;//a hacky way to make the player want to buy dev cards in the late game, but should not be used to overwrite buying a city!
        }

        //returns a rather small value because largest army doesn't really care about roads much
        public float buyRoadPriority(IGameState g, IPlayer p) {
            if (g.getState() == Const.INITIALROAD) return 1.0F;
            return 0.3F;
        }

        public float buySettlementPriority(IGameState g, IPlayer p) {
            if (g.getState() == Const.INITIALSETTLEMENT) return 1.0F;
            return 0.6F;
        }

        public bool isLongestRoad() {
            return false;
        }

        public float getOreWeighting() {
            return ore;
        }

        public float getWoolWeighting() {
            return wool;
        }

        public float getLumberWeighting() {
            return lumber;
        }

        public float getBrickWeighting() {
            return brick;
        }

        public float getGrainWeighting() {
            return grain;
        }

        public float getResourceWeighting(int resource) {
            switch (resource) {
                case Const.ORE: return ore;
                case Const.WOOL: return wool;
                case Const.LUMBER: return lumber;
                case Const.BRICK: return brick;
                case Const.GRAIN: return grain;
                default: return 0;
            }
        }
    }
}
