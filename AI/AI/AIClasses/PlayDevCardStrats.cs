﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class PlayDevCardStrats {
        //returns a value for choosing which of any development cards to play.
        //always returns 0 if the player has not yet rolled the dice because it is never really necessary to play a card before rolling.
        internal static float valueOfAction(CatanAI ai, IGameState currentState, PlayDevCard devCard) {
            switch (devCard.devCardType) {
                case Const.KNIGHT: return playKnight(ai, currentState);
                case Const.ROAD_BUILDING: return playRoadBuilding(currentState);
                case Const.YEAR_OF_PLENTY: return playYOP(currentState);
                case Const.MONOPOLY: return playMonopoly(currentState);
            }
            throw new InvalidOperationException();
        }

        private static float playMonopoly(IGameState currentState) {
            if (currentState.getState() == Const.WAITPLAY) {
                return 800F;
            } else {
                return 0;
            }
        }

        private static float playYOP(IGameState currentState) {
            if (currentState.getState() == Const.WAITPLAY) {
                return 1000;
            } else {
                return 0;
            }
        }

        private static float playRoadBuilding(IGameState currentState) {
            return 700;
        }

        private static float playKnight(CatanAI ai, IGameState currentState) {
            //returns 0 unless certain criteria are met.
            if (Heuristics.getMe(ai, currentState).getNumberDevCard(Const.KNIGHT) > 1) {//ifthe player has more than 1 knight then it gets rid of one.
                return 500;
            } else if (Heuristics.getMe(ai, currentState) != currentState.getLargestArmyPlayer() && Heuristics.getMe(ai, currentState).numKnightsPlayed() == currentState.getLargestArmy()) {
                return 1200;//if playing the knight gives the player largest army then definitely play the knight.
            }
            List<ISettlement> robberNeighbours = currentState.getNeighbourSettlements(currentState.getBoard().getTile(currentState.getRobberPos()));
            foreach (ISettlement s in robberNeighbours) {
                if (s.getOwner() == Heuristics.getMe(ai, currentState)) {
                    return 500;//if the player is blocked by the robber currently then play a knight
                }
            }
            return 0;//if none of the above are met then there is no real reason to play the knight
        }
    }
}
