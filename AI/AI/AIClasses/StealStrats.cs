﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class StealStrats {
        //returns a value of a steal choice.
        //scales directly with the targets points
        internal static float valueOfAction(IGameState currentState, RequestSteal steal) {
            return currentState.getPlayer(steal.targetPlayer).points();
        }
    }
}
