﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI {
    class BuildInitialRoadStrats {
        //a programming abstraction of getting the value of initial road positions.
        //In this version it simply calls the normal road heuristics value
        internal static float valueOfAction(CatanAI ai, IGameState currentState, BuildInitialRoad initialRoad, IStrategy theChosenStrat) {
            return BuyRoadStrats.valueOfAction(ai, currentState, new BuyRoad(initialRoad.pos, initialRoad.pos2), theChosenStrat);
        }
    }
}
