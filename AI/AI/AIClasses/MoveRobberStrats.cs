﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class MoveRobberStrats {
        //returns a value defining how good a spot to place the robber is.
        internal static float valueOfAction(CatanAI ai, IGameState currentState, MoveRobber robber) {
            bool playerHasCardsToSteal = false;
            float blockValue = 0;
            List<ISettlement> neighbours = currentState.getNeighbourSettlements(currentState.getBoard().getTile(robber.pos));
            foreach (ISettlement s in neighbours) {
                if (s.getOwner() == Heuristics.getMe(ai, currentState)) {
                    return 0;//returns 0 to prevent the player ever blocking itself
                }
                if (currentState.getBoard().getTile(robber.pos).getResourceType() != Const.DESERT) {
                    blockValue += (float)Const.ROLL_PROBABILITY[currentState.getBoard().getTile(robber.pos).getRollValue()] + (float)s.getOwner().points();
                    if (s.isCity()) {
                        blockValue += (float)Const.ROLL_PROBABILITY[currentState.getBoard().getTile(robber.pos).getRollValue()] + (float)s.getOwner().points();//provides a vlaue that combines the points of the person to steal from and also the roll probability
                    }
                } else {
                    blockValue = 1F;
                }
                if (s.getOwner().totalResources() > 0) {
                    playerHasCardsToSteal = true;
                }
            }
            if (!playerHasCardsToSteal) {
                blockValue = blockValue * 0.5F;//reduces the value if the player can't be stolen from
            }
            return blockValue;
        }
    }
}
