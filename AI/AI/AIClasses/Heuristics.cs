﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class Heuristics {

        private static IRules rules = new Rules();

        //encapsulates getting the ai player from a game state
        public static IPlayer getMe(CatanAI ai, IGameState g) {
            return g.getPlayer(ai.aiPlayerID);
        }

        //returns the average of resource that p is getting each turn in g
        public static double getIncome(IGameState g, IPlayer p, int resource) {
            double total = 0.0;
            foreach (ISettlement stl in p.getSettlements()) {
                foreach (ITile t in rules.getHexes(g, stl)) {
                    if (t.getResourceType() == resource) {
                        total += Const.ROLL_PROBABILITY[t.getRollValue()];
                    }
                }
            }
            foreach (ISettlement stl in p.getCities()) {
                foreach (ITile t in rules.getHexes(g, stl)) {
                    if (t.getResourceType() == resource) {
                        total += Const.ROLL_PROBABILITY[t.getRollValue()];
                        total += Const.ROLL_PROBABILITY[t.getRollValue()];//twice because cities
                    }
                }
            }
            return total;
        }

        //returns how many resources a given settlement will get you per turn
        public static double getSettlementTotalIncome(ICoord c, IGameState g) {
            double total = 0.0;
            total += getSettlementIncome(c, g, Const.ORE);
            total += getSettlementIncome(c, g, Const.GRAIN);
            total += getSettlementIncome(c, g, Const.WOOL);
            total += getSettlementIncome(c, g, Const.LUMBER);
            total += getSettlementIncome(c, g, Const.BRICK);
            return total;
        }

        //returns the average resources a settlement will get you per turn in a given resource
        public static double getSettlementIncome(ICoord c, IGameState g, int resource) {
            double total = 0.0;
            foreach (ITile t in rules.getHexes(g, c)) {
                if (t.getResourceType() == resource) {
                    total += Const.ROLL_PROBABILITY[t.getRollValue()];
                }
            }
            return total;
        }

        //returns how many resources a given city will get you per turn
        public static double getCityTotalIncome(ICoord c, IGameState g) {
            return getSettlementTotalIncome(c, g) * 2;
        }

        //returns the average resources a city will get you per turn in a given resource
        public static double getCityIncome(ICoord c, IGameState g, int resource) {
            return getSettlementIncome(c, g, resource) * 2;
        }

        public static List<ICoord> getSurroundingCoordinates(ICoord c) {
            List<ICoord> coordinateList = new List<ICoord>();
            int x = c.X();
            int y = c.Y();
            for (int i = x - 1; i <= x + 1; i++) {
                for (int j = y - 1; j <= y + 1; j++) {
                    if (c.isAdjacent(new Coord(i, j))) {
                        coordinateList.Add(new Coord(i, j));
                    }
                }
            }
            return coordinateList;
        }
    }
}
