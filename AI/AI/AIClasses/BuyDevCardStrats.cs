﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class BuyDevCardStrats {
        //returns a value representing whether the player wants to buy a devcard.
        public static float valueOfAction(CatanAI ai, IGameState currentState, BuyDevCard devCard, IStrategy theChosenStrat) {
            IPlayer p = Heuristics.getMe(ai, currentState);
            int largestArmyOther = 0;
            //calculates the other player's largest army to see if it's own largest army is "threatened"
            foreach (IPlayer player in currentState.getPlayers()) {
                if (player != p && player.numKnightsPlayed() > largestArmyOther) {
                    largestArmyOther = player.numKnightsPlayed();
                }
            }
            if (theChosenStrat is LargestArmyStrats) {
                //prioritises devcards if the player doesn't have the largest army cemented
                if (p.numKnightsPlayed() < 3 || p.numKnightsPlayed() - largestArmyOther < 2) {
                    return 2;
                }
            }
            //if the ai needs to get rid of cards it will want to buy a devcard so return a high value
            if (p.totalResources() >= 8) {
                return 2;
            }
            return 0;
        }
    }
}
