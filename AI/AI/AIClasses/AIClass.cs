﻿using AI;
using System;
using System.Collections.Generic;
namespace AI {
    class AIClass : Decider {

        public const int BUYROAD = 0;
        public const int BUYSETTLEMENTS = 1;
        public const int BUYCITY = 2;
        public const int BUYDEVCARD = 3;

        //overrides the abstract method. defines how this implementation of an AI chooses a choice.
        public override Choice chooseChoice(CatanAI ai, List<Choice> choices, IGameState currentState, IPlayer p, NetworkManager nm, bool recursive) {
            IStrategy theChosenStrat = chooseStrat(currentState, p);//choose the strategy to follow
            float[] actionPriorities = findActionPriorities(currentState, p, theChosenStrat);//use the strategy to find the weights of different action types
            float maxPriority = 0;
            Choice choiceToMake = choices[0];
            foreach (Choice opt in choices) {
                if (opt.getActions().Count > 0) {
                    float choiceValue = evaluateChoice(ai, currentState, opt, actionPriorities, theChosenStrat, choiceToMake, maxPriority, nm, recursive);
                    if (choiceValue > maxPriority) {
                        maxPriority = choiceValue;
                        choiceToMake = opt;
                    }
                }
            }
            return choiceToMake;
        }

        //depending on the action, the game state, and the choice actions, this generates a float defining how good the choice is.
        public float evaluateChoice(CatanAI ai, IGameState originalState, Choice opt, float[] actionPriorities, IStrategy theChosenStrat, Choice bestChoice, float maxPriority, NetworkManager nm, bool recursive) {
            float choiceTotal = 0;
            foreach (IAction theAction in opt.getActions()) {
                float theActionValue = new float();
                if (theAction is BuyRoad) {
                    theActionValue = BuyRoadStrats.valueOfAction(ai, originalState, (BuyRoad)theAction, theChosenStrat);
                    theActionValue *= actionPriorities[BUYROAD];
                } else if (theAction is BuildRoadBuildingRoad) {
                    theActionValue = BuyRoadStrats.valueOfActionRoadBuildingVersion(ai, originalState, (BuildRoadBuildingRoad)theAction, theChosenStrat);
                    theActionValue *= actionPriorities[BUYROAD];
                } else if (theAction is BuySettlement) {
                    theActionValue = BuySettlementStrats.valueOfAction(ai, originalState, (BuySettlement)theAction, theChosenStrat);
                    theActionValue *= actionPriorities[BUYSETTLEMENTS];
                } else if (theAction is BuyCity) {
                    theActionValue = BuyCityStrats.valueOfAction(originalState, (BuyCity)theAction, theChosenStrat);
                    theActionValue *= actionPriorities[BUYCITY];
                } else if (theAction is BuyDevCard) {
                    theActionValue = BuyDevCardStrats.valueOfAction(ai, originalState, (BuyDevCard)theAction, theChosenStrat);
                    theActionValue *= actionPriorities[BUYDEVCARD];
                } else if (theAction is EndTurn) {
                    theActionValue = EndTurnStrats.valueOfAction(ai, originalState, bestChoice, maxPriority, theChosenStrat);
                } else if (theAction is BuildInitialRoad) {
                    theActionValue = BuildInitialRoadStrats.valueOfAction(ai, originalState, (BuildInitialRoad)theAction, theChosenStrat);
                } else if (theAction is BuildInitialSettlement) {
                    theActionValue = BuildInitialSettlementStrats.valueOfAction(ai, originalState, (BuildInitialSettlement)theAction, theChosenStrat);
                } else if (theAction is PlayDevCard) {
                    theActionValue = PlayDevCardStrats.valueOfAction(ai, originalState, (PlayDevCard)theAction);
                } else if (theAction is ChooseResource) {
                    theActionValue = ChooseResourceStrats.valueOfAction(ai, originalState, (ChooseResource)theAction, theChosenStrat);
                } else if (theAction is MoveRobber) {
                    theActionValue = MoveRobberStrats.valueOfAction(ai, originalState, (MoveRobber)theAction);
                } else if (theAction is Discard) {
                    theActionValue = DiscardStrats.valueOfAction(ai, originalState, (Discard)theAction, theChosenStrat, opt.state(), nm);
                } else if (theAction is BankTrade && !recursive) {
                    theActionValue = BankTradeStrats.valueOfAction(ai, opt.state(), originalState, (BankTrade)theAction, theChosenStrat, nm);
                } else if (theAction is RequestSteal) {
                    theActionValue = StealStrats.valueOfAction(originalState, (RequestSteal)theAction);
                } else if (theAction is RequestDiceRoll) {
                    theActionValue = 1;
                } else if (theAction is ConfirmPlayerTrade) {
                    theActionValue = PlayerTradeStrats.valueOfAccept(ai, originalState, (ConfirmPlayerTrade)theAction, theChosenStrat);
                } else if (theAction is RejectPlayerTrade) {
                    theActionValue = PlayerTradeStrats.valueOfReject(originalState, (RejectPlayerTrade)theAction, theChosenStrat);
                } else if (theAction is RequestPlayerTrade) {
                    theActionValue = 0;//dont request player trades. This is out of the implementations scope.
                }
                choiceTotal += theActionValue;
            }
            return choiceTotal;
        }

        //this AI has 2 strategies it follows. This method chooses a strategy
        public IStrategy chooseStrat(IGameState g, IPlayer p) {
            //Chooses either longest road or largest army strat
            /*
             priorities generated via mathematical averages of paths to victory via this strategy
               
                ##Longest Road Average##
                0 dev cards
                2 cities
                6 settlements (must build city before settlement)
                11 roads

                Costs:
                Ore: 6
                Wool: 4
                Lumber: 13
                Brick: 13
                Grain: 8
                Total: 44

                Percentage Distributions
                Ore: 13.636%
                Wool: 9.091%
                Lumber: 29.545%
                Brick: 29.545%
                Grain: 18.182%

                ##Largest Army Average##
                7 dev cards - 4 knights, 1 vp, 2 others (other cards save 3 resources on average)
                3 cities
                4 settlements
                6 roads

                Costs:
                Ore: 16
                Wool: 9
                Lumber: 6
                Brick: 6
                Grain: 15
                Total: 52
                Other Cards: -6
                Total: 46

                Percentage Distributions
                Ore: 30.769%
                Wool: 17.308%
                Lumber: 11.538%
                Brick: 11.538%
                Grain: 28.846%
            */
            double percentageOreLongestRoad = 13.636;
            double percentageWoolLongestRoad = 9.091;
            double percentageLumberLongestRoad = 29.545;
            double percentageBrickLongestRoad = 29.545;
            double percentageGrainLongestRoad = 18.182;

            double percentageOreLargestArmy = 30.769;
            double percentageWoolLargestArmy = 17.308;
            double percentageLumberLargestArmy = 11.538;
            double percentageBrickLargestArmy = 11.538;
            double percentageGrainLargestArmy = 28.846;


            double longestRoadPriority = 0;
            double largestArmyPriority = 0;
            double longestRoadResources = 0;
            double largestArmyRescources = 0;
            double longestRoadAvailableResources = 0;
            double largestArmyAvailableResources = 0;
            double longestRoadCurrentRescources = 0;
            double largestArmyCurrentResources = 0;

            double oreProbability = 0;
            double brickProbability = 0;
            double lumberProbability = 0;
            double grainProbability = 0;
            double woolProbability = 0;

            for (int i = 0; i < g.getBoard().getTiles().Count; i++) {
                if (g.getBoard().getTiles()[i].getResourceType() == Const.ORE) {
                    oreProbability += Const.ROLL_PROBABILITY[g.getBoard().getTiles()[i].getRollValue()];
                } else if (g.getBoard().getTiles()[i].getResourceType() == Const.BRICK) {
                    brickProbability += Const.ROLL_PROBABILITY[g.getBoard().getTiles()[i].getRollValue()];
                } else if (g.getBoard().getTiles()[i].getResourceType() == Const.LUMBER) {
                    lumberProbability += Const.ROLL_PROBABILITY[g.getBoard().getTiles()[i].getRollValue()];
                } else if (g.getBoard().getTiles()[i].getResourceType() == Const.GRAIN) {
                    grainProbability += Const.ROLL_PROBABILITY[g.getBoard().getTiles()[i].getRollValue()];
                } else if (g.getBoard().getTiles()[i].getResourceType() == Const.WOOL) {
                    woolProbability += Const.ROLL_PROBABILITY[g.getBoard().getTiles()[i].getRollValue()];
                }
            }

            longestRoadResources = oreProbability * percentageOreLongestRoad + woolProbability * percentageWoolLongestRoad + lumberProbability * percentageLumberLongestRoad + brickProbability * percentageBrickLongestRoad + grainProbability * percentageGrainLongestRoad;
            largestArmyRescources = oreProbability * percentageOreLargestArmy + woolProbability * percentageWoolLargestArmy + lumberProbability * percentageLumberLargestArmy + brickProbability * percentageBrickLargestArmy + grainProbability * percentageGrainLargestArmy;

            double availableOre = 0;
            double availableBrick = 0;
            double availableLumber = 0;
            double availableGrain = 0;
            double availableWool = 0;

            foreach (ICoord c in g.getFreeSettlementSpots()) {
                availableOre += Heuristics.getSettlementIncome(c, g, Const.ORE);
                availableBrick += Heuristics.getSettlementIncome(c, g, Const.BRICK);
                availableLumber += Heuristics.getSettlementIncome(c, g, Const.LUMBER);
                availableGrain += Heuristics.getSettlementIncome(c, g, Const.GRAIN);
                availableWool += Heuristics.getSettlementIncome(c, g, Const.WOOL);
            }

            longestRoadAvailableResources = availableOre * percentageOreLongestRoad + availableWool * percentageWoolLongestRoad + availableLumber * percentageLumberLongestRoad + availableBrick * percentageBrickLongestRoad + availableGrain * percentageGrainLongestRoad;
            largestArmyAvailableResources = availableOre * percentageOreLargestArmy + availableWool * percentageWoolLargestArmy + availableLumber * percentageLumberLargestArmy + availableBrick * percentageBrickLargestArmy + availableGrain * percentageGrainLargestArmy;

            double currentOre = Heuristics.getIncome(g, p, Const.ORE);
            double currentBrick = Heuristics.getIncome(g, p, Const.BRICK);
            double currentLumber = Heuristics.getIncome(g, p, Const.LUMBER);
            double currentGrain = Heuristics.getIncome(g, p, Const.GRAIN);
            double currentWool = Heuristics.getIncome(g, p, Const.WOOL);

            longestRoadCurrentRescources = currentOre * percentageOreLongestRoad + currentWool * percentageWoolLongestRoad + currentLumber * percentageLumberLongestRoad + currentBrick * percentageBrickLongestRoad + currentGrain * percentageGrainLongestRoad;
            largestArmyCurrentResources = currentOre * percentageOreLargestArmy + currentWool * percentageWoolLargestArmy + currentLumber * percentageLumberLargestArmy + currentBrick * percentageBrickLargestArmy + currentGrain * percentageGrainLargestArmy;

            longestRoadPriority = (longestRoadAvailableResources / longestRoadResources) * (longestRoadAvailableResources * (longestRoadCurrentRescources + 1));
            largestArmyPriority = (largestArmyAvailableResources / largestArmyRescources) * (largestArmyAvailableResources * (largestArmyCurrentResources + 1));

            float oreValueLongestRoad = (float)percentageOreLongestRoad / ((float)currentOre + p.getNumberResource(Const.ORE) + 1);
            float woolValueLongestRoad = (float)percentageWoolLongestRoad / ((float)currentWool + p.getNumberResource(Const.WOOL) + 1);
            float lumberValueLongestRoad = (float)percentageLumberLongestRoad / ((float)currentLumber + p.getNumberResource(Const.LUMBER) + 1);
            float brickValueLongestRoad = (float)percentageBrickLongestRoad / ((float)currentBrick + p.getNumberResource(Const.BRICK) + 1);
            float grainValueLongestRoad = (float)percentageGrainLongestRoad / ((float)currentGrain + p.getNumberResource(Const.GRAIN) + 1);

            float oreValueLargestArmy = (float)percentageOreLargestArmy / ((float)currentOre + p.getNumberResource(Const.GRAIN) + 1);
            float woolValueLargestArmy = (float)percentageWoolLargestArmy / ((float)currentWool + p.getNumberResource(Const.WOOL) + 1);
            float lumberValueLargestArmy = (float)percentageLumberLargestArmy / ((float)currentLumber + p.getNumberResource(Const.LUMBER) + 1);
            float brickValueLargestArmy = (float)percentageBrickLargestArmy / ((float)currentBrick + p.getNumberResource(Const.BRICK) + 1);
            float grainValueLargestArmy = (float)percentageGrainLargestArmy / ((float)currentGrain + p.getNumberResource(Const.GRAIN) + 1);

            if (longestRoadPriority >= largestArmyPriority) {
                return new LongestRoadStrats(oreValueLongestRoad, woolValueLongestRoad, lumberValueLongestRoad, brickValueLongestRoad, grainValueLongestRoad);
            } else {
                return new LargestArmyStrats(oreValueLargestArmy, woolValueLargestArmy, lumberValueLargestArmy, brickValueLargestArmy, grainValueLargestArmy);
            }
        }

        //currently just returns the strategies overall priorities
        //In a future version this might vary depending on the game state, using the strategies overall priorities as weights
        public float[] findActionPriorities(IGameState g, IPlayer p, IStrategy s) {
            //either decide that the ai wants to build a road, or a city or a devcard etc etc.
            //returns a float indicating how useful each of the actions would be in the given state
            return new float[4] { s.buyRoadPriority(g, p), s.buySettlementPriority(g, p), s.buyCityPriority(g, p), s.buyDevCardPriority(g, p) };
        }
    }
}
