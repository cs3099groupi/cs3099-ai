﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class BuyCityStrats {
        //heuristical representation of the value of a city. 
        public static float valueOfAction(IGameState currentState, BuyCity city, IStrategy theChosenStrat) {
            if (currentState.getCurrentPlayer().points() == 9) return 2000F;//if this wins the game then hard code a very high value
            //adds the value of the income it provides.
            float actionValue = (float)Heuristics.getCityTotalIncome(city.pos, currentState);
            //also adds the cities income weighted by the strategy the ai is following (i.e. largest army rates ORE high, longest road rates BRICK high etc.)
            actionValue += (float)Heuristics.getCityIncome(city.pos, currentState, Const.BRICK) * theChosenStrat.getBrickWeighting();
            actionValue += (float)Heuristics.getCityIncome(city.pos, currentState, Const.LUMBER) * theChosenStrat.getLumberWeighting();
            actionValue += (float)Heuristics.getCityIncome(city.pos, currentState, Const.ORE) * theChosenStrat.getOreWeighting();
            actionValue += (float)Heuristics.getCityIncome(city.pos, currentState, Const.GRAIN) * theChosenStrat.getGrainWeighting();
            actionValue += (float)Heuristics.getCityIncome(city.pos, currentState, Const.WOOL) * theChosenStrat.getWoolWeighting();
            return actionValue;
        }
    }
}
