﻿using System;
using System.Collections.Generic;

namespace AI {
    class PlayerTradeStrats {
        internal static Random r = new Random();

        private static float rejectValue = 0.5f;

        internal static float valueOfAccept(CatanAI ai, IGameState currentState, ConfirmPlayerTrade confirm, IStrategy theChosenStrat) {
            float shouldTrade = 0;
            float instigatorThreat = calculateInstigatorThreat(ai, currentState, confirm.getInstigator(), new Tuple<int[], int[]>((confirm.getWant()), confirm.getOffer()));
            for (int i = 0; i < confirm.getOffer().Length; i++) {
                for (int j = 0; j < confirm.getOffer()[i]; j++) {
                    shouldTrade += theChosenStrat.getResourceWeighting(i);
                }
            }
            for (int i = 0; i < confirm.getWant().Length; i++) {
                for (int j = 0; j < confirm.getWant()[i]; j++) {
                    shouldTrade -= theChosenStrat.getResourceWeighting(i) * 1.3F * instigatorThreat;
                }
            }
            return shouldTrade;
        }


        internal static float valueOfReject(IGameState currentState, RejectPlayerTrade reject, IStrategy theChosenStrat) {
            return rejectValue;
        }

        //instigator - person requesting the trade
        //target - person targetted for the trade (always the AI)
        private static float calculateInstigatorThreat(CatanAI ai, IGameState currentState, IPlayer instigator, Tuple<int[], int[]> playerTrade) {
            float instigatorThreat = 1;
            IPlayer p = Heuristics.getMe(ai, currentState);
            int instigatorVP = instigator.points();
            int aiVP = p.points();

            //Check if the instigator has more VP than AI **REJECT?
            if (instigatorVP > aiVP) {
                instigatorThreat += 0.2F;
            }

            return instigatorThreat;
        }
    }
}
