﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class ChooseResourceStrats {
        //returns a value defining how good that resource choice is.
        //runs different heuristics depending on why the resource is being chosen.
        internal static float valueOfAction(CatanAI ai, IGameState currentState, ChooseResource choose, IStrategy theChosenStrat) {
            switch (currentState.getState()) {
                case Const.FIRSTYEAROFPLENTY:
                case Const.FIRSTYEAROFPLENTYBEFOREROLLINGTHEDICE:
                    return firstYearOfPlenty(currentState, choose, theChosenStrat);
                case Const.SECONDYEAROFPLENTY:
                case Const.SECONDYEAROFPLENTYBEFOREROLLINGTHEDICE:
                    return secondYearOfPlenty(currentState, choose, theChosenStrat);
                case Const.CHOOSINGMONOPOLY:
                case Const.CHOOSINGMONOPOLYBEFOREROLLINGTHEDICE:
                    return chooseMonopoly(ai, currentState, choose, theChosenStrat);
                default: return 0;
            }
        }

        //heuristic value of a monopoly resource choice.
        //combines the strategies weightings of different resources with how many resources would be gained.
        private static float chooseMonopoly(CatanAI ai, IGameState currentState, ChooseResource choose, IStrategy theChosenStrat) {
            int numOwned = Heuristics.getMe(ai, currentState).getNumberResource(choose.res);
            int bankOwned = currentState.getBank().getResourceCount(choose.res);
            int val = (Const.Q_BANK_RESOURCES - numOwned) - bankOwned;
            return val * theChosenStrat.getResourceWeighting(choose.res);
        }

        //encapsulates the value of a first year of plenty choice.
        //simply returns the strategies weighting of the resource.
        private static float secondYearOfPlenty(IGameState currentState, ChooseResource choose, IStrategy theChosenStrat) {
            return theChosenStrat.getResourceWeighting(choose.res);
        }

        //simply returns the strategies weighting of the resource.
        private static float firstYearOfPlenty(IGameState currentState, ChooseResource choose, IStrategy theChosenStrat) {
            return theChosenStrat.getResourceWeighting(choose.res);
        }
    }
}
