﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class EndTurnStrats {
        //returns a value that defines how good a choice it is to end the turn.
        //this returns a flat value unless the best alternative choice found so far is a buyroad.
        //if the best alternative choice found is a buy road then it MAY return a higher value if the road is bad, and if the player doesn't have too many resources.
        public static float valueOfAction(CatanAI ai, IGameState startState, Choice bestChoice, float maxPriority, IStrategy strat) {
            if (bestChoice.getActions().Count > 0 && !(bestChoice.getActions()[0] is EndTurn)) {
                if (bestChoice.getActions()[0] is BuyRoad && Heuristics.getMe(ai, startState).totalResources() < 15) {
                    return buyRoad(ai, startState, (BuyRoad)bestChoice.getActions()[0], strat, maxPriority);
                }
            }
            return 0.2F;
        }

        private static float buyRoad(CatanAI ai, IGameState startState, BuyRoad road, IStrategy strat, float priority) {
            IPlayer p = Heuristics.getMe(ai, startState);
            if (BuyRoadStrats.addsToLongestRoad(ai, startState, Heuristics.getMe(ai, startState), road, strat) < 1.5F) {
                int count = 0;
                foreach (IRoad pRoad in p.getRoads()) {
                    foreach (ICoord c in startState.getFreeSettlementSpots()) {
                        if (c.isEqual(pRoad.getPos()) || c.isEqual(pRoad.getPos2())) {
                            count++;
                        }
                    }
                }
                if (count >= 2) {
                    return priority * 2F;
                }
            }
            return 0.2F;
        }
    }
}
