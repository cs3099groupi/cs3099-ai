﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class BuySettlementStrats {
        //returns the value of a buy settlement action.
        //this is done by getting the value of the settlement and applying a weight to it depending on the value of OTHER settlements it could choose.
        public static float valueOfAction(CatanAI ai, IGameState currentState, BuySettlement settlement, IStrategy theChosenStrat) {
            if (currentState.getCurrentPlayer().points() == 9) return 2000F;
            return evaluateSettlement(ai, currentState, settlement, theChosenStrat) * betterOtherSettlements(ai, currentState, settlement, theChosenStrat);
        }

        //returns a value dictating how good a settlement is. This is a combination of the settlement's income, with a weight applied to it by the strategy
        public static float evaluateSettlement(CatanAI ai, IGameState currentState, BuySettlement settlement, IStrategy theChosenStrat) {
            float actionValue = (float)Heuristics.getSettlementTotalIncome(settlement.pos, currentState);
            actionValue += (float)Heuristics.getSettlementIncome(settlement.pos, currentState, Const.BRICK) * theChosenStrat.getBrickWeighting();
            actionValue += (float)Heuristics.getSettlementIncome(settlement.pos, currentState, Const.LUMBER) * theChosenStrat.getLumberWeighting();
            actionValue += (float)Heuristics.getSettlementIncome(settlement.pos, currentState, Const.ORE) * theChosenStrat.getOreWeighting();
            actionValue += (float)Heuristics.getSettlementIncome(settlement.pos, currentState, Const.GRAIN) * theChosenStrat.getGrainWeighting();
            actionValue += (float)Heuristics.getSettlementIncome(settlement.pos, currentState, Const.WOOL) * theChosenStrat.getWoolWeighting();
            actionValue *= portValue(ai, currentState, settlement, theChosenStrat);
            return actionValue;
        }

        //returns a value that is used as a weight to adjust a settlements value by.
        //increases a settlements wieght if it is a port.
        private static float portValue(CatanAI ai, IGameState currentState, BuySettlement settlement, IStrategy theChosenStrat) {
            IPort port = currentState.getBoard().getPort(settlement.pos);
            if (port != null) {
                if (port.getResourceType() == Const.UNIVERSAL) {
                    return 1.2F;
                } else {
                    return 1 + ((float)Heuristics.getIncome(currentState, Heuristics.getMe(ai, currentState), port.getResourceType()) / 50F);
                }
            }
            return 1;
        }
        //looks at the other free settlements that the player can't currently build at but can in the future (because it is 1 road away)
        //returns 1 if there is no better settlement, and returns 0 if there is a better settlement nearby
        private static float betterOtherSettlements(CatanAI ai, IGameState g, BuySettlement s, IStrategy strat) {
            if (Heuristics.getMe(ai, g).points() != 9) {
                foreach (ICoord c in Heuristics.getSurroundingCoordinates(s.pos)) {
                    foreach (ICoord stl in g.getFreeSettlementSpots()) {
                        if (stl.isEqual(c)) {
                            if (evaluateSettlement(ai, g, new BuySettlement(c), strat) * 0.8F > evaluateSettlement(ai, g, s, strat)) {
                                bool better = true;
                                foreach (ICoord c2 in Heuristics.getSurroundingCoordinates(c)) {
                                    if (c2 != s.pos) {
                                        if ((evaluateSettlement(ai, g, s, strat) * 0.8F) + (evaluateSettlement(ai, g, new BuySettlement(c2), strat) * 0.8F) > evaluateSettlement(ai, g, new BuySettlement(c), strat)) {
                                            better = false;
                                        }
                                    }
                                }
                                if (better) {
                                    return 0F;
                                }
                            }
                        }
                    }
                }
            }
            return 1F;
        }
    }
}
