﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    abstract class Decider {

        IPlayer me;
        ChoiceGen generator = new ChoiceGen();

        public void run(CatanAI ai, NetworkManager nm) {
            IGameState currentState = ai.theGameState;
            lock (ai.theGameState) {
                currentState = ai.theGameState.copy();
            }
            me = currentState.getPlayer(ai.aiPlayerID);
            //it must be ensured that the variables currentState and me are set appropriately before this method is run
            List<Choice> choices = generator.generateChoices(ai, currentState, me, nm);//get options available to player
            Choice thingsToDo = chooseChoice(ai, choices, currentState, me, nm, false);//choose the option using the ai
            if (thingsToDo.getActions().Count != 0) {
                Console.WriteLine("Sending action to server.");
                thingsToDo.print(ai);
            }
            foreach (IAction act in thingsToDo.getActions()) {
                //given a game state it chooses what to do and gives this list to the network manager to send to the server
                //the rest of the program is abstracted, so the ai just sends the list to the main class and forgets about it.
                ai.handleOutgoingEvent(act);
            }
            //end of thread
        }

        public abstract Choice chooseChoice(CatanAI ai, List<Choice> choices, IGameState currentState2, IPlayer p2, NetworkManager nm, bool recursive);//abstract decision making process is implemented by sub classes
    }
}
