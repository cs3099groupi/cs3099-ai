﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class RandomAI : Decider {
        Random r = new Random();
        public override Choice chooseChoice(CatanAI ai, List<Choice> choices, IGameState currentState2, IPlayer p2, NetworkManager nm, bool recursive) {
            return choices.ElementAt(r.Next(choices.Count));//chooses a random choice.
        }
    }
}
