﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class LongestRoadStrats : IStrategy {

        public float ore = 0;
        public float wool = 0;
        public float lumber = 0;
        public float brick = 0;
        public float grain = 0;

        public LongestRoadStrats(float ore, float wool, float lumber, float brick, float grain) {
            this.ore = ore;
            this.wool = wool;
            this.lumber = lumber;
            this.brick = brick;
            this.grain = grain;
        }

        public float buyCityPriority(IGameState g, IPlayer p) {
            //0.0 never buy a city
            //1.0 definitely buy a city
            //10.0 definitely buy any city (wins the game?)
            return 0.7F;
        }

        public float buyDevCardPriority(IGameState g, IPlayer p) {
            return 0.2F;
        }

        public float buyRoadPriority(IGameState g, IPlayer p) {
            if (g.getState() == Const.INITIALROAD) return 1.0F;
            if (g.getLongestRoadPlayer() == p) return 0.1F;
            return 0.5F;
        }

        public float buySettlementPriority(IGameState g, IPlayer p) {
            if (g.getState() == Const.INITIALSETTLEMENT) return 1.0F;
            return 0.9F;
        }

        public bool isLongestRoad() {
            return true;
        }

        public float getOreWeighting() {
            return ore;
        }

        public float getWoolWeighting() {
            return wool;
        }

        public float getLumberWeighting() {
            return lumber;
        }

        public float getBrickWeighting() {
            return brick;
        }

        public float getGrainWeighting() {
            return grain;
        }

        public float getResourceWeighting(int resource) {
            switch (resource) {
                case Const.ORE: return ore;
                case Const.WOOL: return wool;
                case Const.LUMBER: return lumber;
                case Const.BRICK: return brick;
                case Const.GRAIN: return grain;
                default: return 0;
            }
        }
    }
}
