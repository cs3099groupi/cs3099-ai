﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class BuildInitialSettlementStrats {
        //a programming abstraction of getting the value of initial Settlement positions.
        //In this version it simply calls the normal settlement heuristics value
        internal static float valueOfAction(CatanAI ai, IGameState currentState, BuildInitialSettlement initialSettlement, IStrategy theChosenStrat) {
            return BuySettlementStrats.valueOfAction(ai, currentState, new BuySettlement(initialSettlement.pos), theChosenStrat);
        }
    }
}