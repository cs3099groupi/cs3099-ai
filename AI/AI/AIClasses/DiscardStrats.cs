﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class DiscardStrats {

        //returns the value of a discard choice.
        //returns a combination of a heuristic prioritising resources that the player has a lot of income in and the value of the best action that is possible after the discard.
        internal static float valueOfAction(CatanAI ai, IGameState currentState, Discard discard, IStrategy theChosenStrat, IGameState endState, NetworkManager nm) {
            float valueOfDiscard = 0;
            //creates a value that is higher if the player has a lot of income in the resources it is choosing to discard.
            for (int i = 0; i < discard.resourcesToDiscard.Length; i++) {
                for (int j = 0; j < discard.resourcesToDiscard[i]; j++) {
                    valueOfDiscard += (float)Heuristics.getIncome(currentState, Heuristics.getMe(ai, currentState), i) / theChosenStrat.getResourceWeighting(i);
                }
            }
            int endStateState = endState.getState();
            endState.setState(Const.WAITPLAY);
            AIClass AI = new AIClass();
            ChoiceGen generator = new ChoiceGen();
            List<Choice> choices = generator.generateChoices(ai, endState, Heuristics.getMe(ai, endState), nm);
            Choice bestChoice = AI.chooseChoice(ai, choices, endState, Heuristics.getMe(ai, endState), nm, true);
            float choiceValue = 0;
            //creates a value that represents the best choice of actions possible after the discard.
            if (bestChoice.getActions().Count > 0 && !(bestChoice.getActions()[0] is EndTurn)) {
                choiceValue = AI.evaluateChoice(ai, endState, bestChoice, AI.findActionPriorities(endState, Heuristics.getMe(ai, endState), AI.chooseStrat(endState, Heuristics.getMe(ai, endState))), AI.chooseStrat(endState, Heuristics.getMe(ai, endState)), bestChoice, 0, nm, true);
            } else {
                choiceValue = 0.1F;
            }
            endState.setState(endStateState);
            return valueOfDiscard * choiceValue;
        }
    }
}
