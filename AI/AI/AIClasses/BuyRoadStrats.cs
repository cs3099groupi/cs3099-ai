﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AI {
    class BuyRoadStrats {

        //encapsulates the value of a road building road.
        //in this version it converts the action to a buyRoad to standardise it with the normal buildRoad heuristic method and returns the normal build road heuristic value.
        public static float valueOfActionRoadBuildingVersion(CatanAI ai, IGameState currentState, BuildRoadBuildingRoad road, IStrategy theChosenStrat) {
            BuyRoad buyroad = new BuyRoad(road.pos, road.pos2);
            return valueOfAction(ai, currentState, buyroad, theChosenStrat);
        }

        //returns the value of a build road action. 
        public static float valueOfAction(CatanAI ai, IGameState currentState, BuyRoad road, IStrategy theChosenStrat) {
            float valOfAction = 0.18F;
            IGameState newState = currentState.copy();
            IPlayer me = currentState.getCurrentPlayer();

            ICoord freeSettlement = getFreeSettlement(currentState, me, road);

            if (freeSettlement != null) {
                valOfAction += getFreeSettlementValue(ai, currentState, me, theChosenStrat, freeSettlement);
                valOfAction *= addsToLongestRoad(ai, currentState, me, road, theChosenStrat);
            } else {
                road.run(ai, newState);
                foreach (BuyRoad freeRoad in getFreeRoadsLeadingOff(currentState, road, me)) {
                    float val = valueOfAction(ai, newState, freeRoad, theChosenStrat) * 0.8F;
                    if (val > valOfAction) {
                        valOfAction = val;
                    }
                }
            }


            return valOfAction;
        }

        //returns the value of the settlements added to possibilities, but divided a bit to prevent over prioritising
        public static float getFreeSettlementValue(CatanAI ai, IGameState stateBeforeAction, IPlayer p, IStrategy theChosenStrat, ICoord freeSettlement) {
            BuySettlement buyStlAct = new BuySettlement(freeSettlement);
            float stratval = theChosenStrat.buySettlementPriority(stateBeforeAction, p);
            return (BuySettlementStrats.valueOfAction(ai, stateBeforeAction, buyStlAct, theChosenStrat) * stratval) * 0.6F;
        }

        //returns the position that the player can build a settlement at BECAUSE of the buy road action. can return null.
        public static ICoord getFreeSettlement(IGameState currentState, IPlayer p, BuyRoad rd) {
            foreach (ICoord stl in currentState.getFreeSettlementSpots()) {
                if (rd.pos.isEqual(stl) || rd.pos2.isEqual(stl)) {
                    return stl;
                }
            }
            return null;
        }
        //gets the positions that the player can build roads at BECAUSE of the buy road action. can return an empty list.
        public static List<BuyRoad> getFreeRoadsLeadingOff(IGameState currentstate, BuyRoad road, IPlayer p) {
            List<BuyRoad> freeRoads = new List<BuyRoad>();
            ICoord newCoord = getNewCoord(currentstate, p, road);
            if (newCoord != null) {
                ICoord oldCoord;
                if (newCoord.isEqual(road.pos)) {
                    oldCoord = road.pos;
                } else {
                    oldCoord = road.pos2;
                }
                List<ICoord> coordsAround = Heuristics.getSurroundingCoordinates(newCoord);
                foreach (ICoord coord in coordsAround) {
                    if (!coord.isEqual(oldCoord)) {
                        foreach (Tuple<ICoord, ICoord> rds in currentstate.getFreeRoadSpots()) {
                            if (rds.Item1.isEqual(newCoord) && rds.Item2.isEqual(coord)) {
                                freeRoads.Add(new BuyRoad(newCoord, coord));
                            } else if (rds.Item1.isEqual(coord) && rds.Item2.isEqual(newCoord)) {
                                freeRoads.Add(new BuyRoad(coord, newCoord));
                            }
                        }
                    }
                }
            }
            return freeRoads;
        }
        //returns the new coordinate that the player has reached with the buy road. can return null if the player is connecting 2 of his/her existing roads
        public static ICoord getNewCoord(IGameState stateBeforeAction, IPlayer p, BuyRoad rd) {
            ICoord pos1 = rd.pos;
            ICoord pos2 = rd.pos2;

            bool pos1Touching = false;
            bool pos2Touching = false;

            foreach (ISettlement settlement in p.getSettlementsAndCities()) {
                if (pos1.isEqual(settlement.getPos())) {
                    pos1Touching = true;
                } else if (pos2.isEqual(settlement.getPos())) {
                    pos2Touching = true;
                }
            }

            foreach (IRoad playersroad in p.getRoads()) {
                if (pos1.isEqual(playersroad.getPos()) || pos1.isEqual(playersroad.getPos2())) {
                    pos1Touching = true;
                } else if (pos2.isEqual(playersroad.getPos()) || pos2.isEqual(playersroad.getPos2())) {
                    pos2Touching = true;
                }
            }

            if (pos1Touching && pos2Touching) return null;
            if (pos1Touching) return pos2;
            if (pos2Touching) return pos1;
            return null;
        }

        //returns a value if the buy road adds to the player's longest road.
        public static float addsToLongestRoad(CatanAI ai, IGameState stateBeforeAction, IPlayer p, BuyRoad rd, IStrategy theChosenStrat) {
            int longestOther = 0;
            foreach (IPlayer player in stateBeforeAction.getPlayers()) {
                if (player != p && player.getLongestRoad() > longestOther) {
                    longestOther = player.getLongestRoad();
                }
            }
            int longestRoadCurrent = p.getLongestRoad();
            IGameState newState = stateBeforeAction.copy();
            rd.run(ai, newState);
            int newLongest = newState.getLongestRoad();
            if (newLongest > longestRoadCurrent && !(newLongest - longestOther > 2)) {
                if (theChosenStrat is LongestRoadStrats) {
                    if (newLongest - longestRoadCurrent >= 2) {
                        return 1001F;
                    } else {
                        return 1.4F;
                    }
                } else {
                    return (float)(newLongest - longestRoadCurrent) * 1.1F;
                }
            } else return 1F;
        }
    }
}
