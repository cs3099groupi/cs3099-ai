// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: lobby/lobby.proto
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Intergroup.Lobby {

  /// <summary>Holder for reflection information generated from lobby/lobby.proto</summary>
  public static partial class LobbyReflection {

    #region Descriptor
    /// <summary>File descriptor for lobby/lobby.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static LobbyReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "ChFsb2JieS9sb2JieS5wcm90bxIQaW50ZXJncm91cC5sb2JieRoRYm9hcmQv",
            "Ym9hcmQucHJvdG8iKAoESm9pbhIQCgh1c2VybmFtZRgBIAEoCRIOCgZnYW1l",
            "SWQYAiABKAUiHQoJVXNlcm5hbWVzEhAKCHVzZXJuYW1lGAEgAygJIqgDCglH",
            "YW1lU2V0dXASJAoFaGV4ZXMYASADKAsyFS5pbnRlcmdyb3VwLmJvYXJkLkhl",
            "eBIrCghoYXJib3VycxgCIAMoCzIZLmludGVyZ3JvdXAuYm9hcmQuSGFyYm91",
            "chJBCg5wbGF5ZXJTZXR0aW5ncxgDIAMoCzIpLmludGVyZ3JvdXAubG9iYnku",
            "R2FtZVNldHVwLlBsYXllclNldHRpbmcSKwoJb3duUGxheWVyGAQgASgLMhgu",
            "aW50ZXJncm91cC5ib2FyZC5QbGF5ZXIa1wEKDVBsYXllclNldHRpbmcSEAoI",
            "dXNlcm5hbWUYASABKAkSKAoGcGxheWVyGAIgASgLMhguaW50ZXJncm91cC5i",
            "b2FyZC5QbGF5ZXISQAoGY29sb3VyGAMgASgOMjAuaW50ZXJncm91cC5sb2Ji",
            "eS5HYW1lU2V0dXAuUGxheWVyU2V0dGluZy5Db2xvdXIiSAoGQ29sb3VyEgcK",
            "A1JFRBAAEggKBEJMVUUQARIKCgZPUkFOR0UQAhIJCgVXSElURRADEgkKBUdS",
            "RUVOEAQSCQoFQlJPV04QBSLXAQoHR2FtZVdvbhIoCgZ3aW5uZXIYASABKAsy",
            "GC5pbnRlcmdyb3VwLmJvYXJkLlBsYXllchI5CgtoaWRkZW5DYXJkcxgCIAMo",
            "CzIkLmludGVyZ3JvdXAubG9iYnkuR2FtZVdvbi5DYXJkUmV2ZWFsGmcKCkNh",
            "cmRSZXZlYWwSKAoGcGxheWVyGAEgASgLMhguaW50ZXJncm91cC5ib2FyZC5Q",
            "bGF5ZXISLwoHVlBDYXJkcxgCIAMoDjIeLmludGVyZ3JvdXAuYm9hcmQuVmlj",
            "dG9yeVBvaW50YgZwcm90bzM="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { global::Intergroup.Board.BoardReflection.Descriptor, },
          new pbr::GeneratedClrTypeInfo(null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::Intergroup.Lobby.Join), global::Intergroup.Lobby.Join.Parser, new[]{ "Username", "GameId" }, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::Intergroup.Lobby.Usernames), global::Intergroup.Lobby.Usernames.Parser, new[]{ "Username" }, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::Intergroup.Lobby.GameSetup), global::Intergroup.Lobby.GameSetup.Parser, new[]{ "Hexes", "Harbours", "PlayerSettings", "OwnPlayer" }, null, null, new pbr::GeneratedClrTypeInfo[] { new pbr::GeneratedClrTypeInfo(typeof(global::Intergroup.Lobby.GameSetup.Types.PlayerSetting), global::Intergroup.Lobby.GameSetup.Types.PlayerSetting.Parser, new[]{ "Username", "Player", "Colour" }, null, new[]{ typeof(global::Intergroup.Lobby.GameSetup.Types.PlayerSetting.Types.Colour) }, null)}),
            new pbr::GeneratedClrTypeInfo(typeof(global::Intergroup.Lobby.GameWon), global::Intergroup.Lobby.GameWon.Parser, new[]{ "Winner", "HiddenCards" }, null, null, new pbr::GeneratedClrTypeInfo[] { new pbr::GeneratedClrTypeInfo(typeof(global::Intergroup.Lobby.GameWon.Types.CardReveal), global::Intergroup.Lobby.GameWon.Types.CardReveal.Parser, new[]{ "Player", "VPCards" }, null, null, null)})
          }));
    }
    #endregion

  }
  #region Messages
  /// <summary>
  ///  To join a game, a client must open a TCP connection using
  ///  the IP of the Game Server. Upon connecting, the client
  ///  should send the server a Join message.
  /// </summary>
  public sealed partial class Join : pb::IMessage<Join> {
    private static readonly pb::MessageParser<Join> _parser = new pb::MessageParser<Join>(() => new Join());
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<Join> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Intergroup.Lobby.LobbyReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Join() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Join(Join other) : this() {
      username_ = other.username_;
      gameId_ = other.gameId_;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Join Clone() {
      return new Join(this);
    }

    /// <summary>Field number for the "username" field.</summary>
    public const int UsernameFieldNumber = 1;
    private string username_ = "";
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public string Username {
      get { return username_; }
      set {
        username_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }

    /// <summary>Field number for the "gameId" field.</summary>
    public const int GameIdFieldNumber = 2;
    private int gameId_;
    /// <summary>
    ///  Only clients/AIs which wish to connect to catan
    ///  servers that host multiple games will need to specify
    ///  the gameId.
    /// </summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int GameId {
      get { return gameId_; }
      set {
        gameId_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as Join);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(Join other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (Username != other.Username) return false;
      if (GameId != other.GameId) return false;
      return true;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (Username.Length != 0) hash ^= Username.GetHashCode();
      if (GameId != 0) hash ^= GameId.GetHashCode();
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (Username.Length != 0) {
        output.WriteRawTag(10);
        output.WriteString(Username);
      }
      if (GameId != 0) {
        output.WriteRawTag(16);
        output.WriteInt32(GameId);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (Username.Length != 0) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Username);
      }
      if (GameId != 0) {
        size += 1 + pb::CodedOutputStream.ComputeInt32Size(GameId);
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(Join other) {
      if (other == null) {
        return;
      }
      if (other.Username.Length != 0) {
        Username = other.Username;
      }
      if (other.GameId != 0) {
        GameId = other.GameId;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            input.SkipLastField();
            break;
          case 10: {
            Username = input.ReadString();
            break;
          }
          case 16: {
            GameId = input.ReadInt32();
            break;
          }
        }
      }
    }

  }

  /// <summary>
  /// The list of players that are in the lobby
  /// </summary>
  public sealed partial class Usernames : pb::IMessage<Usernames> {
    private static readonly pb::MessageParser<Usernames> _parser = new pb::MessageParser<Usernames>(() => new Usernames());
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<Usernames> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Intergroup.Lobby.LobbyReflection.Descriptor.MessageTypes[1]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Usernames() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Usernames(Usernames other) : this() {
      username_ = other.username_.Clone();
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Usernames Clone() {
      return new Usernames(this);
    }

    /// <summary>Field number for the "username" field.</summary>
    public const int UsernameFieldNumber = 1;
    private static readonly pb::FieldCodec<string> _repeated_username_codec
        = pb::FieldCodec.ForString(10);
    private readonly pbc::RepeatedField<string> username_ = new pbc::RepeatedField<string>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public pbc::RepeatedField<string> Username {
      get { return username_; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as Usernames);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(Usernames other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if(!username_.Equals(other.username_)) return false;
      return true;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      hash ^= username_.GetHashCode();
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      username_.WriteTo(output, _repeated_username_codec);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      size += username_.CalculateSize(_repeated_username_codec);
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(Usernames other) {
      if (other == null) {
        return;
      }
      username_.Add(other.username_);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            input.SkipLastField();
            break;
          case 10: {
            username_.AddEntriesFrom(input, _repeated_username_codec);
            break;
          }
        }
      }
    }

  }

  /// <summary>
  ///  When the game server is ready to begin the game, it sends a
  ///  GameSetup event to all players
  /// </summary>
  public sealed partial class GameSetup : pb::IMessage<GameSetup> {
    private static readonly pb::MessageParser<GameSetup> _parser = new pb::MessageParser<GameSetup>(() => new GameSetup());
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<GameSetup> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Intergroup.Lobby.LobbyReflection.Descriptor.MessageTypes[2]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public GameSetup() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public GameSetup(GameSetup other) : this() {
      hexes_ = other.hexes_.Clone();
      harbours_ = other.harbours_.Clone();
      playerSettings_ = other.playerSettings_.Clone();
      OwnPlayer = other.ownPlayer_ != null ? other.OwnPlayer.Clone() : null;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public GameSetup Clone() {
      return new GameSetup(this);
    }

    /// <summary>Field number for the "hexes" field.</summary>
    public const int HexesFieldNumber = 1;
    private static readonly pb::FieldCodec<global::Intergroup.Board.Hex> _repeated_hexes_codec
        = pb::FieldCodec.ForMessage(10, global::Intergroup.Board.Hex.Parser);
    private readonly pbc::RepeatedField<global::Intergroup.Board.Hex> hexes_ = new pbc::RepeatedField<global::Intergroup.Board.Hex>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public pbc::RepeatedField<global::Intergroup.Board.Hex> Hexes {
      get { return hexes_; }
    }

    /// <summary>Field number for the "harbours" field.</summary>
    public const int HarboursFieldNumber = 2;
    private static readonly pb::FieldCodec<global::Intergroup.Board.Harbour> _repeated_harbours_codec
        = pb::FieldCodec.ForMessage(18, global::Intergroup.Board.Harbour.Parser);
    private readonly pbc::RepeatedField<global::Intergroup.Board.Harbour> harbours_ = new pbc::RepeatedField<global::Intergroup.Board.Harbour>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public pbc::RepeatedField<global::Intergroup.Board.Harbour> Harbours {
      get { return harbours_; }
    }

    /// <summary>Field number for the "playerSettings" field.</summary>
    public const int PlayerSettingsFieldNumber = 3;
    private static readonly pb::FieldCodec<global::Intergroup.Lobby.GameSetup.Types.PlayerSetting> _repeated_playerSettings_codec
        = pb::FieldCodec.ForMessage(26, global::Intergroup.Lobby.GameSetup.Types.PlayerSetting.Parser);
    private readonly pbc::RepeatedField<global::Intergroup.Lobby.GameSetup.Types.PlayerSetting> playerSettings_ = new pbc::RepeatedField<global::Intergroup.Lobby.GameSetup.Types.PlayerSetting>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public pbc::RepeatedField<global::Intergroup.Lobby.GameSetup.Types.PlayerSetting> PlayerSettings {
      get { return playerSettings_; }
    }

    /// <summary>Field number for the "ownPlayer" field.</summary>
    public const int OwnPlayerFieldNumber = 4;
    private global::Intergroup.Board.Player ownPlayer_;
    /// <summary>
    ///  to let a client identify themselves in the playerSettings
    /// </summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Intergroup.Board.Player OwnPlayer {
      get { return ownPlayer_; }
      set {
        ownPlayer_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as GameSetup);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(GameSetup other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if(!hexes_.Equals(other.hexes_)) return false;
      if(!harbours_.Equals(other.harbours_)) return false;
      if(!playerSettings_.Equals(other.playerSettings_)) return false;
      if (!object.Equals(OwnPlayer, other.OwnPlayer)) return false;
      return true;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      hash ^= hexes_.GetHashCode();
      hash ^= harbours_.GetHashCode();
      hash ^= playerSettings_.GetHashCode();
      if (ownPlayer_ != null) hash ^= OwnPlayer.GetHashCode();
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      hexes_.WriteTo(output, _repeated_hexes_codec);
      harbours_.WriteTo(output, _repeated_harbours_codec);
      playerSettings_.WriteTo(output, _repeated_playerSettings_codec);
      if (ownPlayer_ != null) {
        output.WriteRawTag(34);
        output.WriteMessage(OwnPlayer);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      size += hexes_.CalculateSize(_repeated_hexes_codec);
      size += harbours_.CalculateSize(_repeated_harbours_codec);
      size += playerSettings_.CalculateSize(_repeated_playerSettings_codec);
      if (ownPlayer_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(OwnPlayer);
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(GameSetup other) {
      if (other == null) {
        return;
      }
      hexes_.Add(other.hexes_);
      harbours_.Add(other.harbours_);
      playerSettings_.Add(other.playerSettings_);
      if (other.ownPlayer_ != null) {
        if (ownPlayer_ == null) {
          ownPlayer_ = new global::Intergroup.Board.Player();
        }
        OwnPlayer.MergeFrom(other.OwnPlayer);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            input.SkipLastField();
            break;
          case 10: {
            hexes_.AddEntriesFrom(input, _repeated_hexes_codec);
            break;
          }
          case 18: {
            harbours_.AddEntriesFrom(input, _repeated_harbours_codec);
            break;
          }
          case 26: {
            playerSettings_.AddEntriesFrom(input, _repeated_playerSettings_codec);
            break;
          }
          case 34: {
            if (ownPlayer_ == null) {
              ownPlayer_ = new global::Intergroup.Board.Player();
            }
            input.ReadMessage(ownPlayer_);
            break;
          }
        }
      }
    }

    #region Nested types
    /// <summary>Container for nested types declared in the GameSetup message type.</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static partial class Types {
      public sealed partial class PlayerSetting : pb::IMessage<PlayerSetting> {
        private static readonly pb::MessageParser<PlayerSetting> _parser = new pb::MessageParser<PlayerSetting>(() => new PlayerSetting());
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public static pb::MessageParser<PlayerSetting> Parser { get { return _parser; } }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public static pbr::MessageDescriptor Descriptor {
          get { return global::Intergroup.Lobby.GameSetup.Descriptor.NestedTypes[0]; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        pbr::MessageDescriptor pb::IMessage.Descriptor {
          get { return Descriptor; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public PlayerSetting() {
          OnConstruction();
        }

        partial void OnConstruction();

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public PlayerSetting(PlayerSetting other) : this() {
          username_ = other.username_;
          Player = other.player_ != null ? other.Player.Clone() : null;
          colour_ = other.colour_;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public PlayerSetting Clone() {
          return new PlayerSetting(this);
        }

        /// <summary>Field number for the "username" field.</summary>
        public const int UsernameFieldNumber = 1;
        private string username_ = "";
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public string Username {
          get { return username_; }
          set {
            username_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
          }
        }

        /// <summary>Field number for the "player" field.</summary>
        public const int PlayerFieldNumber = 2;
        private global::Intergroup.Board.Player player_;
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public global::Intergroup.Board.Player Player {
          get { return player_; }
          set {
            player_ = value;
          }
        }

        /// <summary>Field number for the "colour" field.</summary>
        public const int ColourFieldNumber = 3;
        private global::Intergroup.Lobby.GameSetup.Types.PlayerSetting.Types.Colour colour_ = 0;
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public global::Intergroup.Lobby.GameSetup.Types.PlayerSetting.Types.Colour Colour {
          get { return colour_; }
          set {
            colour_ = value;
          }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override bool Equals(object other) {
          return Equals(other as PlayerSetting);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public bool Equals(PlayerSetting other) {
          if (ReferenceEquals(other, null)) {
            return false;
          }
          if (ReferenceEquals(other, this)) {
            return true;
          }
          if (Username != other.Username) return false;
          if (!object.Equals(Player, other.Player)) return false;
          if (Colour != other.Colour) return false;
          return true;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override int GetHashCode() {
          int hash = 1;
          if (Username.Length != 0) hash ^= Username.GetHashCode();
          if (player_ != null) hash ^= Player.GetHashCode();
          if (Colour != 0) hash ^= Colour.GetHashCode();
          return hash;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override string ToString() {
          return pb::JsonFormatter.ToDiagnosticString(this);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void WriteTo(pb::CodedOutputStream output) {
          if (Username.Length != 0) {
            output.WriteRawTag(10);
            output.WriteString(Username);
          }
          if (player_ != null) {
            output.WriteRawTag(18);
            output.WriteMessage(Player);
          }
          if (Colour != 0) {
            output.WriteRawTag(24);
            output.WriteEnum((int) Colour);
          }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public int CalculateSize() {
          int size = 0;
          if (Username.Length != 0) {
            size += 1 + pb::CodedOutputStream.ComputeStringSize(Username);
          }
          if (player_ != null) {
            size += 1 + pb::CodedOutputStream.ComputeMessageSize(Player);
          }
          if (Colour != 0) {
            size += 1 + pb::CodedOutputStream.ComputeEnumSize((int) Colour);
          }
          return size;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void MergeFrom(PlayerSetting other) {
          if (other == null) {
            return;
          }
          if (other.Username.Length != 0) {
            Username = other.Username;
          }
          if (other.player_ != null) {
            if (player_ == null) {
              player_ = new global::Intergroup.Board.Player();
            }
            Player.MergeFrom(other.Player);
          }
          if (other.Colour != 0) {
            Colour = other.Colour;
          }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void MergeFrom(pb::CodedInputStream input) {
          uint tag;
          while ((tag = input.ReadTag()) != 0) {
            switch(tag) {
              default:
                input.SkipLastField();
                break;
              case 10: {
                Username = input.ReadString();
                break;
              }
              case 18: {
                if (player_ == null) {
                  player_ = new global::Intergroup.Board.Player();
                }
                input.ReadMessage(player_);
                break;
              }
              case 24: {
                colour_ = (global::Intergroup.Lobby.GameSetup.Types.PlayerSetting.Types.Colour) input.ReadEnum();
                break;
              }
            }
          }
        }

        #region Nested types
        /// <summary>Container for nested types declared in the PlayerSetting message type.</summary>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public static partial class Types {
          public enum Colour {
            [pbr::OriginalName("RED")] Red = 0,
            [pbr::OriginalName("BLUE")] Blue = 1,
            [pbr::OriginalName("ORANGE")] Orange = 2,
            [pbr::OriginalName("WHITE")] White = 3,
            [pbr::OriginalName("GREEN")] Green = 4,
            [pbr::OriginalName("BROWN")] Brown = 5,
          }

        }
        #endregion

      }

    }
    #endregion

  }

  public sealed partial class GameWon : pb::IMessage<GameWon> {
    private static readonly pb::MessageParser<GameWon> _parser = new pb::MessageParser<GameWon>(() => new GameWon());
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<GameWon> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Intergroup.Lobby.LobbyReflection.Descriptor.MessageTypes[3]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public GameWon() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public GameWon(GameWon other) : this() {
      Winner = other.winner_ != null ? other.Winner.Clone() : null;
      hiddenCards_ = other.hiddenCards_.Clone();
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public GameWon Clone() {
      return new GameWon(this);
    }

    /// <summary>Field number for the "winner" field.</summary>
    public const int WinnerFieldNumber = 1;
    private global::Intergroup.Board.Player winner_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Intergroup.Board.Player Winner {
      get { return winner_; }
      set {
        winner_ = value;
      }
    }

    /// <summary>Field number for the "hiddenCards" field.</summary>
    public const int HiddenCardsFieldNumber = 2;
    private static readonly pb::FieldCodec<global::Intergroup.Lobby.GameWon.Types.CardReveal> _repeated_hiddenCards_codec
        = pb::FieldCodec.ForMessage(18, global::Intergroup.Lobby.GameWon.Types.CardReveal.Parser);
    private readonly pbc::RepeatedField<global::Intergroup.Lobby.GameWon.Types.CardReveal> hiddenCards_ = new pbc::RepeatedField<global::Intergroup.Lobby.GameWon.Types.CardReveal>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public pbc::RepeatedField<global::Intergroup.Lobby.GameWon.Types.CardReveal> HiddenCards {
      get { return hiddenCards_; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as GameWon);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(GameWon other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (!object.Equals(Winner, other.Winner)) return false;
      if(!hiddenCards_.Equals(other.hiddenCards_)) return false;
      return true;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (winner_ != null) hash ^= Winner.GetHashCode();
      hash ^= hiddenCards_.GetHashCode();
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (winner_ != null) {
        output.WriteRawTag(10);
        output.WriteMessage(Winner);
      }
      hiddenCards_.WriteTo(output, _repeated_hiddenCards_codec);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (winner_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(Winner);
      }
      size += hiddenCards_.CalculateSize(_repeated_hiddenCards_codec);
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(GameWon other) {
      if (other == null) {
        return;
      }
      if (other.winner_ != null) {
        if (winner_ == null) {
          winner_ = new global::Intergroup.Board.Player();
        }
        Winner.MergeFrom(other.Winner);
      }
      hiddenCards_.Add(other.hiddenCards_);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            input.SkipLastField();
            break;
          case 10: {
            if (winner_ == null) {
              winner_ = new global::Intergroup.Board.Player();
            }
            input.ReadMessage(winner_);
            break;
          }
          case 18: {
            hiddenCards_.AddEntriesFrom(input, _repeated_hiddenCards_codec);
            break;
          }
        }
      }
    }

    #region Nested types
    /// <summary>Container for nested types declared in the GameWon message type.</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static partial class Types {
      public sealed partial class CardReveal : pb::IMessage<CardReveal> {
        private static readonly pb::MessageParser<CardReveal> _parser = new pb::MessageParser<CardReveal>(() => new CardReveal());
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public static pb::MessageParser<CardReveal> Parser { get { return _parser; } }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public static pbr::MessageDescriptor Descriptor {
          get { return global::Intergroup.Lobby.GameWon.Descriptor.NestedTypes[0]; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        pbr::MessageDescriptor pb::IMessage.Descriptor {
          get { return Descriptor; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public CardReveal() {
          OnConstruction();
        }

        partial void OnConstruction();

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public CardReveal(CardReveal other) : this() {
          Player = other.player_ != null ? other.Player.Clone() : null;
          vPCards_ = other.vPCards_.Clone();
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public CardReveal Clone() {
          return new CardReveal(this);
        }

        /// <summary>Field number for the "player" field.</summary>
        public const int PlayerFieldNumber = 1;
        private global::Intergroup.Board.Player player_;
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public global::Intergroup.Board.Player Player {
          get { return player_; }
          set {
            player_ = value;
          }
        }

        /// <summary>Field number for the "VPCards" field.</summary>
        public const int VPCardsFieldNumber = 2;
        private static readonly pb::FieldCodec<global::Intergroup.Board.VictoryPoint> _repeated_vPCards_codec
            = pb::FieldCodec.ForEnum(18, x => (int) x, x => (global::Intergroup.Board.VictoryPoint) x);
        private readonly pbc::RepeatedField<global::Intergroup.Board.VictoryPoint> vPCards_ = new pbc::RepeatedField<global::Intergroup.Board.VictoryPoint>();
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public pbc::RepeatedField<global::Intergroup.Board.VictoryPoint> VPCards {
          get { return vPCards_; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override bool Equals(object other) {
          return Equals(other as CardReveal);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public bool Equals(CardReveal other) {
          if (ReferenceEquals(other, null)) {
            return false;
          }
          if (ReferenceEquals(other, this)) {
            return true;
          }
          if (!object.Equals(Player, other.Player)) return false;
          if(!vPCards_.Equals(other.vPCards_)) return false;
          return true;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override int GetHashCode() {
          int hash = 1;
          if (player_ != null) hash ^= Player.GetHashCode();
          hash ^= vPCards_.GetHashCode();
          return hash;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override string ToString() {
          return pb::JsonFormatter.ToDiagnosticString(this);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void WriteTo(pb::CodedOutputStream output) {
          if (player_ != null) {
            output.WriteRawTag(10);
            output.WriteMessage(Player);
          }
          vPCards_.WriteTo(output, _repeated_vPCards_codec);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public int CalculateSize() {
          int size = 0;
          if (player_ != null) {
            size += 1 + pb::CodedOutputStream.ComputeMessageSize(Player);
          }
          size += vPCards_.CalculateSize(_repeated_vPCards_codec);
          return size;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void MergeFrom(CardReveal other) {
          if (other == null) {
            return;
          }
          if (other.player_ != null) {
            if (player_ == null) {
              player_ = new global::Intergroup.Board.Player();
            }
            Player.MergeFrom(other.Player);
          }
          vPCards_.Add(other.vPCards_);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void MergeFrom(pb::CodedInputStream input) {
          uint tag;
          while ((tag = input.ReadTag()) != 0) {
            switch(tag) {
              default:
                input.SkipLastField();
                break;
              case 10: {
                if (player_ == null) {
                  player_ = new global::Intergroup.Board.Player();
                }
                input.ReadMessage(player_);
                break;
              }
              case 18:
              case 16: {
                vPCards_.AddEntriesFrom(input, _repeated_vPCards_codec);
                break;
              }
            }
          }
        }

      }

    }
    #endregion

  }

  #endregion

}

#endregion Designer generated code
