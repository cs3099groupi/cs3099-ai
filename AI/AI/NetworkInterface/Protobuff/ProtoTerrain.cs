// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: terrain/terrain.proto
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Intergroup.Terrain {

  /// <summary>Holder for reflection information generated from terrain/terrain.proto</summary>
  public static partial class TerrainReflection {

    #region Descriptor
    /// <summary>File descriptor for terrain/terrain.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static TerrainReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "ChV0ZXJyYWluL3RlcnJhaW4ucHJvdG8SEmludGVyZ3JvdXAudGVycmFpbipR",
            "CgRLaW5kEgkKBUhJTExTEAASCwoHUEFTVFVSRRABEg0KCU1PVU5UQUlOUxAC",
            "EgoKBkZJRUxEUxADEgoKBkZPUkVTVBAEEgoKBkRFU0VSVBAFYgZwcm90bzM="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { },
          new pbr::GeneratedClrTypeInfo(new[] {typeof(global::Intergroup.Terrain.Kind), }, null));
    }
    #endregion

  }
  #region Enums
  public enum Kind {
    [pbr::OriginalName("HILLS")] Hills = 0,
    [pbr::OriginalName("PASTURE")] Pasture = 1,
    [pbr::OriginalName("MOUNTAINS")] Mountains = 2,
    [pbr::OriginalName("FIELDS")] Fields = 3,
    [pbr::OriginalName("FOREST")] Forest = 4,
    [pbr::OriginalName("DESERT")] Desert = 5,
  }

  #endregion

}

#endregion Designer generated code
