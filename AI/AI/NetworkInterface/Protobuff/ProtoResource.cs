// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: resource/resource.proto
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Intergroup.Resource {

  /// <summary>Holder for reflection information generated from resource/resource.proto</summary>
  public static partial class ResourceReflection {

    #region Descriptor
    /// <summary>File descriptor for resource/resource.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static ResourceReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "ChdyZXNvdXJjZS9yZXNvdXJjZS5wcm90bxITaW50ZXJncm91cC5yZXNvdXJj",
            "ZSJRCgZDb3VudHMSDQoFYnJpY2sYASABKAUSDgoGbHVtYmVyGAIgASgFEgwK",
            "BHdvb2wYAyABKAUSDQoFZ3JhaW4YBCABKAUSCwoDb3JlGAUgASgFKkgKBEtp",
            "bmQSCwoHR0VORVJJQxAAEgkKBUJSSUNLEAESCgoGTFVNQkVSEAISCAoEV09P",
            "TBADEgkKBUdSQUlOEAQSBwoDT1JFEAViBnByb3RvMw=="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { },
          new pbr::GeneratedClrTypeInfo(new[] {typeof(global::Intergroup.Resource.Kind), }, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::Intergroup.Resource.Counts), global::Intergroup.Resource.Counts.Parser, new[]{ "Brick", "Lumber", "Wool", "Grain", "Ore" }, null, null, null)
          }));
    }
    #endregion

  }
  #region Enums
  public enum Kind {
    [pbr::OriginalName("GENERIC")] Generic = 0,
    [pbr::OriginalName("BRICK")] Brick = 1,
    [pbr::OriginalName("LUMBER")] Lumber = 2,
    [pbr::OriginalName("WOOL")] Wool = 3,
    [pbr::OriginalName("GRAIN")] Grain = 4,
    [pbr::OriginalName("ORE")] Ore = 5,
  }

  #endregion

  #region Messages
  public sealed partial class Counts : pb::IMessage<Counts> {
    private static readonly pb::MessageParser<Counts> _parser = new pb::MessageParser<Counts>(() => new Counts());
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<Counts> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Intergroup.Resource.ResourceReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Counts() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Counts(Counts other) : this() {
      brick_ = other.brick_;
      lumber_ = other.lumber_;
      wool_ = other.wool_;
      grain_ = other.grain_;
      ore_ = other.ore_;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Counts Clone() {
      return new Counts(this);
    }

    /// <summary>Field number for the "brick" field.</summary>
    public const int BrickFieldNumber = 1;
    private int brick_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int Brick {
      get { return brick_; }
      set {
        brick_ = value;
      }
    }

    /// <summary>Field number for the "lumber" field.</summary>
    public const int LumberFieldNumber = 2;
    private int lumber_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int Lumber {
      get { return lumber_; }
      set {
        lumber_ = value;
      }
    }

    /// <summary>Field number for the "wool" field.</summary>
    public const int WoolFieldNumber = 3;
    private int wool_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int Wool {
      get { return wool_; }
      set {
        wool_ = value;
      }
    }

    /// <summary>Field number for the "grain" field.</summary>
    public const int GrainFieldNumber = 4;
    private int grain_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int Grain {
      get { return grain_; }
      set {
        grain_ = value;
      }
    }

    /// <summary>Field number for the "ore" field.</summary>
    public const int OreFieldNumber = 5;
    private int ore_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int Ore {
      get { return ore_; }
      set {
        ore_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as Counts);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(Counts other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (Brick != other.Brick) return false;
      if (Lumber != other.Lumber) return false;
      if (Wool != other.Wool) return false;
      if (Grain != other.Grain) return false;
      if (Ore != other.Ore) return false;
      return true;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (Brick != 0) hash ^= Brick.GetHashCode();
      if (Lumber != 0) hash ^= Lumber.GetHashCode();
      if (Wool != 0) hash ^= Wool.GetHashCode();
      if (Grain != 0) hash ^= Grain.GetHashCode();
      if (Ore != 0) hash ^= Ore.GetHashCode();
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (Brick != 0) {
        output.WriteRawTag(8);
        output.WriteInt32(Brick);
      }
      if (Lumber != 0) {
        output.WriteRawTag(16);
        output.WriteInt32(Lumber);
      }
      if (Wool != 0) {
        output.WriteRawTag(24);
        output.WriteInt32(Wool);
      }
      if (Grain != 0) {
        output.WriteRawTag(32);
        output.WriteInt32(Grain);
      }
      if (Ore != 0) {
        output.WriteRawTag(40);
        output.WriteInt32(Ore);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (Brick != 0) {
        size += 1 + pb::CodedOutputStream.ComputeInt32Size(Brick);
      }
      if (Lumber != 0) {
        size += 1 + pb::CodedOutputStream.ComputeInt32Size(Lumber);
      }
      if (Wool != 0) {
        size += 1 + pb::CodedOutputStream.ComputeInt32Size(Wool);
      }
      if (Grain != 0) {
        size += 1 + pb::CodedOutputStream.ComputeInt32Size(Grain);
      }
      if (Ore != 0) {
        size += 1 + pb::CodedOutputStream.ComputeInt32Size(Ore);
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(Counts other) {
      if (other == null) {
        return;
      }
      if (other.Brick != 0) {
        Brick = other.Brick;
      }
      if (other.Lumber != 0) {
        Lumber = other.Lumber;
      }
      if (other.Wool != 0) {
        Wool = other.Wool;
      }
      if (other.Grain != 0) {
        Grain = other.Grain;
      }
      if (other.Ore != 0) {
        Ore = other.Ore;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            input.SkipLastField();
            break;
          case 8: {
            Brick = input.ReadInt32();
            break;
          }
          case 16: {
            Lumber = input.ReadInt32();
            break;
          }
          case 24: {
            Wool = input.ReadInt32();
            break;
          }
          case 32: {
            Grain = input.ReadInt32();
            break;
          }
          case 40: {
            Ore = input.ReadInt32();
            break;
          }
        }
      }
    }

  }

  #endregion

}

#endregion Designer generated code
