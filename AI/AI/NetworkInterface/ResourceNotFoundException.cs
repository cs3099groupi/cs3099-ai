﻿using System;
using System.Runtime.Serialization;

[Serializable]
internal class ResourceNotFoundException : Exception {//thrown by the network manager if a parsing error occurs while converting a protobuf message into Event (usually caused by server problems)
    public ResourceNotFoundException() {
    }

    public ResourceNotFoundException(string message) : base(message) {
    }

    public ResourceNotFoundException(string message, Exception innerException) : base(message, innerException) {
    }

    protected ResourceNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) {
    }
}