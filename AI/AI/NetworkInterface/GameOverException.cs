﻿using System;

namespace AI {
    class GameOverException : Exception { }//is thrown by the main loop when the game ends, to close the program.
}
