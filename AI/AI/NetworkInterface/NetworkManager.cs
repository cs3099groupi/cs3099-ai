﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System;
using Intergroup.Board;
using Intergroup;   //Intergroup.Requests in Namespace Intergroup
using Intergroup.Resource;
using System.Text.RegularExpressions;
using AI.ChoiceGenerator.Events;
using AI.ChoiceGenerator.Action_Classes;
using Google.Protobuf;

namespace AI {

    public class TerrainNotFoundException : Exception {
        public TerrainNotFoundException(Intergroup.Terrain.Kind terrain) {
            Console.WriteLine("The resource " + terrain.ToString() + " could not be found");
        }
    }

    public class PortNotFoundException : Exception {
        public PortNotFoundException(int x1, int y1, int x2, int y2) {
            Console.WriteLine("The port " + x1 + "," + y1 + " " + x2 + "," + y2 + " could not be found");
        }
    }

    public class IdNotFoundException : Exception {
        public IdNotFoundException(Intergroup.Board.Player.Types.Id id) {
            Console.WriteLine("Player ID: " + id + " cannot be found.");
        }
    }

    class NetworkManager {
        IActions actions = new Actions();
        public bool gameLoopStarted;
        private int PORT = 3334;
        private string ADDRESS = "localhost";
        private string Username;
        Thread mThread;
        private NetworkStream ns;

        //used to prevent stacking of changes upon server rejecting a previous change
        public volatile bool syncedWithServer;//sets to not synced while it is waiting for a response from the server. if the response is excpected then the synced is set to true
                                              //volatile for thread safety, is atomic
        public Intergroup.Event currentPlayerTradeOfferToAi;//contains the information about the trade that has been offered to the AI. This is used by multiple threads so should be locked.          

        //sendRequest encapsulates the actual sending of the message to the server.
        public void sendRequest(Intergroup.Request req) {
            syncedWithServer = false;
            Message mess = new Message {
                Request = req
            };
            mess.WriteDelimitedTo(ns);
        }

        // Use this for initialization
        //called by the main thread
        public void StartClient(CatanAI ai, string address, int port, string username) {
            syncedWithServer = false;
            ADDRESS = address;
            PORT = port;
            Username = username;
            mThread = new Thread(() => ConnectClient(ai));
            mThread.Start();
            Console.WriteLine("Client Thread Started...");
        }

        //closes the threads. This causes exceptions if the thread is currently trying to read something from the socket, which it will be.
        public void EndClient() {
            ns.Close();
            mThread.Join(500);
        }

        public void ConnectClient(CatanAI ai) {
            try {
                TcpClient client = new TcpClient(ADDRESS, PORT);
                ns = client.GetStream();

                Intergroup.Board.Player player = new Intergroup.Board.Player { Id = new Intergroup.Board.Player.Types.Id() };

                //the first message sent is always a join request. This is hard coded.
                Console.WriteLine("Sent player");
                Intergroup.Lobby.Join joinGameReq = new Intergroup.Lobby.Join { Username = Username };
                Intergroup.Message newReq = new Intergroup.Message {
                    Request = new Intergroup.Request { JoinLobby = joinGameReq }
                };
                newReq.WriteDelimitedTo(ns);
                Intergroup.Message response;
                //after the join request has been sent the thread infinitely reads messages from the server and parses them into Events etc.
                while (true) {//'forever'
                    response = Intergroup.Message.Parser.ParseDelimitedFrom(ns);
                    if (response != null) {
                        ai.eventParsed = false;
                        handle(ai, response.Event, this);//give it to the main class to handle
                    }
                }
            } catch (Exception e) {
                Console.WriteLine(e.ToString());
                Console.ReadKey();
            }
        }

        //encapsulates dealing with a response from the server.
        //parses them into IAction and adds them to the list for the main thread to deal with.
        private void handle(CatanAI ai, Intergroup.Event ev, NetworkManager nm) {
            ai.handleIncomingEvent(ev);
        }

        //parse parses the event into an IAction object, representing the event. on failure it returns the special IAction object Error.
        public IAction parse(CatanAI ai, Intergroup.Event newEvent, NetworkManager nm) {
            //each of these ifs checks for a different type of event, there is one if for every event in the intergroup protocol.
            if (newEvent.BeginGame != null) {
                return new GameSetup(newEvent.BeginGame, Username);
            } else if (newEvent.LobbyUpdate != null) {
                return updateLobby(newEvent);
            } else if (newEvent.RoadBuilt != null) {
                return buildRoad(newEvent, ai.theGameState.getState());
            } else if (newEvent.SettlementBuilt != null) {
                return buildSettlement(newEvent, ai.theGameState.getState());
            } else if (newEvent.CityBuilt != null) {
                return buildCity(newEvent);
            } else if (newEvent.BankTrade != null) {
                return bankTrade(newEvent);
            } else if (newEvent.TypeCase == Event.TypeOneofCase.ResourceChosen) {
                return resourceChosen(newEvent);
            } else if (newEvent.RobberMoved != null) {
                return robberMoved(newEvent);
            } else if (newEvent.TypeCase == Event.TypeOneofCase.DevCardPlayed) {
                return devCardPlayed(newEvent);
            } else if (newEvent.MonopolyResolution != null) {
                return monopolyResolution(ai, newEvent, nm);
            } else if (newEvent.Error != null) {
                Console.WriteLine("error returned: " + newEvent);
                return new Error(newEvent);
            } else if (newEvent.TurnEnded != null) {
                Console.WriteLine("turn ended");//some minimal printing is done to keep track of progress
                return new StartNewTurn();
            } else if (newEvent.PlayerTradeInitiated != null) {
                //need to check if its an offer for the AI
                return playerTradeInitiated(newEvent, nm);
            } else if (newEvent.PlayerTradeRejected != null) {
                return new RejectPlayerTrade();
            } else if (newEvent.PlayerTradeAccepted != null) {
                return playerTradeAccepted(newEvent, nm);
            } else if (newEvent.DevCardBought != null) {
                return boughtDevCard(newEvent);
            } else if (newEvent.ResourceStolen != null) {
                return steal(newEvent);
            } else if (newEvent.CardsDiscarded != null) {
                return cardsDiscarded(newEvent);
            } else if (newEvent.Rolled != null) {//assume this tells the main loop thread to create a collectResources action?
                return diceRolled(newEvent);
            } else if (newEvent.GameWon != null) {//ends the main loop thread?
                return gameWon(newEvent);
            } else if (newEvent.ChatMessage != null) {
                return chatMessage(newEvent);
            }
            //if the event was none of the above then an error must be generated. This should not happen, if the server is only sending appropriate messages.\
            Console.WriteLine("Error returned, no valid catches found!");
            return new Error(newEvent);
        }

        //parses an intergroup resource into an int that is recognised by the program as one of the Const int values. on failuer throws an exception.
        public int parseResource(Intergroup.Resource.Kind resource) {
            switch (resource) {
                case Intergroup.Resource.Kind.Ore:
                    return Const.ORE;
                case Intergroup.Resource.Kind.Grain:
                    return Const.GRAIN;
                case Intergroup.Resource.Kind.Brick:
                    return Const.BRICK;
                case Intergroup.Resource.Kind.Lumber:
                    return Const.LUMBER;
                case Intergroup.Resource.Kind.Wool:
                    return Const.WOOL;
                case Intergroup.Resource.Kind.Generic:
                    return Const.UNIVERSAL;
                default:
                    throw new ResourceNotFoundException(resource.ToString());
            }
        }

        private int parsePlayableDevCard(PlayableDevCard card) {
            switch (card) {
                case PlayableDevCard.Knight:
                    return Const.KNIGHT;
                case PlayableDevCard.Monopoly:
                    return Const.MONOPOLY;
                case PlayableDevCard.YearOfPlenty:
                    return Const.YEAR_OF_PLENTY;
                case PlayableDevCard.RoadBuilding:
                    return Const.ROAD_BUILDING;
                default:
                    return -1;
            }
        }

        public int parsePlayerID(Intergroup.Board.Player.Types.Id id) {
            switch (id) {
                case Intergroup.Board.Player.Types.Id.Player1:
                    return 0;
                case Intergroup.Board.Player.Types.Id.Player2:
                    return 1;
                case Intergroup.Board.Player.Types.Id.Player3:
                    return 2;
                case Intergroup.Board.Player.Types.Id.Player4:
                    return 3;
                case Intergroup.Board.Player.Types.Id.Player5:
                    return 4;
                case Intergroup.Board.Player.Types.Id.Player6:
                    return 5;
                default:
                    throw new IdNotFoundException(id);
            }
        }

        public Intergroup.Board.Player.Types.Id getProtobufPlayerID(int id) {
            switch (id) {
                case 0:
                    return Intergroup.Board.Player.Types.Id.Player1;
                case 1:
                    return Intergroup.Board.Player.Types.Id.Player2;
                case 2:
                    return Intergroup.Board.Player.Types.Id.Player3;
                case 3:
                    return Intergroup.Board.Player.Types.Id.Player4;
                case 4:
                    return Intergroup.Board.Player.Types.Id.Player5;
                case 5:
                    return Intergroup.Board.Player.Types.Id.Player6;
                default:
                    throw new Exception();
            }
        }

        //parses a build road event into a build road action
        private IAction buildRoad(Intergroup.Event IntergroupRoad, int gameState) {
            int x1 = IntergroupRoad.RoadBuilt.A.X;
            int y1 = IntergroupRoad.RoadBuilt.A.Y;
            int x2 = IntergroupRoad.RoadBuilt.B.X;
            int y2 = IntergroupRoad.RoadBuilt.B.Y;

            ICoord c1 = new Coord(x1, y1);
            ICoord c2 = new Coord(x2, y2);
            IAction a;
            switch (gameState) {//depending on the game state an IntergroupRoad could represent different actions.
                case Const.FIRSTROADBUILDING:
                case Const.SECONDROADBUILDING:
                case Const.FIRSTROADBUILDINGBEFOREROLLINGTHEDICE:
                case Const.SECONDROADBUILDINGBEFOREROLLINGTHEDICE:
                    a = new BuildRoadBuildingRoad(c1, c2);
                    break;
                case Const.INITIALROAD:
                    a = new BuildInitialRoad(c1, c2);
                    break;
                case Const.WAITPLAY:
                    a = new BuyRoad(c1, c2);
                    break;
                default:
                    Console.WriteLine("invalid road time to build, inconsistent!");
                    a = new Error(IntergroupRoad);
                    break;
            }
            return a;
        }

        //parses a build settlement event into a build settlement action
        private IAction buildSettlement(Intergroup.Event IntergroupSettlement, int gstate) {
            ICoord pos = new Coord(IntergroupSettlement.SettlementBuilt.X, IntergroupSettlement.SettlementBuilt.Y);

            IAction a;
            switch (gstate) {
                case Const.INITIALSETTLEMENT:
                    a = new BuildInitialSettlement(pos);
                    break;
                case Const.WAITPLAY:
                    a = new BuySettlement(pos);
                    break;
                default:
                    Console.WriteLine("invalid settlement time to build, inconsistent!");
                    a = new Error(IntergroupSettlement);
                    break;
            }
            return a;
        }

        //parses a build city event into a bbuy city action
        private IAction buildCity(Intergroup.Event IntergroupCity) {
            return new BuyCity(new Coord(IntergroupCity.CityBuilt.X, IntergroupCity.CityBuilt.Y));
        }

        //parses a steal event into a steal action, either with or without a know resource stolen
        private IAction steal(Intergroup.Event IntergroupSteal) {
            int resource = parseResource(IntergroupSteal.ResourceStolen.Resource);

            //check if the trade is known or not
            if (resource != Const.UNIVERSAL) {
                return new confirmSteal(parseResource(IntergroupSteal.ResourceStolen.Resource), parsePlayerID(IntergroupSteal.ResourceStolen.Victim.Id));
            } else {
                return new confirmSteal(parsePlayerID(IntergroupSteal.ResourceStolen.Victim.Id));
            }
        }

        //parses an updatelobby event into the special IAction update lobby, which essentially does nothing. This is because the AI doesn't really care who else is playing
        private IAction updateLobby(Intergroup.Event IntergroupLobbyUpdate) {
            return new UpdateLobby();
        }

        //parses a text message into a ChatMessage action. just like update lobby this is ignored.
        private IAction chatMessage(Intergroup.Event IntergroupChatMessage) {
            return new ChatMessage();
        }

        //parses a bought devcard event into an allocat dev card action.
        private IAction boughtDevCard(Intergroup.Event Intergroupboughtdev) {
            return new AllocateDevCard(Intergroupboughtdev);
        }

        private IAction playerTradeInitiated(Intergroup.Event IntergroupPlayerTrade, NetworkManager nm) {
            currentPlayerTradeOfferToAi = IntergroupPlayerTrade;
            return new PlayerTradeInitiated(IntergroupPlayerTrade, nm);
        }

        private IAction playerTradeAccepted(Intergroup.Event IntergroupPlayerTrade, NetworkManager nm) {
            return new ConfirmPlayerTrade(currentPlayerTradeOfferToAi, nm);
        }

        private IAction monopolyResolution(CatanAI ai, Event IntergroupMonopRes, NetworkManager nm) {
            return new MonopolyResolution(ai, IntergroupMonopRes, nm);
        }

        private IAction cardsDiscarded(Intergroup.Event Intergroupcardsdiscard) {
            int[] ress = new int[Const.NUM_OF_RESOURCES];
            ress[Const.ORE] = Intergroupcardsdiscard.CardsDiscarded.Ore;
            ress[Const.GRAIN] = Intergroupcardsdiscard.CardsDiscarded.Grain;
            ress[Const.WOOL] = Intergroupcardsdiscard.CardsDiscarded.Wool;
            ress[Const.LUMBER] = Intergroupcardsdiscard.CardsDiscarded.Lumber;
            ress[Const.BRICK] = Intergroupcardsdiscard.CardsDiscarded.Brick;

            int playerID = parsePlayerID(Intergroupcardsdiscard.Instigator.Id);
            IAction a = new Discard(ress, playerID);
            return a;
        }

        private IAction resourceChosen(Intergroup.Event IntergroupResource) {
            return new ChooseResource(parseResource(IntergroupResource.ResourceChosen));
        }

        private IAction bankTrade(Intergroup.Event IntergroupBankTrade) {

            int[] wanting = resourceCounts(IntergroupBankTrade.BankTrade.Wanting);
            int[] offering = resourceCounts(IntergroupBankTrade.BankTrade.Offering);

            return new BankTrade(offering, wanting);
        }

        private int[] resourceCounts(Counts counts) {
            int[] output = new int[Const.NUM_OF_RESOURCES];

            output[Const.BRICK] = counts.Brick;
            output[Const.LUMBER] = counts.Lumber;
            output[Const.WOOL] = counts.Wool;
            output[Const.GRAIN] = counts.Grain;
            output[Const.ORE] = counts.Ore;

            return output;
        }

        private IAction robberMoved(Intergroup.Event IntergroupRobber) {
            int x = IntergroupRobber.RobberMoved.X;
            int y = IntergroupRobber.RobberMoved.Y;
            return new MoveRobber(new Coord(x, y));
        }

        private IAction devCardPlayed(Intergroup.Event IntergroupDevCardPlay) {
            IAction a = new PlayDevCard(parsePlayableDevCard(IntergroupDevCardPlay.DevCardPlayed));
            return a;
        }

        //parses the dice rolled event into either an allocate resource action, or if the diceroll was 7, then a seven rolled which activates the robber sequence.
        private IAction diceRolled(Intergroup.Event IntergroupRoll) {
            int rollval = IntergroupRoll.Rolled.A + IntergroupRoll.Rolled.B;
            if (rollval == 7) {
                return new SevenRolled();
            } else {
                return new AllocateResources(rollval);
            };
        }

        //parses a gameWon event into a wingame, which when run throws a game over exception.
        private IAction gameWon(Intergroup.Event IntergroupWIN) {
            return new WinGame();
        }
    }
}